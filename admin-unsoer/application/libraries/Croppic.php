<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Croppic {
	public $cropprop;
    public function js_set_var($val)
    {
    	return "var croppicContainerPreloadOptions = {
					uploadUrl:'{$val['uploadUrl']}',
					cropUrl:'{$val['cropUrl']}',
					loadPicture:'{$val['loadPicture']}',
					cropData:{
						'baseImage': '{$val['baseImage']}',
						'nameCrop': '{$val['nameCrop']}'
					},
					enableMousescroll:true,
					loaderHtml:'<div class=\"loader bubblingG\"><span id=\"bubblingG_1\"></span><span id=\"bubblingG_2\"></span><span id=\"bubblingG_3\"></span></div> ',
					onBeforeImgUpload: function(){ console.log('onBeforeImgUpload') },
					onAfterImgUpload: function(){ console.log('onAfterImgUpload') },
					onImgDrag: function(){ console.log('onImgDrag') },
					onImgZoom: function(){ console.log('onImgZoom') },
					onBeforeImgCrop: function(){ console.log('onBeforeImgCrop') },
					onAfterImgCrop:function(){ console.log('onAfterImgCrop') },
					onReset:function(){ console.log('onReset') },
					onError:function(errormessage){ console.log('onError:'+errormessage) }
				}
				var {$val['name']} = new Croppic('{$val['id']}', croppicContainerPreloadOptions);
    	";
    }

    //cropper
    public function set_env($arr)
    {
    	$this->cropprop['imgUrl'] = $arr['imgUrl'];
    	$this->cropprop['baseImage'] = $arr['baseImage'];
		// original sizes
		$this->cropprop['imgInitW'] = $arr['imgInitW'];
		$this->cropprop['imgInitH'] = $arr['imgInitH'];
		// resized sizes
		$this->cropprop['imgW'] = $arr['imgW'];
		$this->cropprop['imgH'] = $arr['imgH'];
		// offsets
		$this->cropprop['imgY1'] = $arr['imgY1'];
		$this->cropprop['imgX1'] = $arr['imgX1'];
		// crop box
		$this->cropprop['cropW'] = $arr['cropW'];
		$this->cropprop['cropH'] = $arr['cropH'];
		// rotation angle
		$this->cropprop['angle'] = $arr['rotation'];
		$this->cropprop['jpeg_quality'] = 100;

		$this->cropprop['output_filename'] = $arr['nameCrop'];
    }

    public function crop()
    {  	
    	// $urlpath = str_replace(base_url(), '', $this->cropprop['imgUrl']);
    	$urlpath = $this->cropprop['baseImage'];
		$what = getimagesize($urlpath);

		switch(strtolower($what['mime']))
		{
		    case 'image/png':
		        $img_r = imagecreatefrompng($urlpath);
				$source_image = imagecreatefrompng($urlpath);
				$type = '.png';
		        break;
		    case 'image/jpg':
		    case 'image/jpeg':
		        $img_r = imagecreatefromjpeg($urlpath);
				$source_image = imagecreatefromjpeg($urlpath);
				error_log("jpg");
				$type = '.jpg';
		        break;
		    case 'image/gif':
		        $img_r = imagecreatefromgif($urlpath);
				$source_image = imagecreatefromgif($urlpath);
				$type = '.gif';
		        break;
		    default: 
		    	$data = [
		    		'status' => 'error',
		    		'errors' => true,
		    		'message' => 'Gambar tidak didukung crop',
		    	];
		    	return $data;
		    break;
		}

		//Check write Access to Directory

		if(!is_writable(dirname($this->cropprop['output_filename']))){
		    $data = [
			    	'status' => 'error',
		    		'errors' => true,
		    		'path' => $this->cropprop['output_filename'],
		    		'message' => 'Akses tulis ditolak',
		    	];
		    return $data;
		}else{

		    // resize the original image to size of editor
		    $resizedImage = imagecreatetruecolor($this->cropprop['imgW'], $this->cropprop['imgH']);
			imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $this->cropprop['imgW'], $this->cropprop['imgH'], $this->cropprop['imgInitW'], $this->cropprop['imgInitH']);
		    // rotate the rezized image
		    $rotated_image = imagerotate($resizedImage, - $this->cropprop['angle'], 0);
		    // find new width & height of rotated image
		    $rotated_width = imagesx($rotated_image);
		    $rotated_height = imagesy($rotated_image);
		    // diff between rotated & original sizes
		    $dx = $rotated_width - $this->cropprop['imgW'];
		    $dy = $rotated_height - $this->cropprop['imgH'];
		    // crop rotated image to fit into original rezized rectangle
			$cropped_rotated_image = imagecreatetruecolor($this->cropprop['imgW'], $this->cropprop['imgH']);
			imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
			imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $this->cropprop['imgW'], $this->cropprop['imgH'], $this->cropprop['imgW'], $this->cropprop['imgH']);
			// crop image into selected area
			$final_image = imagecreatetruecolor($this->cropprop['cropW'], $this->cropprop['cropH']);
			imagecolortransparent($final_image, imagecolorallocate($final_image, 0, 0, 0));
			imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $this->cropprop['imgX1'], $this->cropprop['imgY1'], $this->cropprop['cropW'], $this->cropprop['cropH'], $this->cropprop['cropW'], $this->cropprop['cropH']);
			// finally output png image
			//imagepng($final_image, $output_filename.$type, $png_quality);
			$outputfile = $this->cropprop['output_filename'];
			imagejpeg($final_image, $outputfile, $this->cropprop['jpeg_quality']);
			$data = Array(
				'status' => 'success',
			    "errors" => false,
			    "url" => base_url().$outputfile
		    );

		    return $data;
		}
    }
}
