<?php 

/** 
 * Gallery 
 */
class Gallery extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('Gallery_model');
		$this->load->model('Foto_kegiatan_model', 'fkm');
		$this->load->library('form_validation');
		$this->load->library('image_lib');

		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('login');
		}
	}

	public function index()
	{
		$data['judul'] = 'Gallery';
		$data['gallery'] = $this->Gallery_model->getAllGallery();

		if ( $this->input->post('keyword')  ) {
			$data['gallery'] = $this->Gallery_model->cariDataGallery();
		}
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('gallery/index', $data);
		$this->load->view('templates/footer');
	}

	public function tambah() 
	{
		$data['judul'] = 'Form Tambah Album Baru';
		$data['gallery'] = $this->Gallery_model->getAllGallery();

		$this->form_validation->set_rules('judul', 'Judul', 'required');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');

		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('gallery/tambah', $data);
			$this->load->view('templates/footer');
		} else {
			$dataupload = $this->upload_cover();
			//var_dump($dataupload);
			if($dataupload['state'] == false){
				$dataupload['upload_data']['file_name'] = 'no-image.png';
			} 
			// else {
				// $this->wmtest($dataupload['upload_data']['file_name']);	
			// }

			$this->Gallery_model->imageUpload = $dataupload['upload_data']['file_name'];
			$data = $this->Gallery_model->tambahDataGallery();
			$this->session->set_flashdata('flash', 'ditambahkan');
			redirect('gallery');
		}
	}

	public function upload_cover()
    {
        $config['upload_path']          = '../upload/gallery';
        $config['allowed_types']        = 'gif|jpg|jpeg|png';
        $config['file_name']            = time();
        $config['max_size']         	= 5120;

      	$this->load->library('upload',$config);
        if(!empty($_FILES['cover_album']['name'])){
 
            if ($this->upload->do_upload('cover_album')){
                $gbr = $this->upload->data();
                //compress
                // var_dump($gbr);
                // var_dump($gbr['file_size']);

                if ($gbr['file_size'] < 312 ) { // no compress
                	// echo " dibawah 1/2 mb";
                	
                } else if ($gbr['file_size'] > 312 && $gbr['file_size'] < 1024) {
                	// echo " diatas 1/2 - 1mb";
                	$this->compressImageOne($gbr['file_name']);
                } else if ($gbr['file_size'] > 1024 && $gbr['file_size'] < 3072 ) {
                	
                	// echo "di atas 1 mb - 3 mb";
                	$this->compressImageOne($gbr['file_name']);	

                } else if ($gbr['file_size'] > 3072 && $gbr['file_size'] < 5120 ) {
                	
                	// echo "di atas 3 mb - 5 mb";
                	$this->compressImageTwo($gbr['file_name']);	
                } else if ($gbr['file_size'] > 5120 ) {
                	echo " File terlalu Besar, Max 5 MB";
                }
                
	            $data = array('state' => true, 'upload_data' => $gbr);
            } else  {
	            $data = array('state' => false, 'error' => $this->upload->display_errors());
	        }
        } else {
            $data = array('state' => false, 'error' => "Image yang diupload kosong");
        }   
        return $data;         
    }

    //Compress Image diatas 1 - 3 mb
	public function compressImageOne($filename)
	{
        $config['image_library']='gd2';
        $config['source_image']='../upload/gallery/'.$filename;
        $config['maintain_ratio'] = true;
        $config['master_dim'] = 'height';
        $config['create_thumb']= FALSE;
        $config['quality']= '50%';
        $config['width']= 100;
        $config['height']= 500;

        $this->load->library('image_lib', $config);
        $this->image_lib->clear();
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
	}

		// Compress Image di atas 3 - 5 mb
	public function compressImageTwo($filename)
	{
        $config['image_library']='gd2';
        $config['source_image']='../upload/gallery/'.$filename;
        $config['maintain_ratio'] = TRUE;
        $config['master_dim'] = 'height';
        $config['create_thumb']= FALSE;
        $config['quality'] = '300%';
        $config['width']= 1;
        $config['height']= 200;

        $this->load->library('image_lib', $config);
        $this->image_lib->clear();
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
	}


	public function detail($id)
	{
		$data['judul'] = 'Detail Album';
		$data['gallery'] = $this->Gallery_model->getGalleryById($id);
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('gallery/detail', $data);
		$this->load->view('templates/footer');
	}

	public function ubah($id_gallery) 
	{
		$data['judul'] = 'Form Ubah Data Album';
		$data['gallery'] = $this->Gallery_model->getGalleryForUpdate($id_gallery);
	
		$this->form_validation->set_rules('judul', 'Judul', 'required');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
		
		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('gallery/ubah', $data);
			$this->load->view('templates/footer');
		} else {
			// var_dump($this->input->post());
			$upload = $this->input->post('upload_new_file');
			if($upload == 1){
				$dataupload_ubah = $this->upload_cover_ubah();	

				if($dataupload_ubah['state'] == false){
					$dataupload_ubah['upload_data']['file_name'] = 'no-image.png';
				}

				if($dataupload_ubah['state']){
					unlink($this->input->post('gambar_lama'));
				}
			}		
			$this->Gallery_model->imageUpload = $dataupload_ubah['upload_data']['file_name'];

			var_dump($this->Gallery_model);
			if($upload == 1){
				$this->Gallery_model->ubahDataGallery(true);
			} else {
				$this->Gallery_model->ubahDataGallery();
			}
			$this->session->set_flashdata('flash', 'diubah');
			redirect('gallery');
		}
	}

	public function upload_cover_ubah()
    {
        $config['upload_path'] 		= '../upload/gallery'; //path folder
        $config['allowed_types'] 	= 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['file_name'] 		= time(); //Enkripsi nama yang terupload
        $config['max_size']         = 5120; // 5 mb
        

        $this->load->library('upload',$config);
        if(!empty($_FILES['cover_album']['name'])){
 
            if ($this->upload->do_upload('cover_album')){
                $gbr = $this->upload->data();
                //compress
                // var_dump($gbr);
                // var_dump($gbr['file_size']);

                if ($gbr['file_size'] < 312 ) { // no compress
                	// echo " dibawah 1/2 mb";
                	
                } else if ($gbr['file_size'] > 312 && $gbr['file_size'] < 1024) {
                	// echo " diatas 1/2 - 1mb";
                	$this->compressImageOne($gbr['file_name']);
                } else if ($gbr['file_size'] > 1024 && $gbr['file_size'] < 3072 ) {
                	
                	// echo "di atas 1 mb - 3 mb";
                	$this->compressImageOne($gbr['file_name']);	

                } else if ($gbr['file_size'] > 3072 && $gbr['file_size'] < 5120 ) {
                	
                	// echo "di atas 3 mb - 5 mb";
                	$this->compressImageTwo($gbr['file_name']);	
                } else if ($gbr['file_size'] > 5120 ) {
                	echo " File terlalu Besar, Max 5 MB";
                }
                
	            $data = array('state' => true, 'upload_data' => $gbr);
            } else  {
	            $data = array('state' => false, 'error' => $this->upload->display_errors());
	        }
        } else {
            $data = array('state' => false, 'error' => "Image yang diupload kosong");
        }   
        return $data;
	}

	public function upload_foto($id_gallery) 
	{
		$data['judul'] = 'Form Upload Foto Kegiatan';
		$data['gallery'] = $this->Gallery_model->getGalleryUploadFoto($id_gallery);

		$this->form_validation->set_rules('id_gallery', 'Id galery', 'required');
		$this->form_validation->set_rules('id_foto', 'Id foto', 'required');


		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('gallery/upload_foto', $data);
			$this->load->view('templates/footer');
		} else {
			$this->baru_multi();
			redirect('gallery/update_deskripsi/'.$id_gallery);
		}

	}

	public function baru_multi()
	{
		//detect post
		if($this->input->post()){
			// var_dump($_FILES['userfile']);
			if(!empty($_FILES['file_foto']['name'][0])){
				$filesbox = $_FILES;
				// var_dump($filesbox);
				// exit();
				$num = count($_FILES['file_foto']['name']);
				// $arr = [];
				$maxnow = (int) $this->input->post('max_now');
				for($i=0; $i < $num; $i++){
					$maxnow++;
					$_FILES['file_foto']['name'] = $filesbox['file_foto'] ['name'] [$i];
			        $_FILES['file_foto']['type'] = $filesbox['file_foto'] ['type'] [$i];
			        $_FILES['file_foto']['tmp_name'] = $filesbox['file_foto'] ['tmp_name'] [$i];
			        $_FILES['file_foto']['error'] = $filesbox['file_foto'] ['error'] [$i];
			        $_FILES['file_foto']['size'] = $filesbox['file_foto'] ['size'] [$i];

					// $data['data_upload'][$i] = $this->m_member->upload('file_foto',time().'_'.$i);
					$newname = $this->input->post('id_gallery', true).'_'.$maxnow;
					$this->load->library('upload', $this->Gallery_model->dataconfig($newname));

        			if (!$this->upload->do_upload('file_foto'))
        			{
			            $dataupload[$i] = ['state' => false, 'error' => $this->upload->display_errors()];
			        }
			        else
			        {
			        	// $maxnow++;
			            $dataupload[$i] = ['state' => true, 'upload_data' => $this->upload->data()];
			           
			           // proses compress
			            if ($dataupload[$i]['upload_data']['file_size'] < 312 ) { // no compress
		                	// echo " dibawah 1/2 mb";
		                	
		                } else if ($dataupload[$i]['upload_data']['file_size'] > 312 && $dataupload[$i]['upload_data']['file_size'] < 1024) {
		                	// echo " diatas 1/2 - 1mb";
		                	$this->compressImageFotoOne($dataupload[$i]['upload_data']['file_name']);

		                } else if ($dataupload[$i]['upload_data']['file_size'] > 1024 && $dataupload[$i]['upload_data']['file_size'] < 3072 ) {
		                	
		                	// echo "di atas 1 mb - 3 mb";
		                	$this->compressImageFotoOne($dataupload[$i]['upload_data']['file_name']);	

		                } else if ($dataupload[$i]['upload_data']['file_size'] > 3072 && $dataupload[$i]['upload_data']['file_size'] < 5120 ) {
		                	
		                	// echo "di atas 3 mb - 5 mb";
		                	$this->compressImageFotoTwo($dataupload[$i]['upload_data']['file_name']);	
		                } else if ($dataupload[$i]['upload_data']['file_size'] > 5120 ) {
		                	echo " File terlalu Besar, Max 5 MB";
		                }

			        	// $this->wmtest($dataupload[$i]['upload_data']['file_name']);				
			        	$this->fkm->imageUpload = $dataupload[$i]['upload_data']['file_name'];
			        	$this->fkm->incrementId = $maxnow;
			        	$data = $this->fkm->tambahDataFotoKegiatan();
			        }
				}
			}			
		}
	}
	
	//Compress Image diatas 1 - 3 mb
	public function compressImageFotoOne($filename)
	{
        $config['image_library']='gd2';
        $config['source_image']='../upload/gallery/foto-kegiatan/'.$filename;
        $config['maintain_ratio'] = true;
        $config['master_dim'] = 'height';
        $config['create_thumb']= FALSE;
        $config['quality']= '150%';
        $config['width']= 100;
        $config['height']= 500;

        $this->load->library('image_lib', $config);
        $this->image_lib->clear();
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
	}

	//Compress Image diatas 1 - 3 mb
	public function compressImageFotoTwo($filename)
	{
        $config['image_library']='gd2';
        $config['source_image']='../upload/gallery/foto-kegiatan/'.$filename;
        $config['maintain_ratio'] = true;
        $config['master_dim'] = 'height';
        $config['create_thumb']= FALSE;
        $config['quality']= '350%';
        $config['width']= 100;
        $config['height']= 500;

        $this->load->library('image_lib', $config);
        $this->image_lib->clear();
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
	}
	
	// watermark
	public function wmtest($imgurl)
	{
		$config['image_library'] = 'gd2';
        $config['source_image'] = '../upload/gallery/foto-kegiatan/'.$imgurl;
        $config['wm_type'] = 'overlay';
        $config['wm_overlay_path'] = '../assets/images/watermark_foto.png';
        $config['wm_opacity'] = 100;
        $config['wm_vrt_alignment'] = 'middle';
        $config['wm_hor_alignment'] = 'center';
       
        $this->image_lib->initialize($config);
        if (!$this->image_lib->watermark()) {
            $data = array('state' => false, 'error' => $this->image_lib->display_errors());
        } else {
            $data = array('state' => true);
        }
        return $data;
	}

	public function form_deskripsi($id_gallery, $id_foto){
		$data['data'] = [
			'id_gallery' => $id_gallery,
			'id_foto' => $id_foto,
			'data2' => $this->fkm->getGalleryFotoKegiatan2($id_gallery, $id_foto)
		];

		$this->form_validation->set_rules('id_gallery', 'Id galery', 'required');
		$this->form_validation->set_rules('id_foto', 'Id foto', 'required');

		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('gallery/form_deskripsi', $data);
		} else {
			//action if valid
			$v = $this->fkm->ubahDataDeskripsi();
			if($v){
				$status = ['state' => 1, 'message' => 'Deskripsi berhasil di ubah', 'desc' => $this->input->post('deskripsi')];
			} else {
				$status = ['state' => 0, 'message' => 'Deskripsi gagal di ubah', 'desc' => $this->input->post('deskripsi')];
			}

			echo json_encode($status);
		}
		
	}

	public function update_deskripsi($id_gallery)
	{
		$data['judul'] = 'Foto Kegiatan';
		$data['gallery'] = $this->fkm->getAllFotoKegiatanFromGallery($id_gallery);

		$this->form_validation->set_rules('id_gallery', 'Id galery', 'required');
		$this->form_validation->set_rules('id_foto', 'Id foto', 'required');

		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('gallery/update_deskripsi', $data);
			$this->load->view('templates/footer');
		} else {
			//action if valid
		}
	}

	public function delete($id_gallery, $id_foto)
	{
		$this->Gallery_model->hapusFotoGallery($id_gallery, $id_foto);
		$this->session->set_flashdata('flash', 'dihapus');
		redirect('gallery/update_deskripsi/'.$id_gallery);
	}

	public function hapus($id_gallery)
	{
		$this->Gallery_model->hapusDataGallery($id_gallery);
		$this->session->set_flashdata('flash', 'dihapus');
		redirect('gallery');
	}

}