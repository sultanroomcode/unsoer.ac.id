<?php 

/**
 * RunningText
 */
class RunningText extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('form_validation');
		$this->load->model('RunningText_model');
		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('login');
		}
	}

	public function index()
	{
		$data['judul'] = 'Running Text';
		$data['RunningText'] = $this->RunningText_model->getAllRunningText();
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('RunningText/index', $data);
		$this->load->view('templates/footer');
	}

	public function ubah($id) 
	{
		$data['judul'] = 'Form Ubah Running Text';
		$data['RunningText'] = $this->RunningText_model->getRunningTextById($id);
		
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');

		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('RunningText/ubah', $data);
			$this->load->view('templates/footer');
		} else {
			$this->RunningText_model->ubahDataRunningText();
			$this->session->set_flashdata('flash', 'diubah');
			redirect('RunningText');
		}
	}
}