<?php 

/**
 * Login
 */
class Login extends CI_Controller
{
	function __construct(){
        parent::__construct();

        if($this->session->userdata('masuk') == TRUE){
            $url = base_url('home');
            redirect($url);
        } else {
        	$this->load->model('Login_model');
        }
    }

	public function index()
	{
		$data['judul'] = 'Login';
		// cek session
		$this->load->view('templates/auth_header', $data);
		$this->load->view('login/index');
		$this->load->view('templates/auth_footer');
	}

	public function auth(){
        $username = htmlspecialchars($this->input->post('username',TRUE),ENT_QUOTES);
        $password = htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);
        $cek_admin = $this->Login_model->auth_admin($username,$password);
 
        if($cek_admin->num_rows() > 0) { //jika login sebagai admin
                $data=$cek_admin->row_array();
                $this->session->set_userdata('masuk',TRUE);
                $this->session->set_userdata('depart',$data['depart']);
                $this->session->set_userdata('depart_detail',$data['depart_detail']);
                if($data['level']=='1'){ //Akses admin
                    $this->session->set_userdata('akses','1');
                    $this->session->set_userdata('ses_id',$data['id']);
                    $this->session->set_userdata('user_access',$data['user_access']);
                    $this->session->set_userdata('user_access_name',$data['nama_access']);
                    $this->session->set_userdata('ses_nama',$data['username']);
                    redirect('/');
 
                 } else { //akses admin
                    $this->session->set_userdata('akses','2');
                    $this->session->set_userdata('ses_id',$data['id']);
                    $this->session->set_userdata('user_access',$data['user_access']);
                    $this->session->set_userdata('user_access_name',$data['nama_access']);
                    $this->session->set_userdata('ses_nama',$data['username']);
                    redirect('/');
                 }
 
        } else { //jika login sebagai mahasiswa
            /*$cek_member = $this->Login_model->auth_member($username,$password);
            if ( $cek_member->num_rows() > 0 ) {
            	$data = $cek_member->row_array();
            	$this->session->set_userdata('masuk',TRUE);
                $this->session->set_userdata('akses','3');
                $this->session->set_userdata('ses_id',$data['id']);
                $this->session->set_userdata('ses_nama',$data['username']);
                redirect('page');
            }else{  // jika username dan password tidak ditemukan atau salah
            	echo "gagal";
                // $url = base_url();
                // echo $this->session->set_flashdata('msg','Username Atau Password Salah');
                // redirect($url);
            }*/
            redirect('/');
        }
 			
    }
 
}