<?php 

/**
 * Slider
 */
class Slider extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Slider_model');
		$this->load->library('form_validation');
		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('login');
		}
	}

	public function index()
	{
		$data['judul'] = 'Slider';  
		$data['slider'] = $this->Slider_model->getAdminAllSlider();

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('slider/index', $data);
		$this->load->view('templates/footer');
	}

	public function tambah() 
	{
		$data['judul'] = 'Form Tambah Slider Baru';
		$data['slider'] = $this->Slider_model->getAdminAllSlider();
		
		$data['user'] = $this->db->get_where('admin', ['username' => $this->session->userdata('ses_nama')])->row_array();

		$this->form_validation->set_rules('judul', 'Judul', 'required');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
 
		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('slider/tambah', $data);
			$this->load->view('templates/footer');
		} else {
			$dataupload = $this->upload_cover();
			if($dataupload['state'] == false){
				$dataupload['upload_data']['file_name'] = 'no-image-slide.png';
			}
			$this->Slider_model->imageUpload = $dataupload['upload_data']['file_name'];
			$data = $this->Slider_model->tambahDataSlider();
			$this->session->set_flashdata('flash', 'ditambahkan');
			redirect('slider');
		}
	}

	public function upload_cover()
    {
        $config['upload_path']          = '../upload/slider';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = time();
        $config['max_size']             = 3000;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('background'))
        {
            $data = array('state' => false, 'error' => $this->upload->display_errors());
        }
        else
        {
            $data = array('state' => true, 'upload_data' => $this->upload->data());
        }
        return $data;
	}


	public function ubah($id_slider) 
	{
		$data['judul'] = 'Form Ubah Data Slider';
		$data['slider'] = $this->Slider_model->getSliderForUpdate($id_slider);
		// $data['kategori'] = $this->Kategori_model->getAllKategori();
		$this->form_validation->set_rules('judul', 'Judul', 'required');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
		
		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('slider/ubah', $data);
			$this->load->view('templates/footer');
		} else {
			// var_dump($this->input->post());
			$upload = $this->input->post('upload_new_file');
			if($upload == 1){
				$dataupload_ubah = $this->upload_cover_ubah();	

				if($dataupload_ubah['state']){
					//$dataupload_ubah['upload_data']['file_name'] = 'no-image.png';
					//delete gambar lama
					//unlink($this->input->post('gambar_lama'));
				}
			}		
			$this->Slider_model->imageUpload = $dataupload_ubah['upload_data']['file_name'];

			$this->Slider_model;
			if($upload == 1){
				$this->Slider_model->ubahDataSlider(true);
			} else {
				$this->Slider_model->ubahDataSlider();
			}
			$this->session->set_flashdata('flash', 'diubah');
			redirect('slider');
		}
	}

	public function upload_cover_ubah()
    {
        $config['upload_path']          = '../upload/slider';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = time();
        $config['max_size']             = 3000;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('background'))
        {
            $data = array('state' => false, 'error' => $this->upload->display_errors());
        }
        else
        {
            $data = array('state' => true, 'upload_data' => $this->upload->data());
        }
        return $data;
	}


	public function hapus($id_slider)
	{
		$this->Slider_model->hapusDataSlider($id_slider);
		$this->session->set_flashdata('flash', 'dihapus');
		redirect('slider');
	}


}