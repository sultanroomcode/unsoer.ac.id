<?php 

/**
 * Kategori
 */
class Kategori extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Kategori_model');
		$this->load->library('form_validation');
		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('login');
		}
	}

	public function index()
	{
		$data['judul'] = 'Kategori';
		$data['kategori'] = $this->Kategori_model->getAllKategori();
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('kategori/index', $data);
		$this->load->view('templates/footer');
	}

	public function tambah () 
	{
		$data['judul'] = 'Form Tambah Kategori';

		$this->form_validation->set_rules('nama_kategori', 'Nama Kategori', 'required');
		

		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('kategori/tambah', $data);
			$this->load->view('templates/footer');
		} else {
			$this->Kategori_model->tambahDataKategori();
			$this->session->set_flashdata('flash', 'ditambahkan');
			redirect('kategori');
		}
	}

	public function detail($id)
	{
		$data['judul'] = 'Detail Data Kategori';
		$data['kategori'] = $this->Kategori_model->getKategoriById($id);
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('kategori/detail', $data);
		$this->load->view('templates/footer');
	}

	public function ubah ($id_kategori) 
	{
		$data['judul'] = 'Form Ubah Data Kategori';
		$data['kategori'] = $this->Kategori_model->getKategoriById($id_kategori);
		$this->form_validation->set_rules('nama_kategori', 'Nama Kategori', 'required');
		
		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('kategori/ubah', $data);
			$this->load->view('templates/footer');
		} else {
			$this->Kategori_model->ubahDataKategori();
			$this->session->set_flashdata('flash', 'diubah');
			redirect('kategori');
		}
	}

	public function hapus($id_kategori)
	{
		$this->Kategori_model->hapusDataKategori($id_kategori);
		$this->session->set_flashdata('flash', 'dihapus');
		redirect('kategori');
	}
}