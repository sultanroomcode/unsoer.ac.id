<?php 
/**
 * 
 */
class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Slider_model');
		$this->load->model('Tentang_kami_model');
		$this->load->library('form_validation');
		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('login');
		}
	}
	
	public function index ($nama = '')
	{
		$data['judul'] = 'Selamat Datang Admin';
		$data['nama'] =$nama;
		$data['slider'] = $this->Slider_model->getAdminAllSlider();
		$data['tentang_kami'] = $this->Tentang_kami_model->getAllTentangKami();
		$data['user'] = $this->db->get_where('admin', ['username' => $this->session->userdata('ses_nama')])->row_array();
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		if($this->session->userdata('masuk') != TRUE) {
			$this->load->view('login/index', $data);// dashboard
  		} else {
			$this->load->view('user/index', $data);// dashboard
		}
		$this->load->view('templates/footer');
	}

}