<?php 

/**
 * Pimpinan
 */
class Pimpinan extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Pimpinan_model');
		$this->load->library('form_validation');
		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('login');
		}
	} 

	public function index()
	{
		$data['judul'] = 'Pimpinan';
		// $data['pimpinan'] = $this->Pimpinan_model->getAllPimpinan();
        $data['pimpinan'] = $this->Pimpinan_model->getAdminAllPimpinan();
		
		if ( $this->input->post('keyword')  ) {
			$data['pimpinan'] = $this->Pimpinan_model->cariDataPimpinan();
		}
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('pimpinan/index', $data);
		$this->load->view('templates/footer');
	}

	public function tambah() 
	{
		$data['judul'] = 'Form Tambah Pimpinan';
		$data['pimpinan'] = $this->Pimpinan_model->getAllPimpinan();
		
		$this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
    	$this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
    	$this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');		      
    	$this->form_validation->set_rules('unit_kerja', 'Unit Kerja', 'required');		
        $this->form_validation->set_rules('jabatan', 'Jabatan', 'required');        
        $this->form_validation->set_rules('pengalaman_organisasi', 'Pengalaman Organisasi', 'required');        
    	$this->form_validation->set_rules('pengalaman_pemilu', 'Pengalaman di Kegiatan Kepemiluan', 'required');

		if ($this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
			$this->load->view('pimpinan/tambah', $data);
			$this->load->view('templates/footer');
			
		} else {
			$dataupload = $this->upload_foto();
			// var_dump($dataupload);
			if($dataupload['state'] == false){
				$dataupload['upload_data']['file_name'] = 'default.png';
			}
			$this->Pimpinan_model->imageUpload = $dataupload['upload_data']['file_name'];
			$data = $this->Pimpinan_model->tambahDataPimpinan();
			$this->session->set_flashdata('flash', 'ditambahkan');
			redirect('pimpinan');
		}
	}

	function upload_foto() {
        $config['upload_path'] 		= '../upload/pimpinan'; //path folder
        $config['allowed_types'] 	= 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['file_name'] 		= time(); //Enkripsi nama yang terupload
        $config['max_size']         = 5120;

 
        $this->load->library('upload',$config);
        if(!empty($_FILES['foto']['name'])){
 
            if ($this->upload->do_upload('foto')){
                $gbr = $this->upload->data();
                //compress
                // var_dump($gbr);
                // var_dump($gbr['file_size']);

                if ($gbr['file_size'] < 312 ) { // no compress
                	// echo " dibawah 1/2 mb";
                	// $doWaterMark = true;
                	
                } else if ($gbr['file_size'] > 312 && $gbr['file_size'] < 1024) {
                	// echo " diatas 1/2 - 1mb";
                	$this->compressImageOne($gbr['file_name']);
                	// $doWaterMark = true;
                } else if ($gbr['file_size'] > 1024 && $gbr['file_size'] < 3072 ) {
                	
                	// echo "di atas 1 mb - 3 mb";
                	$this->compressImageOne($gbr['file_name']);	
                	// $doWaterMark =true;

                } else if ($gbr['file_size'] > 3072 && $gbr['file_size'] < 5120 ) {
                	
                	// echo "di atas 3 mb - 5 mb";
                	$this->compressImageTwo($gbr['file_name']);	
                	// $doWaterMark = true;
                } else if ($gbr['file_size'] > 5120 ) {
                	echo " File terlalu Besar, Max 5 MB";
                	// $doWaterMark = false;
                }
                
                // if($doWaterMark){
	               //  $this->wmtest($gbr['file_name']);				
                // }
	            $data = array('state' => true, 'upload_data' => $gbr);
            } else  {
	            $data = array('state' => false, 'error' => $this->upload->display_errors());
	        }
        } else {
            $data = array('state' => false, 'error' => "Image yang diupload kosong");
        }   
        return $data;         
    }
 
		//Compress Image diatas 1 - 3 mb
	public function compressImageOne($filename)
	{
        $config['image_library']='gd2';
        $config['source_image']='../upload/pimpinan/'.$filename;
        $config['maintain_ratio'] = true;
        $config['master_dim'] = 'height';
        $config['create_thumb']= FALSE;
        $config['quality']= '50%';
        $config['width']= 100;
        $config['height']= 500;

        $this->load->library('image_lib', $config);
        $this->image_lib->clear();
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
	}

		// Compress Image di atas 3 - 5 mb
	public function compressImageTwo($filename)
	{
        $config['image_library']='gd2';
        $config['source_image']='../upload/pimpinan/'.$filename;
        $config['maintain_ratio'] = TRUE;
        $config['master_dim'] = 'height';
        // $config['create_thumb']= FALSE;
        $config['quality'] = '300%';
        $config['width']= 1;
        $config['height']= 500;

        $this->load->library('image_lib', $config);
        $this->image_lib->clear();
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
	}

	public function ubah($id) 
	{
		$data['judul'] = 'Form Ubah Data Pimpinan'; 
		$data['pimpinan'] = $this->Pimpinan_model->getPimpinanById($id);
		
		$this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
        $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
        $this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');        
        $this->form_validation->set_rules('unit_kerja', 'Unit Kerja', 'required');      
        $this->form_validation->set_rules('jabatan', 'Jabatan', 'required');        
        $this->form_validation->set_rules('pengalaman_organisasi', 'Pengalaman Organisasi', 'required');        
        $this->form_validation->set_rules('pengalaman_pemilu', 'Pengalaman di Kegiatan Kepemiluan', 'required');
		
		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
			$this->load->view('pimpinan/ubah', $data);
			$this->load->view('templates/footer');
		} else {
			// var_dump($this->input->post());
			$upload = $this->input->post('upload_new_file');
			if($upload == 1){
				$dataupload_ubah = $this->upload_foto_ubah();	

				if($dataupload_ubah['state'] == false){
					$dataupload_ubah['upload_data']['file_name'] = 'default.png';
				}

				if($dataupload_ubah['state']){
					unlink($this->input->post('gambar_lama'));
				}

				$this->Pimpinan_model->imageUpload = $dataupload_ubah['upload_data']['file_name'];
			}		
			if($upload == 1){
				$this->Pimpinan_model->ubahDataPimpinan(true);
			} else {
				$this->Pimpinan_model->ubahDataPimpinan();
			}
			$this->session->set_flashdata('flash', 'diubah');
			redirect('pimpinan');
		}
	}

	public function upload_foto_ubah() 
	{
        $config['upload_path'] 		= '../upload/pimpinan'; //path folder
        $config['allowed_types'] 	= 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['file_name'] 		= time(); //Enkripsi nama yang terupload
        $config['max_size']         = 5120; // 5 mb
 
        $this->load->library('upload',$config);
        if(!empty($_FILES['foto']['name'])){
 
            if ($this->upload->do_upload('foto')){
                $gbr = $this->upload->data();

                //compress
                if ($gbr['file_size'] < 312 ) { // no compress
                	// echo " dibawah 1/2 mb";
                	// $doWaterMark = true;
                	$this->compressImageOne($gbr['file_name']);

                } else if ($gbr['file_size'] > 312 && $gbr['file_size'] < 1024) {
                	// echo " dibawah 1/2 - 1mb";
                	$this->compressImageOne($gbr['file_name']);
                	// $doWaterMark = true;

                } else if ($gbr['file_size'] > 1024 && $gbr['file_size'] < 3072 ) {
                	
                	// echo "di atas 1 mb - 3 mb";
                	$this->compressImageOne($gbr['file_name']);	
                	// $doWaterMark =true;
                } else if ($gbr['file_size'] > 3072 && $gbr['file_size'] < 5120 ) {
                	
                	// echo "di atas 3 mb - 5 mb";
                	$this->compressImageTwo($gbr['file_name']);	
                	// $doWaterMark = true;
                } else if ($gbr['file_size'] > 5120 ) {
                	echo " File terlalu Besar, Max 5 MB";
                	// $doWaterMark = false;
                }

                // watermark
                // if($doWaterMark){
	               //  $this->wmtest($gbr['file_name']);				
                // }

	            $data = array('state' => true, 'upload_data' => $gbr);
                
            } else  {
	            $data = array('state' => false, 'error' => $this->upload->display_errors());
	        }
        } else {
            $data = array('state' => false, 'error' => "Image yang diupload kosong");
        }   
        return $data;         
    }



	public function detail($id)
	{
		$data['judul'] = 'Detail Data Pimpinan';
		$data['pimpinan'] = $this->Pimpinan_model->getPimpinanById($id);
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('pimpinan/detail', $data);
		$this->load->view('templates/footer');
	}

	public function hapus($id)
	{
		$this->Pimpinan_model->hapusDataPimpinan($id);
		$this->session->set_flashdata('flash', 'dihapus');
		redirect('pimpinan');
	}

}