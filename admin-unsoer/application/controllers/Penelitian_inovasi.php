<?php 
/**
 * 
 */
class Penelitian_inovasi extends CI_Controller
{

	public function penelitian()
	{
		$data['judul'] = 'Penelitian';
		// $data['universitas'] = $this->Profil_model->getAllProfil();
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('penelitian/penelitian', $data);
		$this->load->view('templates/footer');
	}

	public function inovasi()
	{
		$data['judul'] = 'Inovasi';
		// $data['universitas'] = $this->Profil_model->getAllProfil();
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('penelitian/inovasi', $data);
		$this->load->view('templates/footer');
	}

	public function riset_inovasi()
	{
		$data['judul'] = 'Riset & Inovasi';
		// $data['universitas'] = $this->Profil_model->getAllProfil();
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('penelitian/riset_inovasi', $data);
		$this->load->view('templates/footer');
	}

	public function pengabdian_masyarakat()
	{
		$data['judul'] = 'Pengabdian Masyarakat';
		// $data['universitas'] = $this->Profil_model->getAllProfil();
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('penelitian/pengabdian_masyarakat', $data);
		$this->load->view('templates/footer');
	}


}