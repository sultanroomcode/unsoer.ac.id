<?php 

/**
 * Profil
 */
class Profil extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Profil_model');
		
		$this->load->library('form_validation');
		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('login');
		}
	} 

	public function index()
	{
		$data['judul'] = 'Profil';

		$data['visimisi'] = $this->Profil_model->getAllVisimisi();
		$data['idmedia'] = $this->Profil_model->getAllIdmedia();
		$data['sejarah'] = $this->Profil_model->getAllSejarah();

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('profil/index', $data);
		$this->load->view('templates/footer');
	}


	// ubah data Visi & Misi
	public function ubahVM($id) 
	{
		$data['judul'] = 'Form Ubah Data Visi & Misi';
		$data['visimisi'] = $this->Profil_model->getVisimisiById($id);
		
		$this->form_validation->set_rules('visi', 'Visi', 'required');
		$this->form_validation->set_rules('deskripsi_visi', 'Deskripsi Visi', 'required');
		$this->form_validation->set_rules('misi', 'Misi', 'required');
		$this->form_validation->set_rules('deskripsi_misi', 'Deskripsi Misi', 'required');
		
		if ( $this->form_validation->run() == FALSE ) {
			
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('profil/ubahVM', $data);
			$this->load->view('templates/footer');

		} else {
			$this->Profil_model->ubahDataVisimisi();
			$this->session->set_flashdata('flash', 'diubah');
			redirect('profil');
		}
	}


	// ubah data Identitas & Media
	public function ubahIM($id) 
	{
		$data['judul'] = 'Form Ubah Data Identitas & Media';
		$data['idmedia'] = $this->Profil_model->getIdmediaById($id);
		
		$this->form_validation->set_rules('nama_sekolah', 'nama_sekolah', 'required');
		$this->form_validation->set_rules('alamat_sekolah', 'Alamat Sekolah', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required','email');
		$this->form_validation->set_rules('telp', 'Telp', 'required');
		$this->form_validation->set_rules('facebook', 'Facebook', 'required');
		$this->form_validation->set_rules('instagram', 'Instagram', 'required');
		$this->form_validation->set_rules('wa', 'WhastApp', 'required');
		
		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('profil/ubahIM', $data);
			$this->load->view('templates/footer');
		} else {
			$this->Profil_model->ubahDataIdmedia();
			$this->session->set_flashdata('flash', 'diubah');
			redirect('profil');
		}
	}

	// ubah data Sejarah Sekolah
	public function ubahSJ($id) 
	{
		$data['judul'] = 'Form Ubah Data Sejarah Sekolah';
		$data['sejarah'] = $this->Profil_model->getSejarahById($id);
		
		$this->form_validation->set_rules('judul', 'Judul', 'required');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
		$this->form_validation->set_rules('gambar', 'Gambar', 'required');
		
		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('profil/ubahSJ', $data);
			$this->load->view('templates/footer');
		} else {
			$this->Profil_model->ubahDataSejarah();
			$this->session->set_flashdata('flash', 'diubah');
			redirect('profil');
		}
	}
}