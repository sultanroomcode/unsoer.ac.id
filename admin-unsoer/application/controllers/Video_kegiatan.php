<?php 
 
/**
 * Video_kegiatan Controller
 */
class Video_kegiatan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Video_kegiatan_model');
		$this->load->library('form_validation');
		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('login');
		}
	}

	public function index()
	{
		$data['judul'] = 'Video Kegiatan';

		$data['video_kegiatan'] = $this->Video_kegiatan_model->getAllVideoKegiatan();

		if ( $this->input->post('keyword')  ) {
			$data['video_kegiatan'] = $this->Video_kegiatan_model->cariDataVideo();
		}

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('video_kegiatan/index', $data);
		$this->load->view('templates/footer');
	}

	public function tambah() 
	{
		$data['judul'] = 'Form Tambah Video Kegiatan';

		$this->form_validation->set_rules('judul', 'Judul', 'required');
		$this->form_validation->set_rules('link', 'Link / Url', 'required');

		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('video_kegiatan/tambah', $data);
			$this->load->view('templates/footer');
		} else {
			$this->Video_kegiatan_model->tambahDataVideoKegiatan();
			$this->session->set_flashdata('flash', 'ditambahkan');
			redirect('video_kegiatan');
		}
	}

	public function ubah($id) 
	{
		$data['judul'] = 'Form Ubah Data Video';
		$data['video_kegiatan'] = $this->Video_kegiatan_model->getVideoKegiatanById($id);

		$this->form_validation->set_rules('judul', 'Judul', 'required');
		$this->form_validation->set_rules('link', 'Link / Url', 'required');

		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('video_kegiatan/ubah', $data);
			$this->load->view('templates/footer');
		} else {
			$this->Video_kegiatan_model->ubahDataVideoKegiatan();
			$this->session->set_flashdata('flash', 'diubah');
			redirect('video_kegiatan');
		}
	}

	public function hapus($id)
	{
		$this->Video_kegiatan_model->hapusDataVideoKegiatan($id);
		$this->session->set_flashdata('flash', 'dihapus');
		redirect('video_kegiatan');
	}

}