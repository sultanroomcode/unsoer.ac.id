<?php 
 
/**
 * Upload_foto Controller
 */
class Upload_foto extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Upload_foto_model');
		$this->load->library('form_validation');
		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('login');
		}
	}

	public function index()
	{
		$data['judul'] = 'Upload Foto Kegiatan';

		$data['upload_foto'] = $this->Upload_foto_model->getAllFoto();

		if ( $this->input->post('keyword')  ) {
			$data['upload_foto'] = $this->Upload_foto_model->cariDataFotoKegiatan();
		}

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('upload_foto/index', $data);
		$this->load->view('templates/footer');
	}

	public function tambah() 
	{
		$data['judul'] = 'Form Upload Foto ';
		$data['upload_foto'] = $this->Upload_foto_model->getAllFoto();
		
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');		

		if ($this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
			$this->load->view('upload_foto/tambah', $data);
			$this->load->view('templates/footer');
			
		} else {
			$dataupload = $this->upload_foto();
			// var_dump($dataupload);
			if($dataupload['state'] == false){
				$dataupload['upload_data']['file_name'] = 'default.png';
			}
			$this->Upload_foto_model->imageUpload = $dataupload['upload_data']['file_name'];
			$data = $this->Upload_foto_model->tambahDataFoto();
			$this->session->set_flashdata('flash', 'ditambahkan');
			redirect('upload_foto');
		}
	}

	function upload_foto() {
        $config['upload_path'] 		= '../upload/upload-foto'; //path folder
        $config['allowed_types'] 	= 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['file_name'] 		= time(); //Enkripsi nama yang terupload
        $config['max_size']         = 5120;

 
        $this->load->library('upload',$config);
        if(!empty($_FILES['file_foto']['name'])){
 
            if ($this->upload->do_upload('file_foto')){
                $gbr = $this->upload->data();
                //compress
                // var_dump($gbr);
                // var_dump($gbr['file_size']);

                if ($gbr['file_size'] < 312 ) { // no compress
                	// echo " dibawah 1/2 mb";
                	// $doWaterMark = true;
                	
                } else if ($gbr['file_size'] > 312 && $gbr['file_size'] < 1024) {
                	// echo " diatas 1/2 - 1mb";
                	$this->compressImageOne($gbr['file_name']);
                	// $doWaterMark = true;
                } else if ($gbr['file_size'] > 1024 && $gbr['file_size'] < 3072 ) {
                	
                	// echo "di atas 1 mb - 3 mb";
                	$this->compressImageOne($gbr['file_name']);	
                	// $doWaterMark =true;

                } else if ($gbr['file_size'] > 3072 && $gbr['file_size'] < 5120 ) {
                	
                	// echo "di atas 3 mb - 5 mb";
                	$this->compressImageTwo($gbr['file_name']);	
                	// $doWaterMark = true;
                } else if ($gbr['file_size'] > 5120 ) {
                	echo " File terlalu Besar, Max 5 MB";
                	// $doWaterMark = false;
                }
                
                // if($doWaterMark){
	               //  $this->wmtest($gbr['file_name']);				
                // }
	            $data = array('state' => true, 'upload_data' => $gbr);
            } else  {
	            $data = array('state' => false, 'error' => $this->upload->display_errors());
	        }
        } else {
            $data = array('state' => false, 'error' => "Image yang diupload kosong");
        }   
        return $data;         
    }

    		//Compress Image diatas 1 - 3 mb
	public function compressImageOne($filename)
	{
        $config['image_library']='gd2';
        $config['source_image']='../upload/upload-foto/'.$filename;
        $config['maintain_ratio'] = true;
        $config['master_dim'] = 'height';
        $config['create_thumb']= FALSE;
        $config['quality']= '50%';
        $config['width']= 100;
        $config['height']= 500;

        $this->load->library('image_lib', $config);
        $this->image_lib->clear();
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
	}

		// Compress Image di atas 3 - 5 mb
	public function compressImageTwo($filename)
	{
        $config['image_library']='gd2';
        $config['source_image']='../upload/upload-foto/'.$filename;
        $config['maintain_ratio'] = TRUE;
        $config['master_dim'] = 'height';
        // $config['create_thumb']= FALSE;
        $config['quality'] = '300%';
        $config['width']= 1;
        $config['height']= 500;

        $this->load->library('image_lib', $config);
        $this->image_lib->clear();
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
	}

	public function ubah($id) 
	{
		$data['judul'] = 'Form Ubah Data Foto'; 
		$data['upload_foto'] = $this->Upload_foto_model->getUploadFotoById($id);
		
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
		
		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
			$this->load->view('upload_foto/ubah', $data);
			$this->load->view('templates/footer');
		} else {
			// var_dump($this->input->post());
			$upload = $this->input->post('upload_new_file');

			// var_dump($upload);
			if($upload == 1){
				$dataupload_ubah = $this->upload_foto_ubah();	

				if($dataupload_ubah['state'] == false){
					$dataupload_ubah['upload_data']['file_name'] = 'default.png';
				}

				if($dataupload_ubah['state']){
					unlink($this->input->post('gambar_lama'));
				}

				$this->Upload_foto_model->imageUpload = $dataupload_ubah['upload_data']['file_name'];
			}		
			if($upload == 1){
				$this->Upload_foto_model->ubahDataUploadFoto(true);
			} else {
				$this->Upload_foto_model->ubahDataUploadFoto();
			}
			$this->session->set_flashdata('flash', 'diubah');
			redirect('upload_foto');
		}
	}

	public function upload_foto_ubah() 
	{
        $config['upload_path'] 		= '../upload/upload-foto'; //path folder
        $config['allowed_types'] 	= 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['file_name'] 		= time(); //Enkripsi nama yang terupload
        $config['max_size']         = 5120; // 5 mb
 
        $this->load->library('upload',$config);
        if(!empty($_FILES['file_foto']['name'])){
 
            if ($this->upload->do_upload('file_foto')){
                $gbr = $this->upload->data();

                //compress
                if ($gbr['file_size'] < 312 ) { // no compress
                	// echo " dibawah 1/2 mb";
                	// $doWaterMark = true;
                	$this->compressImageOne($gbr['file_name']);

                } else if ($gbr['file_size'] > 312 && $gbr['file_size'] < 1024) {
                	// echo " dibawah 1/2 - 1mb";
                	$this->compressImageOne($gbr['file_name']);
                	// $doWaterMark = true;

                } else if ($gbr['file_size'] > 1024 && $gbr['file_size'] < 3072 ) {
                	
                	// echo "di atas 1 mb - 3 mb";
                	$this->compressImageOne($gbr['file_name']);	
                	// $doWaterMark =true;
                } else if ($gbr['file_size'] > 3072 && $gbr['file_size'] < 5120 ) {
                	
                	// echo "di atas 3 mb - 5 mb";
                	$this->compressImageTwo($gbr['file_name']);	
                	// $doWaterMark = true;
                } else if ($gbr['file_size'] > 5120 ) {
                	echo " File terlalu Besar, Max 5 MB";
                	// $doWaterMark = false;
                }

                // watermark
                // if($doWaterMark){
	               //  $this->wmtest($gbr['file_name']);				
                // }

	            $data = array('state' => true, 'upload_data' => $gbr);
                
            } else  {
	            $data = array('state' => false, 'error' => $this->upload->display_errors());
	        }
        } else {
            $data = array('state' => false, 'error' => "Image yang diupload kosong");
        }   
        return $data;         
    }

    public function hapus($id)
	{
		$this->Upload_foto_model->hapusDataFoto($id);
		$this->session->set_flashdata('flash', 'dihapus');
		redirect('upload_foto');
	}


}