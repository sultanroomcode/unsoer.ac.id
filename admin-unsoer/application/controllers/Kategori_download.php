<?php 

/**
 * Kategori_download
 */
class Kategori_download extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Kategori_download_model');
		$this->load->library('form_validation');
		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('login');
		}
	}

	public function index()
	{
		$data['judul'] = 'Kategori Download';
		$data['kategori'] = $this->Kategori_download_model->getAllKategoriDownload();
		$this->load->view('templates/header', $data);
		$this->load->view('kategori_download/index', $data);
		$this->load->view('templates/footer');
	}

	public function tambah() 
	{
		$data['judul'] = 'Form Tambah Kategori File';

		$this->form_validation->set_rules('nama_kategori', 'Nama Kategori', 'required');
		
		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('kategori_download/tambah', $data);
			$this->load->view('templates/footer');
		} else {
			$this->Kategori_download_model->tambahDataKategoriDownload();
			$this->session->set_flashdata('flash', 'ditambahkan');
			redirect('kategori_download');
		}
	}

	public function detail($id)
	{
		$data['judul'] = 'Detail Data Kategori_download';
		$data['kategori_download'] = $this->Kategori_download_model->getKategoriDownloadById($id);
		$this->load->view('templates/header', $data);
		$this->load->view('kategori_download/detail', $data);
		$this->load->view('templates/footer');
	}

	public function ubah($id_kategori_download) 
	{
		$data['judul'] = 'Form Ubah Data Kategori File';
		$data['kategori'] = $this->Kategori_download_model->getKategoriDownloadById($id_kategori_download);
		$this->form_validation->set_rules('nama_kategori', 'Nama Kategori', 'required');
		
		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('kategori_download/ubah', $data);
			$this->load->view('templates/footer');
		} else {
			$this->Kategori_download_model->ubahDataKategoriDownload();
			$this->session->set_flashdata('flash', 'diubah');
			redirect('kategori_download');
		}
	}

	public function hapus($id_kategori_download)
	{
		$this->Kategori_download_model->hapusDataKategoriDownload($id_kategori_download);
		$this->session->set_flashdata('flash', 'dihapus');
		redirect('kategori_download');
	}
}