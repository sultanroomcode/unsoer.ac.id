<?php 
 
/**
 * Jurnal Controller
 */
class Jurnal_a extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Jurnal_model');
		$this->load->library('form_validation');
		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('login');
		}
	}

	public function index ()
	{
		$data['judul'] = 'Jurnal';
		$data['jurnal'] = $this->Jurnal_model->getAllJurnal();

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('jurnal/index', $data);
		$this->load->view('templates/footer');
	}

	public function tambah() 
	{
		$data['judul'] = 'Form Tambah Jurnal';
		$data['jurnal'] = $this->Jurnal_model->getAllJurnal();

		$data['user'] = $this->db->get_where('admin', ['username' => $this->session->userdata('ses_nama')])->row_array();

		$this->form_validation->set_rules('judul_jurnal', 'Judul Jurnal', 'required');
		$this->form_validation->set_rules('penulis', 'Penulis', 'required');
		$this->form_validation->set_rules('publikasi', 'Publikasi', 'required');
		$this->form_validation->set_rules('link', 'Link', 'required');
		$this->form_validation->set_rules('tahun_terbit', 'Tahun Terbit', 'required');
		$this->form_validation->set_rules('volume', 'Volume', 'required');

		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('jurnal/tambah', $data);
			$this->load->view('templates/footer');
		} else {
			$dataupload = $this->upload_file();
			if($dataupload['state']){
				$this->Jurnal_model->fileUpload = $dataupload['upload_data']['file_name'];
				$this->Jurnal_model->tambahDataJurnal();
				$this->session->set_flashdata('flash', 'ditambahkan');
				redirect('jurnal');
			} else {
				// var_dump($dataupload);
				echo "gagal upload";
			}
		}
	}

	public function ubah($id) 
	{
		$data['judul'] = 'Form Ubah Jurnal';
		$data['jurnal'] = $this->Jurnal_model->getJurnal($id);

		$data['user'] = $this->db->get_where('admin', ['username' => $this->session->userdata('ses_nama')])->row_array();

		$this->form_validation->set_rules('judul_jurnal', 'Judul Jurnal', 'required');
		$this->form_validation->set_rules('penulis', 'Penulis', 'required');
		$this->form_validation->set_rules('publikasi', 'Publikasi', 'required');
		$this->form_validation->set_rules('link', 'Link', 'required');
		$this->form_validation->set_rules('tahun_terbit', 'Tahun Terbit', 'required');
		$this->form_validation->set_rules('volume', 'Volume', 'required');

		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('jurnal/ubah', $data);
			$this->load->view('templates/footer');
		} else {
			$dataupload = $this->upload_file(false);
			if($dataupload['state']){
				$this->Jurnal_model->fileUpload = $dataupload['upload_data']['file_name'];
				$this->Jurnal_model->tambahDataJurnal();
				$this->session->set_flashdata('flash', 'ditambahkan');
				redirect('jurnal');
			} else {
				$this->load->view('templates/header', $data);
				$this->load->view('templates/sidebar', $data);
				$this->load->view('templates/topbar', $data);
				$this->load->view('jurnal/ubah', $data);
				$this->load->view('templates/footer');
			}
		}
	}

	public function upload_file($new = true, $imgold='')
    {
        $config['upload_path']          = '../upload/jurnal';
        $config['allowed_types']        = 'doc|docx|pdf|xls|xlsx|odt';
        $config['file_name']            = url_title($this->input->post('judul_jurnal', true), 'dash', true);
        $config['max_size']             = 25000; // 25MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        if(!$new){
        	//update mode
        	// rename old file
        }

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('upload_jurnal'))
        {
             $data = array('state' => false, 'error' => $this->upload->display_errors());
        }
        else
        {
            $data = array('state' => true, 'upload_data' => $this->upload->data());
        }
        return $data;
	}

	public function hapus($id)
	{
		$this->Jurnal_model->hapusDataJurnal($id);
		$this->session->set_flashdata('flash', 'dihapus');
		redirect('jurnal');
	}
}