<?php
class Dashboard extends CI_Controller{
  function __construct(){
    parent::__construct();
    //validasi jika user belum login
    if($this->session->userdata('masuk') != TRUE){
            $url = base_url('');
            redirect($url);
        }
  }
 
  function index(){
    $data['judul'] = 'Dashboard';
    $this->load->view('templates/header', $data);
    $this->load->view('dashboard/index');
    $this->load->view('templates/footer');
  }
 
  function data_member(){
    // function ini hanya boleh diakses oleh admin dan dosen
    if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2'){
      $this->load->view('v_member');
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
 
  }
 
  function input_nilai(){
    // function ini hanya boleh diakses oleh admin dan dosen
    if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2'){
      $this->load->view('v_input_nilai');
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
 
  function krs(){
    // function ini hanya boleh diakses oleh admin dan member
    if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='3'){
      $this->load->view('v_krs');
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
  function lhs(){
    // function ini hanya boleh diakses oleh admin dan member
    if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='3'){
      $this->load->view('v_lhs');
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }

  function logout() {
        $this->session->sess_destroy();
        $url=base_url('');
        redirect($url);
    }
}