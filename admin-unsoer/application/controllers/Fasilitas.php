<?php 
 
/**
 * Fasilitas
 */
class Fasilitas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Fasilitas_model');
		$this->load->library('form_validation');
		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('login');
		}

		// jika tidak sama dengan universitas
		// if ($this->session->userdata('user_access') !== 1){
		// 	redirect('user');
		// }
	}

	public function index ()
	{
		$data['judul'] = 'Fasilitas';
		$data['fasilitas'] = $this->Fasilitas_model->getAllFasilitas();

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('fasilitas/index', $data);
		$this->load->view('templates/footer');
	}

	public function tambah() 
	{
		$data['judul'] = 'Form Tambah Fasilitas Baru';
		$data['fasilitas'] = $this->Fasilitas_model->getAllFasilitas();
		$data['kategori'] = $this->Fasilitas_model->getAllKategoriFasilitas();
		
		$this->form_validation->set_rules('nama_fasilitas', 'Nama Fasilitas', 'required');
		$this->form_validation->set_rules('kategori_fasilitas', 'Kategori Fasilitas', 'required');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
 
		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('fasilitas/tambah', $data);
			$this->load->view('templates/footer');
		} else {
			$dataupload = $this->upload_cover();
			if($dataupload['state'] == false){
				$dataupload['upload_data']['file_name'] = 'no-image.png';
			}
			$this->Fasilitas_model->imageUpload = $dataupload['upload_data']['file_name'];
			$data = $this->Fasilitas_model->tambahDataFasilitas();
			$this->session->set_flashdata('flash', 'ditambahkan');
			redirect('fasilitas');
		}
	}

	public function upload_cover()
    {
        $config['upload_path']          = '../upload/fasilitas';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = time();
        $config['max_size']             = 3000;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        // nama field file gambar
        if ( ! $this->upload->do_upload('foto'))
        {
            $data = array('state' => false, 'error' => $this->upload->display_errors());
        }
        else
        {
            $data = array('state' => true, 'upload_data' => $this->upload->data());
        }
        return $data;
	}

	public function fasilitashapus($id)
	{
		$this->Fasilitas_model->hapusDataFasilitas($id);
		$this->session->set_flashdata('flash', 'dihapus');
		redirect('fasilitas');
	}


	public function kategori()  
	{
		$data['judul'] = 'Kategori Fasilitas';
		$data['kategori'] = $this->Fasilitas_model->getAllKategoriFasilitas();
		// $data['access'] = $this->Admin_model->getAllUserAccess();

		$this->form_validation->set_rules('nama_kategori', 'Nama Kategori', 'required');

		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('fasilitas/kategori', $data);
			$this->load->view('templates/footer');
		} else {
			$this->Fasilitas_model->tambahDataKategori();
			$this->session->set_flashdata('flash', 'ditambahkan');
			redirect('fasilitas/kategori');
		}
	}

	public function kategorihapus($id)
	{
		$this->Fasilitas_model->hapusDataKategori($id);
		$this->session->set_flashdata('flash', 'dihapus');
		redirect('fasilitas/kategori');
	}
}