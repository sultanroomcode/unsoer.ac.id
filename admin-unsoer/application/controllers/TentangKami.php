<?php 
 
/**
 * TentangKami Controller
 */
class TentangKami extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Tentang_kami_model');
		$this->load->library('form_validation');
		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('login');
		}
	}

	public function index()
	{
		$data['judul'] = 'Profil';
		$data['tentang_kami'] = $this->Tentang_kami_model->getAllTentangKami();
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('home/index', $data);
		$this->load->view('templates/footer');
	}

	// public function tambah () 
	// {
	// 	$data['judul'] = 'Form Tambah Data TentangKami';

	// 	$this->form_validation->set_rules('nama', 'Nama', 'required');
	// 	$this->form_validation->set_rules('nrp', 'NRP', 'required|numeric');
	// 	$this->form_validation->set_rules('email', 'Email', 'required|valid_email');

	// 	if ( $this->form_validation->run() == FALSE ) {
	// 		$this->load->view('templates/header', $data);
	// 		$this->load->view('TentangKami/tambah', $data);
	// 		$this->load->view('templates/footer');
	// 	} else {
	// 		$this->TentangKami_model->tambahDataTentangKami();
	// 		$this->session->set_flashdata('flash', 'ditambahkan');
	// 		redirect('TentangKami');
	// 	}
	// }

	// public function hapus($id)
	// {
	// 	$this->TentangKami_model->hapusDataTentangKami($id);
	// 	$this->session->set_flashdata('flash', 'dihapus');
	// 	redirect('TentangKami');
	// }

	public function detail($id)
	{
		$data['judul'] = 'Detail Data Profil';
		$data['tentang_kami'] = $this->TentangKami_model->getTentangKamiById($id);
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('tentang_kami/detail', $data);
		$this->load->view('templates/footer');
	}

	public function ubah($id) 
	{
		$data['judul'] = 'Form Ubah Data Profil';
		$data['tentang_kami'] = $this->Tentang_kami_model->getTentangKamiById($id);
		$this->form_validation->set_rules('judul', 'Judul', 'required');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');

		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('tentangkami/ubah', $data);
			$this->load->view('templates/footer');
		} else { 
			$this->Tentang_kami_model->ubahDataTentangKami();
			$this->session->set_flashdata('flash', 'diubah');
			redirect('home');
		}
	}
}