<?php 
 
/**
 * Link Controller
 */
class Link extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Link_model');
		$this->load->library('form_validation');
		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('login');
		}
	}

	public function index ()
	{
		$data['judul'] = 'Link';
		$data['link'] = $this->Link_model->getAllLink();

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('link/index', $data);
		$this->load->view('templates/footer');
	}

	public function tambah() 
	{
		$data['judul'] = 'Form Tambah Data Link';

		$this->form_validation->set_rules('judul', 'Judul', 'required');
		$this->form_validation->set_rules('url', 'Url', 'required');

		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('link/tambah', $data);
			$this->load->view('templates/footer');
		} else {
			$this->Link_model->tambahDataLink();
			$this->session->set_flashdata('flash', 'ditambahkan');
			redirect('link');
		}
	}

	public function ubah($id_link) 
	{
		$data['judul'] = 'Form Ubah Data Link';
		$data['link'] = $this->Link_model->getLinkById($id_link);

		$this->form_validation->set_rules('judul', 'Judul', 'required');
		$this->form_validation->set_rules('url', 'Url', 'required');

		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('link/ubah', $data);
			$this->load->view('templates/footer');
		} else {
			$this->Link_model->ubahDataLink();
			$this->session->set_flashdata('flash', 'diubah');
			redirect('link');
		}
	}


	public function hapus($id_link)
	{
		$this->Link_model->hapusDataLink($id_link);
		$this->session->set_flashdata('flash', 'dihapus');
		redirect('link');
	}


}
