<?php 
 
/**
 * pengumuman
 */
class Pengumuman extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Pengumuman_model');
		$this->load->library('form_validation');
		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('login');
		}
	}

	public function index ()
	{
		$data['judul'] = 'Pengumuman';
		$data['pengumuman'] = $this->Pengumuman_model->getAllPengumuman();

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('pengumuman/index', $data);
		$this->load->view('templates/footer');
	}

	public function tambah() 
	{ 
		$data['judul'] = 'Form Tambah Pengumuman';
		$data['pengumuman'] = $this->Pengumuman_model->getAllPengumuman();
		$data['user'] = $this->db->get_where('admin', ['username' => $this->session->userdata('ses_nama')])->row_array();

		$this->form_validation->set_rules('judul', 'Judul', 'required');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');

		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('pengumuman/tambah', $data);
			$this->load->view('templates/footer');
		} else {
			$dataupload = $this->upload_file();
			if($dataupload['state']){
				$this->Pengumuman_model->fileUpload = $dataupload['upload_data']['file_name'];
				$this->Pengumuman_model->tambahDataPengumuman();
				$this->session->set_flashdata('flash', 'ditambahkan');
				redirect('pengumuman');
			} else {
				// var_dump($dataupload);
				echo "gagal upload";
			}
		}
	}

	public function upload_file()
    {
        $config['upload_path']          = '../upload/pengumuman';
        $config['allowed_types']        = 'doc|docx|pdf|xls|xlsx|odt';
        $config['file_name']            = url_title($this->input->post('judul', true), 'dash', true);
        $config['max_size']             = 25000; // 25MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('file_pendukung'))
        {
             $data = array('state' => false, 'error' => $this->upload->display_errors());
        }
        else
        {
            $data = array('state' => true, 'upload_data' => $this->upload->data());
        }
        return $data;
	}

	public function detail($id)
	{
		$data['judul'] = 'Detail Pengumuman';
		$data['pengumuman'] = $this->Pengumuman_model->getPengumumanById($id);
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('pengumuman/detail', $data);
		$this->load->view('templates/footer');
	}

	public function ubah($id) 
	{
		$data['judul'] = 'Form Ubah Data Pengumuman'; 
		$data['pengumuman'] = $this->Pengumuman_model->getPengumumanById($id);
		
		$this->form_validation->set_rules('judul', 'Judul', 'required');
		$this->form_validation->set_rules('kategori', 'Kategori', 'required');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
		
		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
			$this->load->view('pengumuman/ubah', $data);
			$this->load->view('templates/footer');
		} else {
			// var_dump($this->input->post());
			$upload = $this->input->post('upload_new_file');
			if($upload == 1){
				$dataupload_ubah = $this->upload_cover_ubah();	

				if($dataupload_ubah['state'] == false){
					$dataupload_ubah['upload_data']['file_name'] = 'no-image.png';
				}

				if($dataupload_ubah['state']){
					unlink($this->input->post('gambar_lama'));
				}

				$this->Pengumuman_model->imageUpload = $dataupload_ubah['upload_data']['file_name'];
			}		
			if($upload == 1){
				$this->Pengumuman_model->ubahDataPengumuman(true);
			} else {
				$this->Pengumuman_model->ubahDataPengumuman();
			}
			$this->session->set_flashdata('flash', 'diubah');
			redirect('pengumuman');
		}
	}

	public function upload_cover_ubah() 
	{
        $config['upload_path'] 		= '../upload/pengumuman'; //path folder
        $config['allowed_types']    = 'doc|docx|pdf|xls|xlsx|odt';
        $config['file_name'] 		= time(); //Enkripsi nama yang terupload
        $config['max_size']         = 5120; // 5 mb
 
        $this->load->library('upload',$config);
        if(!empty($_FILES['file_pendukung']['name'])){
 
            if ($this->upload->do_upload('file_pendukung')){
                $gbr = $this->upload->data();

                //compress
                if ($gbr['file_size'] < 312 ) { // no compress
                	// echo " dibawah 1/2 mb";
                	// $doWaterMark = true;
                	$this->compressImageOne($gbr['file_name']);

                } else if ($gbr['file_size'] > 312 && $gbr['file_size'] < 1024) {
                	// echo " dibawah 1/2 - 1mb";
                	$this->compressImageOne($gbr['file_name']);
                	// $doWaterMark = true;

                } else if ($gbr['file_size'] > 1024 && $gbr['file_size'] < 3072 ) {
                	
                	// echo "di atas 1 mb - 3 mb";
                	$this->compressImageOne($gbr['file_name']);	
                	// $doWaterMark =true;
                } else if ($gbr['file_size'] > 3072 && $gbr['file_size'] < 5120 ) {
                	
                	// echo "di atas 3 mb - 5 mb";
                	$this->compressImageTwo($gbr['file_name']);	
                	// $doWaterMark = true;
                } else if ($gbr['file_size'] > 5120 ) {
                	echo " File terlalu Besar, Max 5 MB";
                	// $doWaterMark = false;
                }

                // watermark
                if($doWaterMark){
	                $this->wmtest($gbr['file_name']);				
                }

	            $data = array('state' => true, 'upload_data' => $gbr);
                
            } else  {
	            $data = array('state' => false, 'error' => $this->upload->display_errors());
	        }
        } else {
            $data = array('state' => false, 'error' => "Image yang diupload kosong");
        }   
        return $data;         
    }

    public function hapus($id)
	{
		$this->Pengumuman_model->hapusDataPengumuman($id);
		$this->session->set_flashdata('flash', 'dihapus');
		redirect('pengumuman');
	}
}