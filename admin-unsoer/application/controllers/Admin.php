<?php 
 
/**
 * Admin Controller
 */
class Admin extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Admin_model');
		$this->load->library('form_validation');
		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('login');
		}

		// jika tidak sama dengan universitas
		// if ($this->session->userdata('user_access') !== 1){
		// 	redirect('user');
		// }
	}

	public function index ()
	{
		$data['judul'] = 'Daftar Admin';
		$data['admin'] = $this->Admin_model->getAllAdmin();
		if ( $this->input->post('keyword')  ) {
			$data['admin'] = $this->Admin_model->cariDataAdmin();
		}
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('admin/index', $data);
		$this->load->view('templates/footer');
	}

	public function tambah()  
	{
		$data['judul'] = 'Form Tambah Data Admin';
		$data['access'] = $this->Admin_model->getAllUserAccess();

		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('depart', 'Badan Universitas', 'required');
		$this->form_validation->set_rules('depart_detail', 'Nama Badang', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('user_access', 'User Access', 'required');

		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('admin/tambah', $data);
			$this->load->view('templates/footer');
		} else {
			$this->Admin_model->tambahDataAdmin();
			$this->session->set_flashdata('flash', 'ditambahkan');
			redirect('admin');
		}
	}


	public function detail($id)
	{
		$data['judul'] = 'Detail Data Admin';
		$data['admin'] = $this->Admin_model->getAdminById($id);
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('admin/detail', $data);
		$this->load->view('templates/footer');
	}

	public function ubah ($id) 
	{
		$data['judul'] = 'Form Ubah Data Admin';
		$data['admin'] = $this->Admin_model->getAdminById($id);
		
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('nrp', 'NRP', 'required|numeric');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');

		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('admin/ubah', $data);
			$this->load->view('templates/footer');
		} else {
			$this->Admin_model->ubahDataAdmin();
			$this->session->set_flashdata('flash', 'diubah');
			redirect('home');
		}
	}

	public function hapus($id)
	{
		$this->Admin_model->hapusDataAdmin($id);
		$this->session->set_flashdata('flash', 'dihapus');
		redirect('home');
	}

	public function user_access()  
	{
		$data['judul'] = 'Tambah Data User Access Baru';
		$data['access'] = $this->Admin_model->getAllUserAccess();

		$this->form_validation->set_rules('nama_access', 'Nama Access', 'required');

		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('admin/user_access', $data);
			$this->load->view('templates/footer');
		} else {
			$this->Admin_model->tambahDataUserAccess();
			$this->session->set_flashdata('flash', 'ditambahkan');
			redirect('admin/user_access');
		}
	}
}