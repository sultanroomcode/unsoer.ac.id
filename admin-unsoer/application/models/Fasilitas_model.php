<?php  

/**
 * Fasilitas
 */
class Fasilitas_model extends CI_model {
	
	public $imageUpload;
	public function getAllFasilitas ()
	{
		$this->db->join('kategori_fasilitas', 'fasilitas.kategori_fasilitas = kategori_fasilitas.id');
		return $this->db->get('fasilitas')->result_array();
	}

	public function tambahDataFasilitas() 
	{
		$data = [
			"nama_fasilitas" => $this->input->post('nama_fasilitas', true),
			"kategori_fasilitas" => $this->input->post('kategori_fasilitas', true),
			"deskripsi" => $this->input->post('deskripsi', true),
			"create_by" => $this->input->post('create_by', true),
			"create_time" => $this->input->post('create_time', true),
			"foto" => $this->imageUpload,
		];
		$this->db->insert('fasilitas', $data);
	}

	public function hapusDataFasilitas($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('fasilitas');
	}



	public function getAllKategoriFasilitas()
	{
		return $this->db->get('kategori_fasilitas')->result_array();
	}

	public function tambahDataKategori() 
	{
		$data = [
			"id" => $this->input->post('id', true),
			"nama_kategori" => $this->input->post('nama_kategori', true),
			"create_by" => $this->input->post('create_by', true),
			"create_time" => $this->input->post('create_time', true),
		];
		$this->db->insert('kategori_fasilitas', $data);
	}

	public function hapusDataKategori($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('kategori_fasilitas');
	}
}