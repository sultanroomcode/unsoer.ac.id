<?php  

/**
 * Link
 */
class Link_model extends CI_model {
	
	public function getAllLink()
	{
		return $this->db->get('link')->result_array();
	}


	public function getLinkById($id_link)
	{
		$this->db->where(['id_link' => $id_link]);
		return $this->db->get('link')->row_array();
	}

	public function tambahDataLink() 
	{
		$data = [
			"judul" => $this->input->post('judul', true),
			"url" => $this->input->post('url', true),
		];
		$this->db->insert('link', $data);
	}

	public function ubahDataLink() 
	{
		$data = [
			"judul" => $this->input->post('judul', true),
			"url" => $this->input->post('url', true),
		];

		$this->db->where('id_link', $this->input->post('id_link'));
		$this->db->update('link', $data);
	}

	public function hapusDataLink($id_link)
	{
		$this->db->where('id_link', $id_link);
		$this->db->delete('link');
	}

}