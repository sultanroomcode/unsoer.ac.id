<?php 

/**
 * Kategori
 */
class Kategori_model extends CI_model
{
	
	public function getAllKategori()
	{
		return $this->db->get('kategori')->result_array();
	}

	public function tambahDataKategori() 
	{
		$data = [
			"id_kategori" => $this->input->post('id_kategori', true),
			"nama_kategori" => $this->input->post('nama_kategori', true),
			"create_by" => $this->input->post('create_by', true),
			"create_time" => $this->input->post('create_time', true),
		];
		$this->db->insert('kategori', $data);
	}

	public function getKategoriById($id_kategori)
	{
		return $this->db->get_where('kategori', ['id_kategori' => $id_kategori])->row_array();
	}


	public function ubahDataKategori() 
	{
		$data = [
			"nama_kategori" => $this->input->post('nama_kategori', true),
		];

		$this->db->where('id_kategori', $this->input->post('id_kategori'));
		$this->db->update('kategori', $data);
	}

	public function hapusDataKategori($id_kategori)
	{
		$this->db->where('id_kategori', $id_kategori);
		$this->db->delete('kategori');
	}

}