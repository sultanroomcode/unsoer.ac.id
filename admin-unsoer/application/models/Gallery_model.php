<?php 

/**
 * Gallery
 */
class Gallery_model extends CI_model {
	
	public function getAllGallery()
	{
		$this->db->order_by('gallery.create_time', 'DESC');
		return $this->db->get('gallery')->result_array();
	}

	
	public function tambahDataGallery() 
	{
		$data = [
			// "id_gallery" => $this->input->post('id_gallery', true),
			"judul" => $this->input->post('judul', true),
			"deskripsi" => $this->input->post('deskripsi', true),
			"cover_album" => $this->imageUpload,
			"create_by" => $this->input->post('create_by', true),
			"create_time" => $this->input->post('create_time', true),
			"update_by" => $this->input->post('update_by', true),
			"update_time" => $this->input->post('update_time', true),
		];
		return $this->db->insert('gallery', $data);
	}

	public function ubahDataGallery($upload=false) 
	{
		$data = [
			"judul" => $this->input->post('judul', true),
			"deskripsi" => $this->input->post('deskripsi', true),
			// "cover_album" => $this->imageUpload,
			"create_by" => $this->input->post('create_by', true),
			"create_time" => $this->input->post('create_time', true),
			"update_by" => $this->input->post('update_by', true),
			"update_time" => $this->input->post('update_time', true),
		];

		if($upload && $this->imageUpload !== null){
			$data["cover_album"] = $this->imageUpload;
		}

		$this->db->where('id_gallery', $this->input->post('id_gallery'));
		$this->db->update('gallery', $data);
	}

	public function getGalleryForUpdate($id_gallery)
	{
		$this->db->where(['id_gallery' => $id_gallery]);
		return $this->db->get('gallery')->row_array();
	}

	public function getGalleryUploadFoto($id_gallery)
	{
		$this->db->where(['id_gallery' => $id_gallery]);
		return $this->db->get('gallery')->row_array();
	}


	public function upload($inputfilename, $newname)
	{
        $this->load->library('upload', $this->dataconfig($newname));

        if (!$this->upload->do_upload($inputfilename))
        {
            $data = ['state' => false, 'error' => $this->upload->display_errors()];
        }
        else
        {
            $data = ['state' => true, 'upload_data' => $this->upload->data()];
        }

        return $data;
	}

    public function dataconfig($newname){
        $config['upload_path']          = '../upload/gallery/foto-kegiatan';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = $newname;
        $config['file_ext_tolower']     = true;
        $config['max_size']             = 5120;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;
        return $config;
    }

	public function getGalleryById($id_gallery)
	{
		return $this->db->get_where('gallery', ['id_gallery' => $id_gallery])->row_array();
	}
	
	public function hapusFotoGallery($id_gallery, $id_foto)
	{
		$this->db->where(['id_gallery' => $id_gallery,'id_foto' => $id_foto]);
		$this->db->delete('foto_kegiatan');
	}

	public function hapusDataGallery($id_gallery)
	{
		$this->db->where('id_gallery', $id_gallery);
		$this->db->delete('gallery');
	}

	public function cariDataGallery()
	{
		$keyword = $this->input->post('keyword', true);
		$this->db->like('judul', $keyword);
		$this->db->or_like('deskripsi', $keyword);
		// $this->db->or_like('nrp', $keyword);
		// $this->db->or_like('email', $keyword);
		return $this->db->get('gallery')->result_array();
	}
}