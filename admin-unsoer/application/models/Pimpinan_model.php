<?php  

/** 
 * Pimpinan
 */
class Pimpinan_model extends CI_model {

	public $imageUpload;
	public function getAllPimpinan ()
	{
		return $this->db->get('pimpinan')->result_array();
	}

	public function getAdminAllPimpinan()
	{
		$this->db->from('pimpinan');
		$ua = $this->session->userdata('ses_id');
		$this->db->where('create_by', $ua);
		return $this->db->get()->result_array();
	}

	public function getPimpinanById($id)
	{
		return $this->db->get_where('pimpinan', ['id' => $id])->row_array();
	}

	public function tambahDataPimpinan() 
	{
		$data = [
			"nama" => $this->input->post('nama', true),
			"jenis_kelamin" => $this->input->post('jenis_kelamin', true),
			"tempat_lahir" => $this->input->post('tempat_lahir', true),
			"tanggal_lahir" => $this->input->post('tanggal_lahir', true),
			"unit_kerja" => $this->input->post('unit_kerja', true),
			"jabatan" => $this->input->post('jabatan', true),
			"pengalaman_organisasi" => $this->input->post('pengalaman_organisasi', true),
			"pengalaman_pemilu" => $this->input->post('pengalaman_pemilu', true),
			"user_access" => $this->input->post('user_access', true),
			"create_by" => $this->input->post('create_by', true),
			"create_time" => $this->input->post('create_time', true),
			"foto" => $this->imageUpload,
		];
		return $this->db->insert('pimpinan', $data);
	}

	public function ubahDataPimpinan($upload=false) 
	{
		$data = [
			"nama" => $this->input->post('nama', true),
			"jenis_kelamin" => $this->input->post('jenis_kelamin', true),
			"tempat_lahir" => $this->input->post('tempat_lahir', true),
			"tanggal_lahir" => $this->input->post('tanggal_lahir', true),
			"unit_kerja" => $this->input->post('unit_kerja', true),
			"jabatan" => $this->input->post('jabatan', true),
			"pengalaman_organisasi" => $this->input->post('pengalaman_organisasi', true),
			"pengalaman_pemilu" => $this->input->post('pengalaman_pemilu', true),
			"user_access" => $this->input->post('user_access', true),
			"create_by" => $this->input->post('create_by', true),
			"create_time" => $this->input->post('create_time', true)
		];

		if($upload){
			$data["foto"] = $this->imageUpload;
		}

		$this->db->where('id', $this->input->post('id'));
		$this->db->update('pimpinan', $data);
	}

	public function cariDataPimpinan()
	{
		$keyword = $this->input->post('keyword', true);
		$this->db->like('nama', $keyword);
		$this->db->or_like('tempat_lahir', $keyword);
		$this->db->or_like('tanggal_lahir', $keyword);
		$this->db->or_like('alamat', $keyword);
		return $this->db->get('pimpinan')->result_array();
	}

	public function hapusDataPimpinan($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('pimpinan');
	}
}