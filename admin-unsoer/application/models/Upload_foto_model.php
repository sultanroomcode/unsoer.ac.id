<?php 
 
/**
 * Upload_foto_model
 */
class Upload_foto_model extends CI_model {
	public $imageUpload;
	
	public function getAllFoto()
	{
		$this->db->order_by('create_time', 'DESC');
		return $this->db->get('upload_foto')->result_array();
	}

	public function getUploadFotoById($id)
	{
		return $this->db->get_where('upload_foto', ['id' => $id])->row_array();
	}

	public function tambahDataFoto() 
	{
		$data = [
			"deskripsi" => $this->input->post('deskripsi', true),
			"create_by" => $this->input->post('create_by', true),
			"create_time" => $this->input->post('create_time', true),
			"update_by" => $this->input->post('update_by', true),
			"update_time" => $this->input->post('update_time', true),
			"file_foto" => $this->imageUpload,
		];
		return $this->db->insert('upload_foto', $data);
	}

	public function ubahDataUploadFoto($upload=false) 
	{
		$data = [
			"deskripsi" => $this->input->post('deskripsi', true),
			"create_by" => $this->input->post('create_by', true),
			"create_time" => $this->input->post('create_time', true),
			"update_by" => $this->input->post('update_by', true),
			"update_time" => $this->input->post('update_time', true),
		];

		if($upload){
			$data["file_foto"] = $this->imageUpload;
		}

		$this->db->where('id', $this->input->post('id'));
		$this->db->update('upload_foto', $data);
	}

	public function hapusDataFoto($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('upload_foto');
	}

}