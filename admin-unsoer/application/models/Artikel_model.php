<?php 

/**
 * Artikel
 */
class Artikel_model extends CI_model {
	
	public $imageUpload;
	public function getAllArtikel()
	{
		$this->db->select('id_artikel, judul, isi_artikel, kategori.nama_kategori, admin.username, artikel.create_time, cover_img');
		$this->db->from('artikel');
		$this->db->join('admin', 'artikel.create_by = admin.id');
		$this->db->join('kategori', 'artikel.id_kategori = kategori.id_kategori');
		$this->db->order_by('artikel.create_time', 'DESC');
		return $this->db->get()->result_array();
	}

	public function getAdminAllArtikel()
	{
		$this->db->from('artikel');
		$ua = $this->session->userdata('user_access');
		// if($ua != 1)
		// {
			$this->db->where('user_access', $ua);
		// }
		return $this->db->get()->result_array();
	}

	public function tambahDataArtikel() 
	{
		$data = [
			"id_kategori" => $this->input->post('id_kategori', true),
			"judul" => $this->input->post('judul', true),
			"slug" => url_title($this->input->post('judul', true), 'dash', true),
			"isi_artikel" => $this->input->post('isi_artikel', true),
			"create_by" => $this->input->post('create_by', true),
			"create_time" => $this->input->post('create_time', true),
			"update_by" => $this->input->post('update_by', true),
			"user_access" => $this->input->post('user_access', true),
			"update_time" => $this->input->post('update_time', true),
			"jumlah_baca" => $this->input->post('jumlah_baca', true),
			"cover_img" => $this->imageUpload,
		];
		return $this->db->insert('artikel', $data);
	}

	public function ubahDataArtikel($upload=false) 
	{
		$data = [
			"id_kategori" => $this->input->post('id_kategori', true),
			"judul" => $this->input->post('judul', true),
			"slug" => url_title($this->input->post('judul', true), 'dash', true),
			"isi_artikel" => $this->input->post('isi_artikel', true),
			"create_by" => $this->input->post('create_by', true),
			"update_by" => $this->input->post('update_by', true),
			"update_time" => $this->input->post('update_time', true),
			"jumlah_baca" => $this->input->post('jumlah_baca', true),
		];

		if($upload){
			$data["cover_img"] = $this->imageUpload;
		}

		$this->db->where('id_artikel', $this->input->post('id_artikel'));
		$this->db->update('artikel', $data);
	}

	public function getArtikelForUpdate($id_artikel)
	{
		$this->db->where(['id_artikel' => $id_artikel]);
		return $this->db->get('artikel')->row_array();
	}

	public function getArtikelById($id_artikel)
	{
		$this->db->select('id_artikel, judul, isi_artikel, kategori.nama_kategori, admin.username, artikel.create_time, cover_img');
		$this->db->from('artikel');
		$this->db->join('admin', 'artikel.create_by = admin.id');
		$this->db->join('kategori', 'artikel.id_kategori = kategori.id_kategori');
		$this->db->order_by('artikel.create_time', 'DESC');
		$this->db->where(['id_artikel' => $id_artikel]);
		return $this->db->get()->row_array();
	}

	public function hapusDataArtikel($id_artikel)
	{
		$this->db->where('id_artikel', $id_artikel);
		$this->db->delete('artikel');
	}

	public function cariDataArtikel()
	{
		$keyword = $this->input->post('keyword', true);
		$this->db->select('id_artikel, judul, isi_artikel, kategori.nama_kategori, admin.username, artikel.create_time, cover_img');
		$this->db->from('artikel');
		$this->db->join('admin', 'artikel.create_by = admin.id');
		$this->db->join('kategori', 'artikel.id_kategori = kategori.id_kategori');
		$this->db->like('judul', $keyword);
		$this->db->or_like('isi_artikel', $keyword);
		$this->db->order_by('artikel.create_time', 'DESC');
		return $this->db->get()->result_array();
	}

	
}