<?php 
 
/**
 * Gallery
 */
class Foto_kegiatan_model extends CI_model {
	public $imageUpload, $incrementId;
	
	public function getAllFotoKegiatan()
	{
		// $this->db->order_by('foto_kegiatan.create_time', 'DESC');
		return $this->db->get('foto_kegiatan')->result_array();
	}
	
	public function tambahDataFotoKegiatan() 
	{
		$data = [
			"id_gallery" => $this->input->post('id_gallery', true),
			"id_foto" => $this->incrementId,
			"deskripsi" => $this->input->post('deskripsi', true),
			"file_foto" => $this->imageUpload,
			"create_by" => $this->input->post('create_by', true),
			"create_time" => $this->input->post('create_time', true),
			"update_by" => $this->input->post('update_by', true),
			"update_time" => $this->input->post('update_time', true),
		];
		return $this->db->insert('foto_kegiatan', $data);
	}

	public function ubahDataFotoKegiatan($upload=false) 
	{
		$data = [
			"id_gallery" => $this->input->post('id_gallery', true),
			"id_foto" => $this->incrementId,
			"deskripsi" => $this->input->post('deskripsi', true),
			"file_foto" => $this->imageUpload,
			"create_by" => $this->input->post('create_by', true),
			"create_time" => $this->input->post('create_time', true),
			"update_by" => $this->input->post('update_by', true),
			"update_time" => $this->input->post('update_time', true),
		];

		if($upload && $this->imageUpload !== null){
			$data["cover_album"] = $this->imageUpload;
		}

		$this->db->where('id_gallery', $this->input->post('id_gallery'));
		$this->db->update('foto_kegiatan', $data);
	}

	public function ubahDataDeskripsi() 
	{
		$data = [
			"deskripsi" => $this->input->post('deskripsi', true),
			"update_time" => time(),
		];

		$this->db->where('id_gallery', $this->input->post('id_gallery'));
		$this->db->where('id_foto', $this->input->post('id_foto'));
		return $this->db->update('foto_kegiatan', $data);
	}

	public function getAllFotoKegiatanFromGallery($id_gallery)
	{
		$this->db->order_by('foto_kegiatan.create_time', 'DESC');
		$this->db->where(['id_gallery' => $id_gallery]);
		return $this->db->get('foto_kegiatan')->result_array();
	}

	public function getGalleryFotoKegiatan2($id_gallery, $id_foto)
	{
		$this->db->where(['id_gallery' => $id_gallery]);
		$this->db->where(['id_foto' => $id_foto]);
		return $this->db->get('foto_kegiatan')->row_array();
	}

	public function getGalleryFotoKegiatan($id_gallery)
	{
		$this->db->where(['id_gallery' => $id_gallery]);
		return $this->db->get('foto_kegiatan')->row_array();
	}

	public function getGalleryUploadFotoKegiatan($id_gallery)
	{
		$this->db->where(['id_gallery' => $id_gallery]);
		return $this->db->get('foto_kegiatan')->row_array();
	}

	public function upload($inputfilename, $newname)
	{
        $this->load->library('upload', $this->dataconfig($newname));

        if (!$this->upload->do_upload($inputfilename))
        {
            $data = ['state' => false, 'error' => $this->upload->display_errors()];
        }
        else
        {
            $data = ['state' => true, 'upload_data' => $this->upload->data()];
        }

        return $data;
	}

    public function dataconfig($newname){
        $config['upload_path']          = '../upload/gallery/foto-kegiatan';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['file_name']            = $newname;
        $config['file_ext_tolower']     = true;
        $config['max_size']             = 1000;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        return $config;
    }

	public function getFotoKegiatanById($id_gallery)
	{
		return $this->db->get_where('gallery', ['id_gallery' => $id_gallery])->row_array();
	}

	public function hapusDataFotoKegiatan($id_gallery)
	{
		$this->db->where('id_gallery', $id_gallery);
		$this->db->delete('foto_kegiatan');
	}

	public function cariDataFotoKegiatan()
	{
		$keyword = $this->input->post('keyword', true);
		$this->db->like('judul', $keyword);
		$this->db->or_like('deskripsi', $keyword);
		// $this->db->or_like('nrp', $keyword);
		// $this->db->or_like('email', $keyword);
		return $this->db->get('foto_kegiatan')->result_array();
	}

	public function maxNumNow($id_gallery_key)
	{
		$this->db->select_max('id_foto');
	    $this->db->where('id_gallery', $id_gallery_key);
	    $res1 = $this->db->get('foto_kegiatan');

		return $res1;
	}
}