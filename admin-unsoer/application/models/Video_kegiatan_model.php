<?php  

/**
 * Video_kegiatan
 */
class Video_kegiatan_model extends CI_model {
	
	public function getAllVideoKegiatan ()
	{
		$this->db->order_by('create_time', 'DESC');
		return $this->db->get('video_kegiatan')->result_array();
	}

	public function getVideoKegiatanById($id)
	{
		$this->db->where(['id' => $id]);
		return $this->db->get('video_kegiatan')->row_array();
	}

	public function tambahDataVideoKegiatan() 
	{
		$data = [
			"judul" => $this->input->post('judul', true),
			"link" => $this->input->post('link', true),
			"create_by" => $this->input->post('create_by', true),
			"create_time" => $this->input->post('create_time', true),
			"update_by" => $this->input->post('update_by', true),
			"update_time" => $this->input->post('update_time', true),
		];
		$this->db->insert('video_kegiatan', $data);
	}

	public function ubahDataVideoKegiatan() 
	{
		$data = [
			"judul" => $this->input->post('judul', true),
			"link" => $this->input->post('link', true),
			"create_by" => $this->input->post('create_by', true),
			"create_time" => $this->input->post('create_time', true),
			"update_by" => $this->input->post('update_by', true),
			"update_time" => $this->input->post('update_time', true),
		];

		$this->db->where('id', $this->input->post('id'));
		$this->db->update('video_kegiatan', $data);
	}

	public function cariDataVideo()
	{
		$keyword = $this->input->post('keyword', true);
		$this->db->like('judul', $keyword);
		
		return $this->db->get('video_kegiatan')->result_array();
	}

	public function hapusDataVideoKegiatan($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('video_kegiatan');
	}
}