<?php  

/**
 * Admin
 */
class Admin_model extends CI_model {
	
	public function getAllAdmin ()
	{
		$this->db->join('user_access', 'admin.user_access = user_access.id');
		return $this->db->get('admin')->result_array();
	}


	public function tambahDataAdmin() 
	{
		$data = [
			"nama" => $this->input->post('nama', true),
			"username" => $this->input->post('username', true),
			"password" => md5($this->input->post('password', true)),
			"level" => $this->input->post('level', true),
			"image" => $this->input->post('image', true),
			"user_access" => $this->input->post('user_access', true)
		];
		$this->db->insert('admin', $data);
	}


	public function getAdminById($id)
	{
		return $this->db->get_where('admin', ['id' => $id])->row_array();
	}

	public function ubahDataAdmin() 
	{
		$data = [
			"nama" => $this->input->post('nama', true),
			"nrp" => $this->input->post('nrp', true),
			"email" => $this->input->post('email', true),
			"jurusan" => $this->input->post('jurusan', true)
		];

		$this->db->where('id', $this->input->post('id'));
		$this->db->update('admin', $data);
	}

	public function cariDataAdmin()
	{
		$keyword = $this->input->post('keyword', true);
		$this->db->like('nama', $keyword);
		$this->db->or_like('jurusan', $keyword);
		$this->db->or_like('nrp', $keyword);
		$this->db->or_like('email', $keyword);
		return $this->db->get('Admin')->result_array();
	}

	public function tambahDataUserAccess()
	{
		$data = [
			"nama_access" => $this->input->post('nama_access', true),
		];
		$this->db->insert('user_access', $data);
	}

	public function getAllUserAccess()
	{
		return $this->db->get('user_access')->result_array();
	}

	public function hapusDataAdmin($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('admin');
	}
}