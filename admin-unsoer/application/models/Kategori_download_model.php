<?php 

/**
 * Kategori
 */
class Kategori_download_model extends CI_model
{
	
	public function getAllKategoriDownload()
	{
		return $this->db->get('kategori_download')->result_array();
	}

	public function tambahDataKategoriDownload() 
	{
		$data = [
			"id_kategori" => $this->input->post('id_kategori', true),
			"nama_kategori" => $this->input->post('nama_kategori', true),
			"create_by" => $this->input->post('create_by', true),
			"create_time" => $this->input->post('create_time', true),
		];
		$this->db->insert('kategori_download', $data);
	}

	public function getKategoriDownloadById($id_kategori_download)
	{
		return $this->db->get_where('kategori_download', ['id_kategori' => $id_kategori_download])->row_array();
	}


	public function ubahDataKategoriDownload() 
	{
		$data = [
			"nama_kategori" => $this->input->post('nama_kategori', true),
		];

		$this->db->where('id_kategori', $this->input->post('id_kategori'));
		$this->db->update('kategori_download', $data);
	}

	public function hapusDataKategoriDownload($id_kategori)
	{
		$this->db->where('id_kategori', $id_kategori);
		$this->db->delete('kategori_download');
	}

}