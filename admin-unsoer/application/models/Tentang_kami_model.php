<?php 

/**
 * Tentang_kami_model
 */
class Tentang_kami_model extends CI_model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Tentang_kami_model');
		$this->load->library('form_validation');
		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('login');
		}
	}
	public function getAllTentangKami()
	{
		return $this->db->get('tentang_kami')->result_array();
	}

	public function getTentangKamiById($id) 
	{
		$this->db->where(['id' => $id]);
		return $this->db->get('tentang_kami')->row_array();
	}

	public function ubahDataTentangKami() 
	{
		$data = [
			"judul" => $this->input->post('judul', true),
			"deskripsi" => $this->input->post('deskripsi', true),
			
		];
		$this->db->where('id', $this->input->post('id'));
		$this->db->update('tentang_kami', $data);
	}

}
