<?php  
 
/**
 * pengumuman_model
 */
class Pengumuman_model extends CI_model {
	
	public $fileUpload;
	public function getAllPengumuman()
	{
		$ua = $this->session->userdata('user_access');
		$this->db->where('user_access', $ua);
		return $this->db->get('pengumuman')->result_array();
	}

	public function getPengumumanById($id)
	{
		return $this->db->get_where('pengumuman', ['id' => $id])->row_array();
	}

	public function tambahDataPengumuman() 
	{
		$data = [
			"judul" => $this->input->post('judul', true),
			"kategori" => $this->input->post('kategori', true),
			"deskripsi" => $this->input->post('deskripsi', true),
			"slug" => url_title($this->input->post('judul', true), 'dash', true),
			"create_by" => $this->input->post('create_by', true),
			"create_time" => $this->input->post('create_time', true),
			"update_by" => $this->input->post('update_by', true),
			"user_access" => $this->input->post('user_access', true),
			"update_time" => $this->input->post('update_time', true),
			"jumlah_download" => $this->input->post('jumlah_download', true),
			"file_pendukung" => $this->fileUpload,
		];
		$this->db->insert('pengumuman', $data);
	}

	public function hapusDataPengumuman($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('pengumuman');
	}
}