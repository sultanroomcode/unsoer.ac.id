<?php  

/**
 * DataPegawai
 */
class DataPegawai_model extends CI_model {

	public $imageUpload;
	public function getAllDataPegawai ()
	{
		return $this->db->get('data_pegawai')->result_array();
	}

	public function getDataPegawaiById($id)
	{
		return $this->db->get_where('data_pegawai', ['id' => $id])->row_array();
	}

	public function tambahDataPegawai() 
	{
		$data = [
			"nik" => $this->input->post('nik', true),
			"nama" => $this->input->post('nama', true),
			"tempat_lahir" => $this->input->post('tempat_lahir', true),
			"tanggal_lahir" => $this->input->post('tanggal_lahir', true),
			"jenis_kelamin" => $this->input->post('jenis_kelamin', true),
			"alamat" => $this->input->post('alamat', true),
			"agama" => $this->input->post('agama', true),
			"status_kawin" => $this->input->post('status_kawin', true),
			"jabatan" => $this->input->post('jabatan', true),
			"no_telp" => $this->input->post('no_telp', true),
			"create_by" => $this->input->post('create_by', true),
			"create_time" => $this->input->post('create_time', true),
			"update_by" => $this->input->post('update_by', true),
			"update_time" => $this->input->post('update_time', true),
			"foto" => $this->imageUpload,
		];
		return $this->db->insert('data_pegawai', $data);
	}

	public function ubahDataPegawai($upload=false) 
	{
		$data = [
			"nik" => $this->input->post('nik', true),
			"nama" => $this->input->post('nama', true),
			"tempat_lahir" => $this->input->post('tempat_lahir', true),
			"tanggal_lahir" => $this->input->post('tanggal_lahir', true),
			"jenis_kelamin" => $this->input->post('jenis_kelamin', true),
			"alamat" => $this->input->post('alamat', true),
			"agama" => $this->input->post('agama', true),
			"status_kawin" => $this->input->post('status_kawin', true),
			"jabatan" => $this->input->post('jabatan', true),
			"no_telp" => $this->input->post('no_telp', true),
			"create_by" => $this->input->post('create_by', true),
			"create_time" => $this->input->post('create_time', true),
			"update_by" => $this->input->post('update_by', true),
			"update_time" => $this->input->post('update_time', true),
		];

		if($upload){
			$data["foto"] = $this->imageUpload;
		}

		$this->db->where('id', $this->input->post('id'));
		$this->db->update('data_pegawai', $data);
	}

	public function cariDataPegawai()
	{
		$keyword = $this->input->post('keyword', true);
		$this->db->like('nama', $keyword);
		$this->db->or_like('nik', $keyword);
		$this->db->or_like('alamat', $keyword);
		return $this->db->get('data_pegawai')->result_array();
	}

	public function hapusDataPegawai($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('data_pegawai');
	}
}