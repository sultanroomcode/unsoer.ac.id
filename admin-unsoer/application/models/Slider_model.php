<?php 

/**
 * Slider_model
 */
class Slider_model extends CI_model
{
	public $imageUpload;

	// public function getAllSlider()
	// {
	// 	return $this->db->get('slider')->result_array();
	// }

	public function getAdminAllSlider()
	{
		$this->db->from('slider');
		$ua = $this->session->userdata('user_access');
		// if($ua != 1)
		// {
			$this->db->where('user_access', $ua);
		// }
		return $this->db->get()->result_array();
	}

	public function getPublicAllSlider()
	{
		$this->db->from('slider');
		// if($this->)
		// $this->session->userdata('user_access')
		$this->db->where('user_access', '1');
		return $this->db->get()->result_array();
	}

	
	public function tambahDataSlider() 
	{
		$data = [
			
			"judul" => $this->input->post('judul', true),
			"deskripsi" => $this->input->post('deskripsi', true),
			"background" => $this->imageUpload,
			"create_by" => $this->input->post('create_by', true),
			"user_access" => $this->input->post('user_access', true),
			"create_time" => $this->input->post('create_time', true),
			"update_by" => $this->input->post('update_by', true),
			"update_time" => $this->input->post('update_time', true),
			"link" => $this->input->post('link', true),
		];
		return $this->db->insert('slider', $data);
	}

	public function ubahDataSlider($upload=false) 
	{
		$data = [
			"judul" => $this->input->post('judul', true),
			"deskripsi" => $this->input->post('deskripsi', true),
			"create_by" => $this->input->post('create_by', true),
			"user_access" => $this->input->post('user_access', true),
			"create_time" => $this->input->post('create_time', true),
			"update_by" => $this->input->post('update_by', true),
			"update_time" => $this->input->post('update_time', true),
			"link" => $this->input->post('link', true),
		];

		if($upload && $this->imageUpload !== null){
			$data["background"] = $this->imageUpload;
		}

		$this->db->where('id_slider', $this->input->post('id_slider'));
		$this->db->update('slider', $data);
	}

	public function getSliderForUpdate($id_slider)
	{
		$this->db->where(['id_slider' => $id_slider]);
		return $this->db->get('slider')->row_array();
	}

	public function hapusDataSlider($id_slider)
	{
		$this->db->where('id_slider', $id_slider);
		$this->db->delete('slider');
	}
}