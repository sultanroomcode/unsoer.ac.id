<?php 
/**
 * Profil_model
 */
class Profil_model extends CI_model
{
	// sejarah
	public function getAllSejarah()
	{
		return $this->db->get('sejarah')->result_array();
	}
	public function getSejarahById($id)
	{
		return $this->db->get_where('sejarah', ['id' => $id])->row_array();
	}
	
	public function ubahDataSejarah() 
	{
		$data = [
			"judul" => $this->input->post('judul', true),
			"deskripsi" => $this->input->post('deskripsi', true),
			"gambar" => $this->input->post('gambar', true)
		];

		$this->db->where('id', $this->input->post('id'));
		$this->db->update('sejarah', $data);
	}

	// Visi & Misi
	public function getAllVisimisi()
	{
		return $this->db->get('visimisi')->result_array();
	}

	public function getVisimisiById($id)
	{
		return $this->db->get_where('visimisi', ['id' => $id])->row_array();
	}

	public function ubahDataVisimisi() 
	{
		$data = [
			"visi" => $this->input->post('visi', true),
			"deskripsi_visi" => $this->input->post('deskripsi_visi', true),
			"misi" => $this->input->post('misi', true),
			"deskripsi_misi" => $this->input->post('deskripsi_misi', true)
		];

		$this->db->where('id', $this->input->post('id'));
		$this->db->update('visimisi', $data);
	}

	// Identitas dan Media
	public function getAllIdmedia()
	{
		return $this->db->get('identitas_media')->result_array();
	}

	public function getIdmediaById($id)
	{
		return $this->db->get_where('identitas_media', ['id' => $id])->row_array();
	}

	public function ubahDataIdmedia() 
	{
		$data = [
			"nama_sekolah" => $this->input->post('nama_sekolah', true),
			"alamat_sekolah" => $this->input->post('alamat_sekolah', true),
			"email" => $this->input->post('email', true),
			"telp" => $this->input->post('telp', true),
			"facebook" => $this->input->post('facebook', true),
			"instagram" => $this->input->post('instagram', true),
			"wa" => $this->input->post('wa', true)
		];

		$this->db->where('id', $this->input->post('id'));
		$this->db->update('identitas_media', $data);
	}
}