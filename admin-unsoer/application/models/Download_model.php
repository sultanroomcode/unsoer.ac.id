<?php 

/**
 * Download
 */
class Download_model extends CI_model {
	
	public $fileUpload;
	public function getAllDownload()
	{
		return $this->db->get('download')->result_array();
	}

	public function tambahDataDownload() 
	{
		$data = [
			"judul" => $this->input->post('judul', true),
			"slug" => url_title($this->input->post('judul', true), 'dash', true),
			"deskripsi" => $this->input->post('deskripsi', true),
			"id_kategori" => $this->input->post('id_kategori', true),
			"create_by" => $this->input->post('create_by', true),
			"create_time" => $this->input->post('create_time', true),
			"update_by" => $this->input->post('update_by', true),
			"update_time" => $this->input->post('update_time', true),
			"file_upload" => $this->fileUpload,
		];
		$this->db->insert('download', $data);
	}

	// public function ubahDataDownload($upload=false) 
	// {
	// 	$data = [
	// 		"judul" => $this->input->post('judul', true),
	// 		"slug" => url_title($this->input->post('judul', true), 'dash', true),
	// 		"deskripsi" => $this->input->post('deskripsi', true),
	// 		"id_kategori" => $this->input->post('id_kategori', true),
	// 		"create_by" => $this->input->post('create_by', true),
	// 		"create_time" => $this->input->post('create_time', true),
	// 		"update_by" => $this->input->post('update_by', true),
	// 		"update_time" => $this->input->post('update_time', true),
	// 	];

	// 	if($upload){
	// 		$data["file"] = $this->fileUpload;
	// 	}

	// 	$this->db->where('id_download', $this->input->post('id_download'));
	// 	$this->db->update('download', $data);
	// }

	public function getDownloadForUpdate($id_download)
	{
		$this->db->where(['id_download' => $id_download]);
		return $this->db->get('download')->row_array();
	}

	public function hapusDataDownload($id_download)
	{
		$this->db->where('id_download', $id_download);
		$this->db->delete('download');
	}

	public function cariDataDownload()
	{
		// $keyword = $this->input->post('keyword', true);
		// $this->db->select('id_download, judul, deskripsi, kategori.nama_kategori, admin.username, download.create_time, file_upload');
		// $this->db->from('download');
		// $this->db->join('admin', 'download.create_by = admin.id');
		// $this->db->join('kategori', 'download.id_kategori = kategori.id_kategori');
		// $this->db->like('judul', $keyword);
		// $this->db->or_like('deskripsi', $keyword);
		// $this->db->order_by('download.create_time', 'DESC');
		// return $this->db->get()->result_array();

		$keyword = $this->input->post('keyword', true);
		$this->db->like('judul', $keyword);
		$this->db->or_like('deskripsi', $keyword);
		// $this->db->or_like('nrp', $keyword);
		// $this->db->or_like('email', $keyword);
		return $this->db->get('download')->result_array();
	}
}
