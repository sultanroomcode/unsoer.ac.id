<?php  
class Jurnal_model extends CI_model {
	
	public $fileUpload;
	public function getAllJurnal()
	{
		return $this->db->get('jurnal')->result_array();
	}

	public function getJurnal($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('jurnal')->result_array();
	}

	public function tambahDataJurnal() 
	{
		$data = [
			"judul_jurnal" => $this->input->post('judul_jurnal', true),
			"penulis" => $this->input->post('penulis', true),
			"publikasi" => $this->input->post('publikasi', true),
			"link" => $this->input->post('link', true),
			"tahun_terbit" => $this->input->post('tahun_terbit', true),
			"volume" => $this->input->post('volume', true),
			"slug" => url_title($this->input->post('judul_jurnal', true), 'dash', true),
			"create_by" => $this->input->post('create_by', true),
			"user_access" => $this->input->post('user_access', true),
			"create_time" => $this->input->post('create_time', true),
			"update_by" => $this->input->post('update_by', true),
			"update_time" => $this->input->post('update_time', true),
			"jumlah_baca" => $this->input->post('jumlah_baca', true),
			"upload_jurnal" => $this->fileUpload,
		];
		$this->db->insert('jurnal', $data);
	}

	public function hapusDataJurnal($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('jurnal');
	}
}