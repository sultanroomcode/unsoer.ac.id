<?php 
/**
 * Artikel_model 
 */
class Jurnal_model extends MY_model
{
	private $wherex_;
	public $varex_;
	public $fileUpload;
	public $urlFileUpload;
	function __construct(){
		parent::__construct();
		$this->setTableName('jurnal');
        $this->load->library('form_validation');
        $this->urlFileUpload = '../upload/jurnal/';
        $this->wherex_ = [];
	}

	function setWherex($wherex_)
	{
		$this->wherex_ = $wherex_;
	}

	public function tambahDataJurnal() 
	{
		$data = [
			"judul_jurnal" => $this->input->post('judul_jurnal', true),
			"penulis" => $this->input->post('penulis', true),
			"publikasi" => $this->input->post('publikasi', true),
			"link" => $this->input->post('link', true),
			"tahun_terbit" => $this->input->post('tahun_terbit', true),
			"volume" => $this->input->post('volume', true),
			"slug" => url_title($this->input->post('judul_jurnal', true), 'dash', true),
			"create_by" => $this->input->post('create_by', true),
			"user_access" => $this->input->post('user_access', true),
			"create_time" => $this->input->post('create_time', true),
			"update_by" => $this->input->post('update_by', true),
			"update_time" => $this->input->post('update_time', true),
			"jumlah_baca" => $this->input->post('jumlah_baca', true),
			"upload_jurnal" => $this->fileUpload,
		];
		$this->db->insert('jurnal', $data);
	}

	function saveContent($insert=true, $upload=true)
	{
		$data = [
			'judul_jurnal' => $this->input->post('judul_jurnal', true),
			'penulis' => $this->input->post('penulis', true),
			'publikasi' => $this->input->post('publikasi', true),
			'link' => $this->input->post('link', true),
			'tahun_terbit' => $this->input->post('tahun_terbit', true),
			'volume' => $this->input->post('volume', true),
			'slug' => url_title($this->input->post('judul_jurnal', true), 'dash', true),
			'create_by' => $this->input->post('create_by', true),
			"user_access" => $this->input->post('user_access', true),
			"create_time" => $this->input->post('create_time', true),
			"update_by" => $this->input->post('update_by', true),
			"update_time" => $this->input->post('update_time', true),
			"jumlah_baca" => $this->input->post('jumlah_baca', true),
		];

		var_dump($data);

		if($upload)
		{
			$data["upload_jurnal"] = $this->fileUpload;
		}

		if($insert)
		{
			$this->wherex_ = $data + $this->wherex_;
			return $this->input_data($this->wherex_);	
		} else {
			return $this->update_data($data,$this->wherex_);
		}		
	}

	function findAll()
	{
		$this->setWhere($this->wherex_);
		return $this->getdata();
	}

	function findById($id)
	{
		$data = ['id' => $id];
		$this->wherex_ = $data + $this->wherex_;
		$this->setWhere($this->wherex_);
		return $this->getdata();
	}

	function searchAll($keyword)
	{
		$this->setWhere($this->wherex_);
		$this->db->like('judul_jurnal', $keyword);
		return $this->getdata();
	}

	function deleteById($id)
	{
		$data = ['id' => $id];
		$this->wherex_ = $data + $this->wherex_;
		$this->setWhere($this->wherex_);
		return $this->delete_data($this->wherex_);
	}

	function deleteDocument($nama)
	{
		//unlink
		unlink($this->urlFileUpload.$nama);
	}
}

