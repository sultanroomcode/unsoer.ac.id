<div class="container">
	<div class="row mt-3">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div>
				<div class="card-body">
					<form action="<?= base_url().'jurnal/tambah'?>" method="post" enctype="multipart/form-data">
						
						<input type="hidden" name="create_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="user_access" value="<?= $user['user_access']; ?>">
						<input type="hidden" name="create_time" value="<?= time(); ?>">
						<input type="hidden" name="update_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="update_time" value="<?= time(); ?>">
						<input type="hidden" name="jumlah_baca" value="0">

						<div class="form-group">
							<label for="judul_jurnal">Judul jurnal</label>
							<input type="text" class="form-control" id="judul_jurnal" name="judul_jurnal" required="true">
						</div>
						<div class="form-group">
							<label for="penulis">Penulis</label>
							<input type="text" class="form-control" id="penulis" name="penulis" required="true">
							
						</div>
						<div class="form-group">
							<label for="publikasi">Publikasi</label>
							<input type="text" class="form-control" id="publikasi" name="publikasi" required="true">
						</div>
						<div class="form-group">
							<label for="link">Link</label>
							<input type="text" class="form-control" id="link" name="link" required="true">
						</div>
						<div class="form-group">
							<label for="tahun_terbit">Tahun Terbit</label>
							<input type="text" class="form-control" id="tahun_terbit" name="tahun_terbit" required="true">
						</div>
						<div class="form-group">
							<label for="volume">Volume</label>
							<input type="text" class="form-control" id="volume" name="volume" required="true">
						</div>
						<div class="form-group">
							<label for="upload_jurnal">Upload File Jurnal</label><br>
							<input type="file" id="upload_jurnal" name="upload_jurnal" accept=".pdf" required="true"><br>
						</div>
						<button type="submit" name="tambah" class="btn btn-primary float-right" >Tambah Data</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>