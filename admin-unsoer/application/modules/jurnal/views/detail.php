<?php 
$artikel = $artikel->result_array()[0];
?>
<div class="container">
	<h5 class="mt-4"><?= $judul ?></h5>
	<div class="row content">
		<div class="col-md-8">
			<a href="<?= base_url().$this->main_model->varext_['urltxt']; ?>" class="btn btn-info btn-sm float-left"><i class="fa fa-arrow-left"></i></a>
			
			<a class="btn btn-primary btn-sm float-left mb-4" href="<?= base_url().$this->main_model->varext_['urltxt'] ?>/ubah/<?= $artikel['id']; ?>">Ubah Data <i class="fa fa-edit"></i></a>

			<br><br><br>
			
			<small class="text-muted float-right"><?= date('d F Y', $artikel['create_time']); ?></small>
			<h5 class="detail-judul"><?= $artikel['judul_jurnal']; ?></h5>
			
			<?php $urlImg = $this->config->item('base_url_frontend').'/upload/fasilitas/1557151941.jpg'; ?>
			<div class="justify-content-center">
				<img src="<?= $urlImg ?>" class="img-fluid">
			</div>
			
			<table class="table table-striped">
				<tr>
					<th>Nama Penulis</th>
					<td><?= $artikel['penulis']; ?></td>
				</tr>

				<tr>
					<th>Link</th>
					<td><?= $artikel['link']; ?></td>
				</tr>

				<tr>
					<th>Publisher</th>
					<td><?= $artikel['publikasi']; ?></td>
				</tr>

				<tr>
					<th>Tahun Terbit</th>
					<td><?= $artikel['tahun_terbit']; ?></td>
				</tr>

				<tr>
					<th>Volume</th>
					<td><?= $artikel['volume']; ?></td>
				</tr>
			</table>
			
		</div>
	</div>
</div>