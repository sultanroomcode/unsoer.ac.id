<?php 
// use Intervention\Image\ImageManager as Image;
/**
 * Artikel New menggunakan model Artikel New Model
 */
class Jurnal extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('image_lib');
		$this->load->model('Jurnal_model','main_model');
        $this->main_model->varext_ = [ 
            'urltxt' => 'jurnal',//untuk base_url di link button
            'basetxt' => 'Jurnal',//digunakan untuk setting header di view, flashdata
        ];
		
		$this->load->library('form_validation');
		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('login');
		}
	}
	
	public function index()
	{  
		$data['judul'] = 'Jurnal';
        $data['artikel'] = $this->main_model->findAll();
		
		if ($this->input->post('keyword')) {
			$data['artikel'] = $this->main_model->searchAll($this->input->post('keyword'));
		}

        $this->template->set('data', $data);
        $this->template->load('template/sbadmin-template', 'index', $data);
	}

	public function detail($id)
	{
		$data['judul'] = 'Detail Artikel';
		$data['artikel'] = $this->main_model->findById($id);
		$this->template->set('data', $data);
        $this->template->load('template/sbadmin-template', 'detail', $data);
	}

	public function tambah() 
	{
		$data['judul'] = 'Form Tambah Jurnal';
        $data['user'] = $this->db->get_where('admin', ['username' => $this->session->userdata('ses_nama')])->row_array();

        $this->form_validation->set_rules('judul_jurnal', 'Judul Jurnal', 'required');
        $this->form_validation->set_rules('penulis', 'Penulis', 'required');
        $this->form_validation->set_rules('publikasi', 'Publikasi', 'required');
        $this->form_validation->set_rules('link', 'Link', 'required');
        $this->form_validation->set_rules('tahun_terbit', 'Tahun Terbit', 'required');
        $this->form_validation->set_rules('volume', 'Volume', 'required');

        if ( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('tambah', $data);
            $this->load->view('templates/footer');
        } else {
            $dataupload = $this->upload_file();
            if($dataupload['state']){
                $this->main_model->fileUpload = $dataupload['upload_data']['file_name'];
                $this->main_model->saveContent();
                $this->session->set_flashdata('flash', 'ditambahkan');
                redirect('jurnal');
            } else {
                // var_dump($dataupload);
                echo "gagal upload";
            }
        }
	}
	
	public function ubah($id_artikel) 
	{
		$data['judul'] = 'Form Ubah Jurnal';
        $data['jurnal'] = $this->main_model->findById((int) $id_artikel);

        $data['user'] = $this->db->get_where('admin', ['username' => $this->session->userdata('ses_nama')])->row_array();

        $this->form_validation->set_rules('judul_jurnal', 'Judul Jurnal', 'required');
        $this->form_validation->set_rules('penulis', 'Penulis', 'required');
        $this->form_validation->set_rules('publikasi', 'Publikasi', 'required');
        $this->form_validation->set_rules('link', 'Link', 'required');
        $this->form_validation->set_rules('tahun_terbit', 'Tahun Terbit', 'required');
        $this->form_validation->set_rules('volume', 'Volume', 'required');

        if ( $this->form_validation->run() == FALSE ) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('ubah', $data);
            $this->load->view('templates/footer');
        } else {
            $dataupload = $this->upload_file(false);
            if($dataupload['state']){
                $this->main_model->fileUpload = $dataupload['upload_data']['file_name'];
                $this->main_model->saveContent(false);
                $this->session->set_flashdata('flash', 'ditambahkan');
                redirect('jurnal');
            } else {
                $this->load->view('templates/header', $data);
                $this->load->view('templates/sidebar', $data);
                $this->load->view('templates/topbar', $data);
                $this->load->view('ubah', $data);
                $this->load->view('templates/footer');
            }
        }
	}

	public function hapus($id_artikel)
	{
        $model = $this->main_model->findById($id_artikel);
        if($model != null){
            //check if exist
            $model = $model->result_array()[0];
            $this->main_model->deleteDocument($model['upload_jurnal']);
    		$this->main_model->deleteById($model['id']);
            
    		$this->session->set_flashdata('flash', $this->main_model->varext_['basetxt'].' dihapus');
    		redirect($this->main_model->varext_['urltxt']);
        }
	}

    public function upload_file($new = true, $fileold='')
    {
        $config['upload_path']          = '../upload/jurnal';
        $config['allowed_types']        = 'doc|docx|pdf|xls|xlsx|odt';
        $config['file_name']            = url_title($this->input->post('judul_jurnal', true), 'dash', true);
        $config['max_size']             = 25000; // 25MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        if(!$new){
            //update mode
            // rename old file
        }

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('upload_jurnal'))
        {
             $data = array('state' => false, 'error' => $this->upload->display_errors());
        }
        else
        {
            $data = array('state' => true, 'upload_data' => $this->upload->data());
        }
        return $data;
    }
 
        //Compress Image diatas 1 - 3 mb
    public function compressImageOne($filename)
    {
        $config['image_library']='gd2';
        $config['source_image']='../upload/artikel/'.$filename;
        $config['maintain_ratio'] = true;
        $config['master_dim'] = 'height';
        $config['create_thumb']= FALSE;
        $config['quality']= '50%';
        $config['width']= 100;
        $config['height']= 500;

        $this->load->library('image_lib', $config);
        $this->image_lib->clear();
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }
}
