<?php 
// use Intervention\Image\ImageManager as Image;
/**
 * Artikel New menggunakan model Artikel New Model
 */
class Pengabdian_masyarakat extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('image_lib');
		$this->load->model('Penelitian_inovasi_model','main_model');
        $this->main_model->setWherex(['type' => 'pengabdian-masyarakat']);
        $this->main_model->varext_ = [ 
            'urltxt' => 'penelitian-inovasi/pengabdian-masyarakat',
            'basetxt' => 'Pengabdian Masyarakat',
        ];
		$this->load->model('Kategori_model');
		$this->load->library('form_validation');
		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('login');
		}
	}
	
	public function index()
	{  
		$data['judul'] = $this->main_model->varext_['basetxt'];
        $this->main_model->adminWherex();
        $data['artikel'] = $this->main_model->findAll();
		
		if ($this->input->post('keyword')) {
			$data['artikel'] = $this->main_model->searchAll($this->input->post('keyword'));
		}

        $this->template->set('data', $data);
        $this->template->load('template/sbadmin-template', 'index', $data);
	}

	public function detail($id)
	{
		$data['judul'] = 'Detail Artikel '.$this->main_model->varext_['basetxt'];;
		$data['artikel'] = $this->main_model->findById($id);
		$this->template->set('data', $data);
        $this->template->load('template/sbadmin-template', 'detail', $data);
	}

	public function tambah() 
	{
		$data['judul'] = 'Form Tambah '.$this->main_model->varext_['basetxt'];
		
		$this->form_validation->set_rules('title', 'Judul', 'required');
    	$this->form_validation->set_rules('content', 'Isi', 'required');		

		if ($this->form_validation->run() == FALSE ) {
			$this->template->set('data', $data);
            $this->template->load('template/sbadmin-template', 'tambah', $data);			
		} else {
			$dataupload = $this->upload_cover();
			// var_dump($dataupload);
			if($dataupload['state'] == false){
				$dataupload['upload_data']['file_name'] = 'no-image.png';
			}
			$this->main_model->imageUpload = $dataupload['upload_data']['file_name'];
			$this->main_model->saveContent();
			$this->session->set_flashdata('flash', $this->main_model->varext_['basetxt'].' ditambahkan');
			redirect($this->main_model->varext_['urltxt']);
		}
	}
	
	public function ubah($id_artikel) 
	{
		$data['judul'] = 'Form Ubah '.$this->main_model->varext_['basetxt']; 
		$data['artikel'] = $this->main_model->findById($id_artikel);
		
		$this->form_validation->set_rules('title', 'Judul', 'required');
    	$this->form_validation->set_rules('content', 'Isi', 'required');
		
		if ( $this->form_validation->run() == FALSE ) {
			$this->template->set('data', $data);
            $this->template->load('template/sbadmin-template', 'ubah', $data);
		} else {
			// var_dump($this->input->post());
			$upload = $this->input->post('upload_new_file');
			if($upload == 1){
				$dataupload_ubah = $this->upload_cover();	

				if($dataupload_ubah['state'] == false){
					$dataupload_ubah['upload_data']['file_name'] = 'no-image.png';
				}

				if($dataupload_ubah['state']){
					unlink($this->input->post('gambar_lama'));
				}

				$this->main_model->imageUpload = $dataupload_ubah['upload_data']['file_name'];
			}		

			if($upload == 1){
				$this->main_model->saveContent(false);
			} else {
				$this->main_model->saveContent(false, false);//not insert, and not upload 
			}
			$this->session->set_flashdata('flash', $this->main_model->varext_['basetxt'].' diubah');
			redirect($this->main_model->varext_['urltxt']);
		}
	}

	public function hapus($id_artikel)
	{
		$this->main_model->deleteById($id_artikel);
		$this->session->set_flashdata('flash', $this->main_model->varext_['basetxt'].' dihapus');
		redirect($this->main_model->varext_['urltxt']);
	}

    function upload_cover() {
        $this->load->library('image_lib');

        $config['upload_path']      = '../upload/artikel'; //path folder
        $config['allowed_types']    = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['file_name']        = time(); //Enkripsi nama yang terupload
        $config['max_size']         = 5120;

 
        $this->load->library('upload',$config);
        if(!empty($_FILES['cover_img']['name'])){
 
            if ($this->upload->do_upload('cover_img')){
                $gbr = $this->upload->data();
                //compress
                // var_dump($gbr);
                // var_dump($gbr['file_size']);

                if ($gbr['file_size'] < 312 ) { // no compress
                    // echo " dibawah 1/2 mb";
                    $doWaterMark = true;
                    
                } else if ($gbr['file_size'] > 312 && $gbr['file_size'] < 1024) {
                    // echo " diatas 1/2 - 1mb";
                    $this->compressImageOne($gbr['file_name']);
                    $doWaterMark = true;
                } else if ($gbr['file_size'] > 1024 && $gbr['file_size'] < 3072 ) {
                    
                    // echo "di atas 1 mb - 3 mb";
                    $this->compressImageOne($gbr['file_name']); 
                    $doWaterMark =true;

                } else if ($gbr['file_size'] > 3072 && $gbr['file_size'] < 5120 ) {
                    
                    // echo "di atas 3 mb - 5 mb";
                    $this->compressImageTwo($gbr['file_name']); 
                    $doWaterMark = true;
                } else if ($gbr['file_size'] > 5120 ) {
                    echo "File terlalu Besar, Max 5 MB";
                    $doWaterMark = false;
                }
                
                if($doWaterMark){
                    $this->wmtest($gbr['file_name']);               
                }
                $data = array('state' => true, 'upload_data' => $gbr);
            } else  {
                $data = array('state' => false, 'error' => $this->upload->display_errors());
            }
        } else {
            $data = array('state' => false, 'error' => "Image yang diupload kosong");
        }   
        return $data;         
    }
 
        //Compress Image diatas 1 - 3 mb
    public function compressImageOne($filename)
    {
        $config['image_library']='gd2';
        $config['source_image']='../upload/artikel/'.$filename;
        $config['maintain_ratio'] = true;
        $config['master_dim'] = 'height';
        $config['create_thumb']= FALSE;
        $config['quality']= '50%';
        $config['width']= 100;
        $config['height']= 500;

        $this->load->library('image_lib', $config);
        $this->image_lib->clear();
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }

        // Compress Image di atas 3 - 5 mb
    public function compressImageTwo($filename)
    {
        $config['image_library']='gd2';
        $config['source_image']='../upload/artikel/'.$filename;
        $config['maintain_ratio'] = TRUE;
        $config['master_dim'] = 'height';
        // $config['create_thumb']= FALSE;
        $config['quality'] = '300%';
        $config['width']= 1;
        $config['height']= 500;

        $this->load->library('image_lib', $config);
        $this->image_lib->clear();
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }

    public function wmtest($imgurl)
    {
        $this->load->library('image_lib');
        $config['image_library'] = 'gd2';
        $config['source_image'] = '../upload/artikel/'.$imgurl;
        $config['wm_type'] = 'overlay';
        $config['wm_overlay_path'] = '../assets/images/watermark.png';
        //the overlay image
        $config['wm_opacity'] = 100;
        $config['wm_vrt_alignment'] = 'bottom';
        $config['wm_hor_alignment'] = 'right';        
       
        $this->image_lib->initialize($config);
        if (!$this->image_lib->watermark()) {
            $data = array('state' => false, 'error' => $this->image_lib->display_errors());
        } else {
            $data = array('state' => true);
        }
        return $data;
    }
}
