<?php 
$artikel = $artikel->result_array()[0];
?>
<div class="container">
	<h5 class="mt-4"><?= $judul ?></h5>
	<div class="row content">
		<div class="col-md-8">
			<a href="<?= base_url(); ?>berita" class="btn btn-info btn-sm float-left"><i class="fa fa-arrow-left"></i></a>
			<a href="<?= base_url(); ?>berita/ubah/<?= $artikel['id']; ?>" class="btn btn-primary btn-sm float-left mb-4">Ubah Data <i class="fa fa-pencil"></i></a>
			<br><br><br>
			<!-- <small class="text-muted"><?= $artikel['nama_kategori']; ?></small> -->
			<!--  <p class="card-text"> <small class="text-muted">Tanggal daftar <?= date('d F Y', $user['created_at']); ?></small> </p> -->
			<small class="text-muted float-right"><?= date('d F Y', $artikel['created_at']); ?></small>
			<h5 class="detail-judul"><?= $artikel['title']; ?></h5>
			
			<?php if($artikel['thumb_img'] == 'no-image.png'){ $urlImg = $this->config->item('base_url_frontend').'/assets/images/'.$artikel['thumb_img']; } else { $urlImg = $this->config->item('base_url_frontend').'/upload/artikel/'.$artikel['thumb_img']; } ?>
			<div class="justify-content-center">
				<img src="<?= $urlImg ?>" class="img-fluid">
			</div>
			
			<!-- <img src="<?= base_url(); ?>upload/artikel/<?= $artikel['thumb_img'];?>" class="img-fluid"><br> -->
			
			<p class="text-muted"><?= $artikel['content']; ?></p>
			
		</div>
	</div>
</div>