<div class="container mt-3">
	<h3 class="text-center mb-4"><?= $judul; ?></h3>
	<div class="row content">
		<div class="col-md-8">
			<a href="<?= base_url().$this->main_model->varext_['urltxt']; ?>/tambah" class="btn btn-primary float-left"><i class="fa fa-plus"></i> <?=$this->main_model->varext_['basetxt']?> Baru</a>
		</div>
		<div class="col-md-4">
			<form action="" method="post">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Cari judul <?= $this->main_model->varext_['basetxt'] ?>.." name="keyword">
					<div class="input-group-append">
						<button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="row mt-3">
		<div class="col-md-12">
			<?php if ( empty($artikel) ) : ?>
					<div class="alert alert-danger" role="alert">
						Data <?= $this->main_model->varext_['basetxt'] ?> tidak ditemukan.
					</div>
				<?php endif; ?>
		</div>
	</div>
</div>
<br>
<div class="container">
	<div class="row">
		<?php foreach ($artikel->result_array() as $art ) { ?>
			<div class="col-lg-3 col-md-4 col-sm-6">
				<?php if($art['thumb_img'] == 'no-image.png'){ $urlImg = $this->config->item('base_url_frontend').'/assets/images/'.$art['thumb_img']; } else { $urlImg = $this->config->item('base_url_frontend').'/upload/artikel/'.$art['thumb_img']; } ?>
			
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary"><?= $art['title']; ?></h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header"></div>
                      <a class="dropdown-item" href="<?= base_url(); ?>pengumuman/pengumuman-baru/detail/<?= $art['id']; ?>"  >Detail <i class="fa fa-eye float-right text-muted"></i></a>
                      <a class="dropdown-item" href="<?= base_url(); ?>pengumuman/pengumuman-baru/ubah/<?= $art['id']; ?>">Ubah Data <i class="fa fa-pencil-alt float-right text-muted"></i></a>
                      <a class="dropdown-item" href="<?= base_url(); ?>pengumuman/pengumuman-baru/hapus/<?= $art['id']; ?>" 
                      	onclick="return confirm('yakin hapus <?=$this->main_model->varext_['basetxt']?> ?');">Hapus <i class="fa fa-trash float-right text-muted"></i></a>
                      
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area-a">
                    <!-- <canvas id="myAreaChart"></canvas> -->
                   <img src="<?= $urlImg ?>" class="img-fluid">
                  </div>
                </div>
              </div>
        	</div>
          	<?php } ?>		
	</div>						
</div>
