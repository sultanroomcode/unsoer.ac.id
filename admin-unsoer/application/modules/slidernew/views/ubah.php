<?php 
$artikel = $artikel->result_array()[0];
?>
<div class="container">
	<div class="row mt-3">
		<div class="col-md-12">
			<div class="card"> 
				<div class="card-header">
					<?= $judul;  ?> 
				</div> 
				<div class="card-body">
					<form action="<?= base_url().$this->main_model->varext_['urltxt'].'/ubah/'.$artikel['id']; ?>" method="post" enctype="multipart/form-data">
 
						<input type="hidden" name="id" value="<?= $artikel['id']; ?>">
						<input type="hidden" name="created_by" value="<?= $artikel['created_by']; ?>">
						<input type="hidden" name="created_at" value="<?= $artikel['created_at']; ?>">
						<input type="hidden" name="updated_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="updated_at" value="<?= time(); ?>">
						<input type="hidden" name="depart" value="<?= $this->session->depart ?>">
						<input type="hidden" name="depart_detail" value="<?= $this->session->depart_detail ?>">
						<input type="hidden" name="counter" value="<?= $artikel['counter']; ?>">
						
						<div class="form-group">
							<label for="title">Judul <?= $this->main_model->varext_['basetxt'] ?></label>
							<input type="text" class="form-control" id="title" name="title" required="true" value="<?= $artikel['title']; ?>">
						</div>

						<div class="form-group">
							<label for="slug">Link <?= $this->main_model->varext_['basetxt'] ?></label>
							<input type="text" class="form-control" id="slug" name="slug" placeholder="https://unsoer.ac.id/..." value="<?= $artikel['slug']; ?>" required="true">
						</div>
						
						<div class="form-group">
							<label for="thumb_img">Ubah Cover <?= $this->main_model->varext_['basetxt'] ?></label><br>
							<?php if($artikel['thumb_img'] == 'no-image.png'){ $urlImg = $this->config->item('base_url_frontend').'/assets/images/'.$artikel['thumb_img']; 
							} else { $urlImg = $this->config->item('base_url_frontend').'/upload/artikel/'.$artikel['thumb_img']; } echo '<input type="hidden" name="gambar_lama" value="./demidesa.id/upload/artikel/'.$artikel['thumb_img'].'">' ?>
							<img src="<?= $urlImg ?>" class="img-fluid" style="width: 100px;"><br>

							<input type="radio" name="upload_new_file" value="1"  id="show"> Ganti
							<input type="radio" name="upload_new_file" value="0" id="hide" checked> Tidak
							<br>
						
							<div class="browse mt-1">
								<input type="file" id="thumb_img" name="thumb_img" accept=".png,.jpg,.jpeg">
								<p><small class="text-muted">Mohon upload cover dengan ukuran Max 5Mb</small></p>
							</div>
							
						</div>
						<div class="form-group"> 
							<label for="content">Isi <?= $this->main_model->varext_['basetxt'] ?></label>
							<textarea name="content" id="editor1" rows="10" cols="80"><?= $artikel['content']; ?>
				            </textarea>
						</div>

						<div class="form-group">
							<label for="status">Status <?= $this->main_model->varext_['basetxt'] ?></label>
							<select id="status" class="form-control" name="status">
								<option value="DRAFT"<?=($artikel['status'])?' selected':''?>>Draft</option>
								<option value="PUBLISH"<?=($artikel['status'])?' selected':''?>>Publish</option>
							</select>
						</div>

						<button type="submit" name="tambah" class="btn btn-success float-right" >Update</button>
						<a href="<?= base_url(); ?>artikel" class="btn btn-info float-left"><i class="fa fa-arrow-left"></i></a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		$(".browse").hide();
	  $("#hide").click(function(){
	    $(".browse").hide(500);
	  });
	  $("#show").click(function(){
	    $(".browse").show(500);
	  });
	});
</script>