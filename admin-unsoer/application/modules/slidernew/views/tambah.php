<?php 
?>
<div class="container">
	<div class="row mt-3">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div> 
				<div class="card-body">
					<form action="<?= base_url().$this->main_model->varext_['urltxt'].'/tambah'?>" method="post" enctype="multipart/form-data">
						
						<input type="hidden" name="created_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="created_at" value="<?= time(); ?>">
						<input type="hidden" name="updated_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="updated_at" value="<?= time(); ?>">
						<input type="hidden" name="depart" value="<?= $this->session->depart ?>">
						<input type="hidden" name="depart_detail" value="<?= $this->session->depart_detail ?>">
						<input type="hidden" name="counter" value="0">						
						
						<div class="form-group">
							<label for="title">Judul <?= $this->main_model->varext_['basetxt'] ?></label>
							<input type="text" class="form-control" id="title" name="title" required="true">
						</div>

						<div class="form-group">
							<label for="slug">Link <?= $this->main_model->varext_['basetxt'] ?></label>
							<input type="text" class="form-control" id="slug" name="slug" placeholder="https://unsoer.ac.id/..." required="true">
						</div>

						<div class="form-group">
							<label for="cover_img">Cover <?= $this->main_model->varext_['basetxt'] ?></label><br>
							<input type="file" id="cover_img" name="cover_img" accept=".png,.jpg,.jpeg">
							<p><small class="text-muted">Mohon upload cover dengan ukuran Max 5Mb</small></p>
						</div>
						<div class="form-group">
							<label for="content">Isi <?= $this->main_model->varext_['basetxt'] ?></label>
							<textarea name="content" id="editor1" rows="10" cols="80">  
				            </textarea>
						</div>

						<div class="form-group">
							<label for="title">Status <?= $this->main_model->varext_['basetxt'] ?></label>
							<select id="status" class="form-control" name="status">
								<option value="DRAFT">Draft</option>
								<option value="PUBLISH">Publish</option>
							</select>
						</div>

						<button type="submit" name="tambah" class="btn btn-success float-right" >Selesai</button>
						<a href="<?= base_url(); ?>artikel" class="btn btn-info float-left"><i class="fa fa-arrow-left"></i></a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>