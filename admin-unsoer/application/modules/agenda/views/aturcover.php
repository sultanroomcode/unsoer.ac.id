<div class="container mt-5">
	<h3>Pengaturan Ukuran Cover artikel</h3>
	<br>
	<div class="row">
		<div class="col-md-4">
			<div id="cropContainerPreload" style="width: 500px; height: 330px;"></div>	
		</div>
	</div>
	<br>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<a href="<?= base_url(); ?>artikel" class="btn btn-info float-left"><i class="fa fa-arrow-left"></i></a>
		</div> 
	</div>
</div>

<script>
<?php
$urlImg = $this->config->item('base_url_frontend').'/upload/artikel/'.$artikel['cover_img'];
$arr = [
	'uploadUrl' => base_url().'artikel/upload-crop',
	'cropUrl' => base_url().'artikel/crop',
	'loadPicture' => $urlImg,
	'baseImage' => '../upload/artikel/'.$artikel['cover_img'],
	'nameCrop' => '../upload/artikel/crop_'.$artikel['cover_img'],
	'name' => 'varCrop',
	'id' => 'cropContainerPreload',
];
echo $this->croppic->js_set_var($arr);
?>
</script>