<?php 
/**
 * Artikel_model 
 */
class Agenda_model extends MY_model
{
	private $wherex_;
	public $varex_;
	public $imageUpload;
	function __construct(){
		parent::__construct();
		$this->setTableName('konten');
		$this->wherex_ = ['type' => 'agenda'];
        $this->load->library('form_validation');
	}

	function setWherex($wherex_)
	{
		$this->wherex_ = $wherex_;
	}

	function adminWherex()
	{
		$this->wherex_ = ['depart' => $this->session->userdata('depart'), 'depart_detail' => $this->session->userdata('depart_detail') ] + $this->wherex_;
	}

	function saveContent($insert=true, $upload=true)
	{
		$data = [
			"title" => $this->input->post('title', true),
			"slug" => url_title($this->input->post('title', true), 'dash', true),
			"content" => $this->input->post('content', true),
			"created_by" => $this->input->post('created_by', true),
			"created_at" => $this->input->post('created_at', true),
			"updated_by" => $this->input->post('updated_by', true),
			"updated_at" => $this->input->post('updated_at', true),
			"status" => $this->input->post('status', true),
			"depart" => $this->input->post('depart', true),
			"depart_detail" => $this->input->post('depart_detail', true),
			"counter" => $this->input->post('counter', true),
			
		];

		if($upload)
		{
			$data["thumb_img"] = $this->imageUpload;
		}

		if($insert)
		{
			$this->wherex_ = $data + $this->wherex_;
			return $this->input_data($this->wherex_);	
		} else {
			return $this->update_data($data,$this->wherex_);
		}		
	}

	function findAll()
	{
		$this->setWhere($this->wherex_);
		return $this->getdata();
	}

	function findById($id)
	{
		$data = ['id' => $id];
		$this->wherex_ = $data + $this->wherex_;
		$this->setWhere($this->wherex_);
		return $this->getdata();
	}

	function searchAll($keyword)
	{
		$this->setWhere($this->wherex_);
		$this->db->like('title', $keyword);
		$this->db->or_like('content', $keyword);
		return $this->getdata();
	}

	function deleteById($id)
	{
		$data = ['id' => $id];
		$this->wherex_ = $data + $this->wherex_;
		$this->setWhere($this->wherex_);
		return $this->delete_data($this->wherex_);
	}
}

