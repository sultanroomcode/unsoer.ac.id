<div class="container mt-3">
	<div class="row">
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div>
				<div class="card-body">
					<form action="" method="post">
						<input type="hidden" name="id_kategori" value="<?= $kategori['id_kategori']; ?>">
						<div class="form-group">
							<label for="nama_kategori">Nama Kategori</label>
							<input type="text" class="form-control" id="nama_kategori" name="nama_kategori" value="<?= $kategori['nama_kategori']; ?>">
						</div>
						<button type="submit" name="ubah" class="btn btn-success float-right" >Selesai</button>
						<a href="<?= base_url(); ?>kategori_download" class="btn btn-info float-left"><i class="fa fa-arrow-left"></i></a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>