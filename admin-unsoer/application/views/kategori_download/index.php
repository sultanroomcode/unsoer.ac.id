<div class="container mt-3">
	<?php if ( $this->session->flashdata('flash') ) : ?>
	<div class="row mt-3">
		<div class="col-md-6">
			<div class="alert alert-success alert-dismissible fade show" role="alert">Kategori <strong>berhasil </strong><?= $this->session->flashdata('flash'); ?>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			</div>
		</div>
	</div>
	<?php endif; ?>

	<div class="row">
		<div class="col-md-12">
			<h3 class="text-center"><?= $judul; ?></h3>
			<br>
		</div>
	</div>
	<div class="row mt-3 content">
		<div class="col-md-6">
			<a href="<?= base_url(); ?>download" class="btn btn-info float-left"><i class="fa fa-arrow-left"></i></a>
			<a href="<?= base_url(); ?>kategori_download/tambah" class="btn btn-primary mb-2"> Tambah Kategori File</a>
			<ul class="list-group">
				<?php foreach( $kategori as $ktg ) : ?>
					<li class="list-group-item"><?= $ktg['nama_kategori']; ?>
			  			<a href="<?= base_url(); ?>kategori_download/hapus/<?= $ktg['id_kategori']; ?>" class="badge badge-danger float-right" onclick="return confirm('yakin hapus ?');" >hapus</a>
			  			<a href="<?= base_url(); ?>kategori_download/ubah/<?= $ktg['id_kategori']; ?>" class="badge badge-success float-right">ubah</a>
			  		</li>
			  	<?php endforeach; ?>	
			</ul>
		</div>
	</div>
</div>