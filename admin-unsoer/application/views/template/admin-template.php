<!doctype html>
<html class="fixed">
    <?php $this->load->view('include/head'); ?>
    <body>
        <section class="body">

            <!-- start: header -->
            <?php $this->load->view('include/navbar') ?>
            <!-- end: header -->

            <div class="inner-wrapper">
                <!-- start: sidebar -->
                <?php $this->load->view('include/leftsidebar'); ?>
                <!-- end: sidebar -->

                <section role="main" class="content-body">
                    <header class="page-header">
                        <h2><?php echo $laman; ?></h2>
                    
                        <div class="right-wrapper pull-right">
                            <ol class="breadcrumbs">
                                <li>
                                    <a href="index.html">
                                        <i class="fa fa-home"></i>
                                    </a>
                                </li>
                                <li><span><?php echo $laman; ?></span></li>
                            </ol>
                    
                            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
                        </div>
                    </header>

                    <!-- start: page -->
                    <?php if($this->session->flashdata('messager')){ echo '<div class="alert alert-info alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>'.$this->session->flashdata('messager').'</strong></div>'; } ?>
                    <?php echo $contents; ?>
                    <!-- end: page -->
                </section>
            </div>
            <?php $this->load->view('include/rightsidebar'); ?>
        </section>
        <?php $this->load->view('include/footer'); ?>
    </body>
</html>