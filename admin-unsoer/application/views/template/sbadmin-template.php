<?php $this->load->view('templates/header', $data) ?>
<body data-url="<?=base_url()?>">
<?php $this->load->view('templates/sidebar', $data) ?>
<?php $this->load->view('templates/topbar', $data) ?>
<?php if($this->session->flashdata('message') != null){
	echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
		<strong>Warning!</strong>
		'.$this->session->flashdata('message').'
		<button type="button" class="close" data-dismiss="alert" aria-label="Tutup">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>';
} ?>
<?php echo $contents ?>
<?php $this->load->view('templates/footer') ?>