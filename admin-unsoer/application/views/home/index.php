<div class="container">
	<?php if ( $this->session->flashdata('flash') ) : ?>
	<div class="row mt-3">
		<div class="col-md-6">
			<div class="alert alert-success alert-dismissible fade show" role="alert">Data <strong>berhasil </strong><?= $this->session->flashdata('flash'); ?>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			</div>
		</div>
	</div>
	<?php endif; ?>

	<div class="row mt-4 mb-4">
		<div class="col-md-12 content ">
			<h3 class="text-center">Selamat Datang <b><?= $this->session->ses_nama; ?></b></h3>				
			<!-- <p class="text-center">Website Resmi Bawaslu Kab Magetan</p> -->
			
			<!-- <a href="<?= base_url(); ?>admin/tambah/" class="badge badge-primary float-right">
				<i class="fa fa-user"></i> Tambah User Baru
			</a> -->
			<!-- <a href="<?= base_url(); ?>admin/ubah/" class="badge badge-primary float-right">
				<i class="fa fa-cog"></i> Ubah Password Anda
			</a> -->
		</div>
	</div>
</div>
<!-- <div class="container">
	<div class="row">
		<div class="col-md-12 content">
			<h6 class="text-muted">Informasi tampilan Slider</h6>
			<a href="<?= base_url(); ?>slider/tambah" class="btn btn-info">Tambah Slider</a>
			<div class="row mt-2">
				<?php foreach ($slider as $sli) { ?>
				<div class="col-md-3 box-slider">
					<p><?= $sli['judul']; ?><br>
					<span><?= $sli['deskripsi']; ?></span>
					</p>

					<?php if($sli['background'] == 'no-image.png'){ $urlImg = $this->config->item('base_url_frontend').'/assets/images/'.$sli['background']; 
					} else { $urlImg = $this->config->item('base_url_frontend').'/upload/slider/'.$sli['background']; } ?>
					<img src="<?= $urlImg ?>" class="img-fluid">


					<a href="<?= base_url(); ?>slider/hapus/<?= $sli['id_slider']; ?>" class="badge badge-danger float-right" onclick="return confirm('yakin hapus Slider ?');" >hapus</a>
					<a href="<?= base_url(); ?>slider/ubah/<?= $sli['id_slider']; ?>" class="badge badge-success float-right">ubah</a>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div> -->
<div class="container">
	<div class="row">
		<div class="col-xl-8 col-lg-7">
			<?php foreach ($tentang_kami as $ttg) { ?>
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary"><?= $ttg['judul']; ?></h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header"></div>
                      <a class="dropdown-item" href="<?= base_url(); ?>TentangKami/ubah/<?= $ttg['id']; ?>">Ubah Data</a>
                      
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area-aa">
                    <!-- <canvas id="myAreaChart"></canvas> -->
                    <?= $ttg['deskripsi']; ?>
                  </div>
                </div>
              </div>
          	<?php } ?>
        </div>
	</div>
</div>

