<div class="container">
	<div class="row mt-3">
		<div class="col-md-12">
			<div class="card"> 
				<div class="card-header">
					<?= $judul;  ?> 
				</div> 
				<div class="card-body">
					<form action="<?= base_url().'artikel/ubah/'.$artikel['id_artikel']; ?>" method="post" enctype="multipart/form-data">
 
						<input type="hidden" name="id_artikel" value="<?= $artikel['id_artikel']; ?>">
						<input type="hidden" name="create_by" value="<?= $artikel['create_by']; ?>">
						<input type="hidden" name="create_time" value="<?= $artikel['create_time']; ?>">
						<input type="hidden" name="update_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="update_time" value="<?= time(); ?>">
						<input type="hidden" name="jumlah_baca" value="<?= $artikel['jumlah_baca']; ?>">
						
						<div class="form-group">
							<label for="judul">Judul Berita</label>
							<input type="text" class="form-control" id="judul" name="judul" required="true" value="<?= $artikel['judul']; ?>">
						</div>
						<div class="form-group">
							<label for="id_kategori">Kategori Berita</label>
							<select class="form-control" id="id_kategori" name="id_kategori">
								<?php foreach ($kategori as $ktg) { ?>
									<?php if ( $ktg['id_kategori'] == $artikel['id_kategori'] ) : ?>
									<option value="<?= $ktg['id_kategori']; ?>" selected><?= $ktg['nama_kategori']; ?></option>
									<?php else : ?>
										<option value="<?= $ktg['id_kategori']; ?>"><?= $ktg['nama_kategori']; ?></option>
									<?php endif; ?>
								<?php } ?>
							</select>
						</div>
						<div class="form-group">
							<label for="cover_img">Ubah Cover Berita</label><br>
							<?php if($artikel['cover_img'] == 'no-image.png'){ $urlImg = $this->config->item('base_url_frontend').'/assets/images/'.$artikel['cover_img']; 
							} else { $urlImg = $this->config->item('base_url_frontend').'/upload/artikel/'.$artikel['cover_img']; } echo '<input type="hidden" name="gambar_lama" value="./demidesa.id/upload/artikel/'.$artikel['cover_img'].'">' ?>
							<img src="<?= $urlImg ?>" class="img-fluid" style="width: 100px;"><br>

							<input type="radio" name="upload_new_file" value="1"  id="show"> Ganti
							<input type="radio" name="upload_new_file" value="0" id="hide" checked> Tidak
							<br>
						
							<div class="browse mt-1">
								<input type="file" id="cover_img" name="cover_img" accept=".png,.jpg,.jpeg">
								<p><small class="text-muted">Mohon upload cover dengan ukuran Max 5Mb</small></p>
							</div>
							
						</div>
						<div class="form-group"> 
							<label for="isi_artikel">Isi Berita</label>
							<textarea name="isi_artikel" id="editor1" rows="10" cols="80"><?= $artikel['isi_artikel']; ?>
				            </textarea>
						</div>
						<button type="submit" name="tambah" class="btn btn-success float-right" >Selesai</button>
						<a href="<?= base_url(); ?>artikel" class="btn btn-info float-left"><i class="fa fa-arrow-left"></i></a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		$(".browse").hide();
	  $("#hide").click(function(){
	    $(".browse").hide(500);
	  });
	  $("#show").click(function(){
	    $(".browse").show(500);
	  });
	});
</script>