<div class="container">
	<div class="row mt-3">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div> 
				<div class="card-body">
					<form action="<?= base_url().'artikel/tambah'?>" method="post" enctype="multipart/form-data">
						
						<input type="hidden" name="create_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="user_access" value="<?= $this->session->user_access; ?>">
						<input type="hidden" name="create_time" value="<?= time(); ?>">
						<input type="hidden" name="update_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="update_time" value="<?= time(); ?>">
						<input type="hidden" name="jumlah_baca" value="0">						
						
						<div class="form-group">
							<label for="judul">Judul Berita</label>
							<input type="text" class="form-control" id="judul" name="judul" required="true">
						</div>
						<div class="form-group">
							<label for="id_kategori">Kategori</label>
							<select class="form-control" id="id_kategori" name="id_kategori">
								<?php foreach ($kategori as $ktg) { ?>
									<option value="<?= $ktg['id_kategori']; ?>"><?= $ktg['nama_kategori']; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group">
							<label for="cover_img">Cover Berita</label><br>
							<input type="file" id="cover_img" name="cover_img" accept=".png,.jpg,.jpeg">
							<p><small class="text-muted">Mohon upload cover dengan ukuran Max 5Mb</small></p>
						</div>
						<div class="form-group">
							<label for="isi_artikel">Isi Berita</label>
							<textarea name="isi_artikel" id="editor1" rows="10" cols="80">  
				            </textarea>
						</div>
						<button type="submit" name="tambah" class="btn btn-success float-right" >Selesai</button>
						<a href="<?= base_url(); ?>artikel" class="btn btn-info float-left"><i class="fa fa-arrow-left"></i></a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>