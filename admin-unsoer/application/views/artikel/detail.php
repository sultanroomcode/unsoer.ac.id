<div class="container">
	<h5 class="mt-4"><?= $judul ?></h5>
	<div class="row content">
		<div class="col-md-8">
			<a href="<?= base_url(); ?>artikel" class="btn btn-info btn-sm float-left"><i class="fa fa-arrow-left"></i></a>
			<a href="<?= base_url(); ?>artikel/ubah/<?= $artikel['id_artikel']; ?>" class="btn btn-primary btn-sm float-left mb-4">Ubah Data <i class="fa fa-pencil"></i></a>
			<br><br><br>
			<!-- <small class="text-muted"><?= $artikel['nama_kategori']; ?></small> -->
			<!--  <p class="card-text"> <small class="text-muted">Tanggal daftar <?= date('d F Y', $user['create_time']); ?></small> </p> -->
			<small class="text-muted float-right"><?= date('d F Y', $artikel['create_time']); ?></small>
			<h5 class="detail-judul"><?= $artikel['judul']; ?></h5>
			<h6><?= $artikel['username']; ?></h6>

			
			<?php if($artikel['cover_img'] == 'no-image.png'){ $urlImg = $this->config->item('base_url_frontend').'/assets/images/'.$artikel['cover_img']; } else { $urlImg = $this->config->item('base_url_frontend').'/upload/artikel/'.$artikel['cover_img']; } ?>
			<div class="justify-content-center">
				<img src="<?= $urlImg ?>" class="img-fluid">
			</div>
			
			<!-- <img src="<?= base_url(); ?>upload/artikel/<?= $artikel['cover_img'];?>" class="img-fluid"><br> -->
			
			<p class="text-muted"><?= $artikel['isi_artikel']; ?></p>
			
		</div>
	</div>
</div>