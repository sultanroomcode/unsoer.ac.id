<div class="container">
	<div class="row">
		<div class="col-xl-8 col-lg-7">
			<?php foreach ($RunningText as $ttg) { ?>
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary"><?= $judul; ?></h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header"></div>
                      <a class="dropdown-item" href="<?= base_url(); ?>RunningText/ubah/<?= $ttg['id']; ?>">Ubah Data</a>
                      
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area-aa">
                    <!-- <canvas id="myAreaChart"></canvas> -->
                    <?= $ttg['deskripsi']; ?>
                  </div>
                </div>
              </div>
          	<?php } ?>
        </div>
	</div>
</div>