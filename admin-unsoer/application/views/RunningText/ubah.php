<div class="container">
	<div class="row mt-3">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div>
				<div class="card-body">
					<form action="<?= base_url().'RunningText/ubah/'.$RunningText['id']; ?>" method="post">
						<input type="hidden" name="id" value="<?= $RunningText['id']; ?>">
						<div class="form-group"> 
							<label for="deskripsi">Deskripsi</label>
							<textarea name="deskripsi" id="editor1" rows="10" cols="80"><?= $RunningText['deskripsi']; ?>
				            </textarea>
						</div>

						<button type="submit" name="ubah" class="btn btn-primary float-right" >Ubah Data Selesai</button>
						<a href="<?= base_url(); ?>RunningText" class="btn btn-info float-left"><i class="fa fa-arrow-left"></i></a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>