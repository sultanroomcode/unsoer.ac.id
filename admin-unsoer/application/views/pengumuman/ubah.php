<div class="container">
	<div class="row mt-3">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div> 
				<div class="card-body">
					<form action="<?= base_url().'pengumuman/ubah/'.$pengumuman['id']; ?>" method="post" enctype="multipart/form-data">

						<input type="hidden" name="create_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="create_time" value="<?= time(); ?>">
						<input type="hidden" name="update_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="update_time" value="<?= time(); ?>">
						<input type="hidden" name="jumlah_download" value="0">						
						
						<div class="form-group">
							<label for="judul">Judul Pengumuman</label>
							<input type="text" class="form-control" id="judul" name="judul" required="true">
						</div>
						<div class="form-group">
							<label for="kategori">Kategori</label>
							<select class="form-control" id="kategori" name="kategori">
								<option value="staf-kecamatan">Staf Kecamatan</option>
								<option value="staf-kabupaten">Staf Kabupaten</option>
								<option value="rekrutmen-ptps">Rekrutmen PTPS</option>
							</select>
						</div>
						<div class="form-group">
							<label for="deskripsi">Deskripsi Pengumuman</label>
							<textarea name="deskripsi" id="editor1" rows="10" cols="80">  
				            </textarea>
						</div>

						<div class="form-group">
							<label for="file_pendukung">Upload File Pendukung</label><br>
							<input type="file" id="file_pendukung" name="file_pendukung" accept=".xls,.xlsx,.pdf,.doc,.docx,.odt" required="true"><br>
						</div>
						
						<button type="submit" name="tambah" class="btn btn-success float-right" >Selesai</button>
						<a href="<?= base_url('pengumuman'); ?>" class="btn btn-info float-left"><i class="fa fa-arrow-left"></i></a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>