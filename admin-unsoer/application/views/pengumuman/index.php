<h4 class="text-center"><?= $judul; ?></h4><br>
<div class="container mt-3">
	<?php if ( $this->session->flashdata('flash') ) : ?>
	<div class="row mt-3">
		<div class="col-md-6">
			<div class="alert alert-success alert-dismissible fade show" role="alert">Pengumuman <strong>berhasil </strong><?= $this->session->flashdata('flash'); ?>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			</div>
		</div>
	</div> 
	<?php endif; ?>
</div>

<div class="container">
	<div class="row"> 
		<div class="col-md-12">		
	        <div class="card shadow mb-4">
	            <div class="card-header py-3">
	            	
        		<div class="row">
        			<div class="col-md-6">
        				<a href="<?= base_url(); ?>pengumuman/tambah" class="btn btn-primary float-left"><i class="fa fa-plus"></i> Pengumuman Baru</a>
        			</div>
        		</div>
	            	
	            </div>
	            <div class="card-body">
	              <div class="table-responsive">
	              	<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
	                  <thead>
	                    <tr>
	                      <th>Judul Pengumuman</th>
	                      <th>Kategori</th>
	                      <th>Deskripsi</th>
	                      <th>File & Aksi</th>
	                    </tr>
	                  </thead>
	                  <tbody>
	                  	<?php foreach ( $pengumuman as $pe) { ?>
	                    <tr>
	                      <td><a href="<?= base_url(); ?>pengumuman/detail/<?= $pe['id']; ?>"><?= $pe['judul']; ?></a></td>
	                      <td><?= $pe['kategori']; ?></td>
	                      <td><?= $pe['deskripsi']; ?></td>
	                      <td>
	                      	<a href="<?= base_url(); ?>pengumuman/hapus/<?= $pe['id']; ?>" class="badge badge-danger float-right" onclick="return confirm('yakin hapus data Pengumuman ?');" ><i class="fa fa-trash-alt"></i></a>
	                      	<a href="<?= base_url(); ?>pengumuman/ubah/<?= $pe['id']; ?>" class="badge badge-success float-right">ubah</a>
	                      	<a href="<?= $this->config->item('base_url_frontend').'/upload/pengumuman/'.$pe['file_pendukung']; ?>" target='blank' class="badge badge-primary float-right">lihat file pendukung</a>
	                      </td>
	                    </tr>
	            		<?php } ?>
	                  </tbody>
	                </table>

	              </div>
	            </div>
	        </div>
        </div>
	</div>
</div>