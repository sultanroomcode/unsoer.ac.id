<div class="container">
	<div class="row">
		<div class="col-md-12">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary"><?= $judul; ?></h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area-aa">
                    <!-- <canvas id="myAreaChart"></canvas> -->
                    <p><b>Judul Pengumuman: </b><?= $rekrutmen_ptps['judul']; ?></p>
                    <p><b>Deskripsi : </b> <?= $rekrutmen_ptps['deskripsi']; ?></p>
                    <p><b>Dibuat oleh : </b><?= $rekrutmen_ptps['create_by']; ?></p>
                    <p><b>Dibuat pada tanggal : </b><?= date('d F Y', $rekrutmen_ptps['create_time']); ?></p>
                    <br>
              		<a href="<?= base_url('rekrutmen_ptps'); ?>" class="btn btn-primary"><i class="fa fa-arrow-left"></i> </a>
                  </div>
                </div>
              </div>

        </div>
	</div>
</div>