<div class="container mb-5">
	<div class="row mt-3">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div>
					<!-- <div class="embed-responsive embed-responsive-16by9">
					  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/cZKLecON8tA?rel=0" allowfullscreen></iframe>
					</div> -->
				<div class="card-body">
					<form action="<?= base_url().'video_kegiatan/tambah'?>" method="post">

						<input type="hidden" name="create_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="create_time" value="<?= time(); ?>">
						<input type="hidden" name="update_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="update_time" value="<?= time(); ?>">
						<div class="form-group">
							<label for="judul">Judul Video</label>
							<input type="text" class="form-control" id="judul" name="judul" required="true" placeholder="">
							
						</div>

						<div class="form-group">
							<label for="link">Url atau Link dari Youtube</label>
							<small class="float-right" id="show" style="cursor: pointer; color: #6161d5;" > Panduan Link ? </small>
							<div class="panduan card-header mb-3">
								<div id="hide" class="float-right" style="cursor: pointer; color: #6161d5;"> X </div>
								<ol class="pl-1">
									<li><small>Masuk ke Youtube</small></li>
									<li><small>Pilih video yang akan di tampilkan</small></li>
									<li><small>Copy ID Url (copy setelah v= ... )</small></li>
									<li><small>Contoh url : https://www.youtube.com/watch?v=<b>cZKLfcONts8tA</b></small></li>
								</ol>
							</div>
							<input type="text" class="form-control" id="link" name="link" required="true" placeholder="cZKLfcONts8tA">
							
						</div>
						<button type="submit" name="tambah" class="btn btn-primary float-right" >Tambah Data Selesai</button>
						<a href="<?= base_url(); ?>video_kegiatan" class="btn btn-info float-left"><i class="fa fa-arrow-left"></i></a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		$(".panduan").hide();
	  $("#hide").click(function(){
	    $(".panduan").hide(500);
	  });
	  $("#show").click(function(){
	    $(".panduan").show(500);
	  });
	});
</script>