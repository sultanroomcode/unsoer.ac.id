<div class="container">
	<div class="row mt-3">
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div>
				<div class="card-body">
					<form action="<?= base_url().'video_kegiatan/ubah/'.$video_kegiatan['id']; ?>" method="post">

						<input type="hidden" name="id" value="<?= $video_kegiatan['id']; ?>">
						<div class="form-group">
							<label for="judul">Judul Video</label>
							<input type="text" class="form-control" id="judul" name="judul" required="true" value="<?= $video_kegiatan['judul']; ?>">
							
						</div>
						<div class="form-group">
							<label for="link">Url atau Link dari Youtube</label>
							<small class="float-right" id="show" style="cursor: pointer; color: #6161d5;" > Panduan Link ? </small>
							<div class="panduan card-header mb-3">
								<div id="hide" class="float-right" style="cursor: pointer; color: #6161d5;"> X </div>
								<ol class="pl-1">
									<li><small>Masuk ke Youtube</small></li>
									<li><small>Pilih video yang akan di tampilkan</small></li>
									<li><small>Copy ID Url (copy setelah v= ... )</small></li>
									<li><small>Contoh url : https://www.youtube.com/watch?v=<b>cZKLfcONts8tA</b></small></li>
								</ol>
							</div>
							<input type="text" class="form-control" id="link" name="link" required="true" placeholder="cZKLfcONts8tA" value="<?= $video_kegiatan['link']; ?>">
							
						</div>
						<button type="submit" name="ubah" class="btn btn-primary float-right" >Ubah Data Selesai</button>
						<a href="<?= base_url(); ?>video_kegiatan" class="btn btn-info float-left"><i class="fa fa-arrow-left"></i></a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
	$(document).ready(function(){
		$(".panduan").hide();
	  $("#hide").click(function(){
	    $(".panduan").hide(500);
	  });
	  $("#show").click(function(){
	    $(".panduan").show(500);
	  });
	});
</script>