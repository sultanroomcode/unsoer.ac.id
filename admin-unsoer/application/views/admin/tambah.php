<div class="container">
	<div class="row">
		<div class="col-md-6 mt-3">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div>
				<div class="card-body">
					<form action="" method="post">
						<input type="hidden" name="image" value="default.png">
						<input type="hidden" name="level" value="2">

						<div class="form-group">
							<label for="nama">Nama</label>
							<input type="text" class="form-control" id="nama" name="nama" required="true">
						</div>
						<div class="form-group">
							<label for="username">Username</label>
							<input type="text" class="form-control" id="username" name="username" required="true">
							
						</div>
						<div class="form-group">
							<label for="password">Password</label>
							<input type="text" class="form-control" id="password" name="password" required="true">
						</div>

						<div class="form-group">
							<label for="depart">Bagian</label>
							<input type="text" class="form-control" id="depart" name="depart" required="true">
							
						</div>

						<div class="form-group">
							<label for="depart_detail">Bagian Detail</label>
							<input type="text" class="form-control" id="depart_detail" name="depart_detail" required="true">
							
						</div>
						<!--  -->
						<div class="form-group">
							<label for="user_access">User Access</label>
							<span class="float-right"><a href="<?= base_url(); ?>admin/user_access"><small> User Access Baru </small></a></span>
							<select class="form-control" id="user_access" name="user_access">
								<?php foreach ($access as $acc) { ?>
									<option value="<?= $acc['id']; ?>"><?= $acc['nama_access']; ?></option>
								<?php } ?>
							</select>
						</div>
						<button type="submit" name="tambah" class="btn btn-primary float-right" >Tambah Data</button>
						<a href="<?= base_url(); ?>admin" class="btn btn-info float-left">
							<i class="fa fa-arrow-left"></i>
						</a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>