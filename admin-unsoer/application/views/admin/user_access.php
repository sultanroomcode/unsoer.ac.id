<div class="container">
	<div class="row mt-3">
		<div class="col-md-7 mt-3 shadow">
			<h3 class="text-center mt-4 mb-3">Data User Access</h3>

			<?php if ( empty($access) ) : ?>
				<div class="alert alert-danger" role="alert">
					data access tidak ada.
				</div>
			<?php endif; ?>
			<ul class="list-group">
				<?php foreach( $access as $acc ) : ?>
			  		<li class="list-group-item"><?= $acc['nama_access']; ?>
			  			<!-- <a href="<?= base_url(); ?>fasilitas/kategorihapus/<?= $acc['id']; ?>" class="badge badge-danger float-right" onclick="return confirm('yakin hapus ?');" >hapus</a>
			  			<a href="<?= base_url(); ?>fasilitas/kategoriubah/<?= $acc['id']; ?>" class="badge badge-success float-right">ubah</a> -->
			  			
			  		</li>
			  	<?php endforeach; ?>	
			</ul>
			<br>
			<a href="<?= base_url(); ?>admin" class="btn btn-info mb-3 float-left">
				<i class="fa fa-arrow-left"></i>
			</a>
		</div>
		<div class="col-md-5 mt-3">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div>
				<div class="card-body">
					<form action="" method="post">
						<div class="form-group">
							<label for="nama_access">Nama User Access</label>
							<input type="text" class="form-control" id="nama_access" name="nama_access" required="true" placeholder="">
						</div>
						<button type="submit" name="tambah" class="btn btn-success float-right" >Selesai <i class="fa fa-check"></i></button>
							
					</form>
				</div>
			</div>
		</div>
	</div>
</div>