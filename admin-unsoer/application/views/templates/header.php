<!doctype html>
<html lang="en">
  <head> 
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/img/icon.png" type="image/x-icon">
    <title><?= $judul; ?></title>
    

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/bootstrap.min.css">
    <link href="<?= base_url(); ?>assets/sbadmin2/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url(); ?>assets/sbadmin2/css/sb-admin-2.min.css" rel="stylesheet">

    <!-- datatables -->
    <link href="<?= base_url(); ?>assets/sbadmin2/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    
    <!-- style -->
    <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css" >
   
    <!-- JS -->
    <script src="<?= base_url(); ?>assets/js/jquery.min.js" type="text/javascript" ></script>
    
  <body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
   
  <!-- 	   <?php if ($this->session->userdata('masuk') != TRUE) { ?>

  	   <?php } else { ?>
      	<nav class="navbar navbar-expand-lg navbar-light navbar-fixed-top">
      		<div class="container">
    			<a class="navbar-brand" href="<?= base_url(); ?>">Admin</a>
    			  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    			    <span class="navbar-toggler-icon"></span>
    			  </button>
    			<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    			    <div class="navbar-nav ml-auto">
    			      <a class="nav-item nav-link" href="<?= base_url(); ?>">Dashboard</a>
    			      <a class="nav-item nav-link" href="<?= base_url(); ?>artikel">Berita</a>
    			      <a class="nav-item nav-link" href="<?= base_url(); ?>gallery">Galeri</a>
    			      <a class="nav-item nav-link" href="<?= base_url(); ?>download">download</a>
    			  	  <a class="nav-item btn btn-primary tombol" href="<?= base_url(); ?>dashboard/logout">Logout</a>
    			    </div>
    			</div>
    	  	</div>
    	</nav>

	<?php } ?> -->
