	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script> -->
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script> -->
    <!-- Footer -->
      <!-- <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Bawaslu Kabupaten Magetan 2019</span>
          </div>
        </div>
      </footer> -->
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Keluar dari Aplikasi ?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <!-- <div class="modal-body"></div> -->
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="<?= base_url('dashboard/logout'); ?>">OK</a>
        </div>
      </div>
    </div>
  </div>


 
    
    <script src="<?= base_url(); ?>assets/js/ckeditor/ckeditor.js" type="text/javascript" ></script>
    <script src="<?= base_url(); ?>assets/js/notify.min.js" type="text/javascript" ></script>
    <script src="<?= base_url(); ?>assets/js/script.js"></script>
    
    <script src="<?= base_url(); ?>assets/sbadmin2/assets/js/bootstrap.min.js" type="text/javascript" ></script>
    <script src="<?= base_url(); ?>assets/sbadmin2/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    
    <!-- datatables -->
    <script src="<?= base_url(); ?>assets/sbadmin2/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="<?= base_url(); ?>assets/sbadmin2/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?= base_url(); ?>assets/sbadmin2/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="<?= base_url(); ?>assets/sbadmin2/js/sb-admin-2.min.js"></script>
    <script>
        $('.trig_form').click(function(){
            var url = $(this).attr('data-url');
            var back = $(this).attr('data-back');
            //alert(back + 'hey' + url);
            $('.form-desc-tog').hide();
            $('#form-desc-'+back).show();
            $('#form-desc-'+back).load(url);
            //$('#form-desc-'+back).notify("Hello Box");
        });
        
        CKEDITOR.replace( 'editor1' );
        CKEDITOR.replace( 'editor2' );
    </script>

        </div> <!-- Akhir class app -->
    </div> <!-- akhir main-wrapper -->
  </body>
</html>