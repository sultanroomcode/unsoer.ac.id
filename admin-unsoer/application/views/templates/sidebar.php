<?php $ua = $this->session->userdata('user_access'); ?>
    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class=""></i>
        </div>
        <div class="sidebar-brand-text mx-3">Administator <br>
          <small><?= $this->session->userdata('user_access_name'); ?></small></div>
      </a> 

      <!-- Divider -->
      <hr class="sidebar-divider">
      <div class="sidebar-heading">
        A D M I N
      </div>
      <?php if($ua == 1){ ?>
      <li class="nav-item">
        <a class="nav-link" href="<?= $this->config->item('base_url_frontend'); ?>">
          <i class="fas fa-fw fa-user"></i>
          <span>Halaman Depan</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('admin'); ?>">
          <i class="fas fa-fw fa-user"></i>
          <span>Data Admin</span></a>
      </li>
      <div class="sidebar-heading mt-3">
        M E N U
      </div>

       <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBeranda" aria-expanded="true" aria-controls="collapseBeranda">
          <i class="fas fa-fw fa-home"></i>
          <span>Beranda</span>
        </a>
        <div id="collapseBeranda" class="collapse" aria-labelledby="headingOne" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="<?= base_url('slidernew/slider-baru'); ?>">Slider</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseProfil" aria-expanded="true" aria-controls="collapseProfil">
          <i class="fas fa-fw fa-user"></i>
          <span>Profil</span>
        </a>
        <div id="collapseProfil" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="<?= base_url('TentangKami'); ?>">Profil Universitas</a>
            <a class="collapse-item" href="<?= base_url('profil'); ?>">Sejarah Soerjo</a>
            <a class="collapse-item" href="<?= base_url('pimpinan'); ?>">Pimpinan Universitas</a>
            <a class="collapse-item" href="<?= base_url('profil'); ?>">Visi Misi</a>
          </div>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePublik" aria-expanded="true" aria-controls="collapsePublik">
          <i class="fas fa-fw fa-bullhorn"></i>
          <span>Penelitian & Inovasi</span>
        </a>
        <div id="collapsePublik" class="collapse" aria-labelledby="headingOne" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="<?= base_url('penelitian-inovasi/penelitian'); ?>">Penelitian</a>
            <a class="collapse-item" href="<?= base_url('penelitian-inovasi/inovasi'); ?>">Inovasi</a>
            <a class="collapse-item" href="<?= base_url('penelitian-inovasi/riset_inovasi'); ?>">Riset dan Inovasi</a>
            <a class="collapse-item" href="<?= base_url('penelitian-inovasi/pengabdian_masyarakat'); ?>">Pengabdian Masyarakat</a>
          </div>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('fakultas'); ?>">
          <i class="fas fa-fw fa-folder-open"></i>
          <span>Fakultas</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('fasilitas'); ?>">
          <i class="fas fa-fw fa-home"></i>
          <span>Fasilitas</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePengawasan" aria-expanded="true" aria-controls="collapsePengawasan">
          <i class="fas fa-fw fa-folder-open"></i>
          <span>Mahasiswa</span>
        </a>
        <div id="collapsePengawasan" class="collapse" aria-labelledby="headingOne" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="<?= base_url('mahasiswa/aturan-akademik'); ?>">Aturan Akademik</a>
            <a class="collapse-item" href="<?= base_url('mahasiswa/ekstrakurikuler'); ?>">Ekstrakulikuler</a>
            <a class="collapse-item" href="<?= base_url('mahasiswa/prestasi'); ?>">Prestasi Mahasiswa</a>
          </div>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('jurnal'); ?>">
          <i class="fas fa-fw fa-folder-open"></i>
          <span>Jurnal</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('pengumuman/pengumuman-baru'); ?>">
          <i class="fas fa-fw fa-folder-open"></i>
          <span>Pengumuman</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('berita'); ?>">
          <i class="fas fa-fw fa-folder-open"></i>
          <span>Berita</span></a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="<?= base_url('agenda'); ?>">
          <i class="fas fa-fw fa-folder-open"></i>
          <span>Agenda</span></a>
      </li>

      <?php } else { //end of ua 1 ?>

      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('slidernew/slider-baru'); ?>">
          <i class="fas fa-fw fa-folder-open"></i>
          <span>Slider</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('pimpinan'); ?>">
          <i class="fas fa-fw fa-folder-open"></i>
          <span>Pimpinan</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('berita'); ?>">
          <i class="fas fa-fw fa-folder-open"></i>
          <span>Berita</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('pengumuman/pengumuman-baru'); ?>">
          <i class="fas fa-fw fa-folder-open"></i>
          <span>Pengumuman</span></a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="<?= base_url('agenda'); ?>">
          <i class="fas fa-fw fa-folder-open"></i>
          <span>Agenda</span></a>
      </li>
      
      <!-- <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseGaleri" aria-expanded="true" aria-controls="collapseGaleri">
          <i class="fas fa-fw fa-image"></i>
          <span>Galeri & Video</span>
        </a>
        <div id="collapseGaleri" class="collapse" aria-labelledby="headingOne" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="<?= base_url('upload_foto'); ?>">Galeri Foto</a>
            <a class="collapse-item" href="<?= base_url('video_kegiatan'); ?>">Video Kegiatan</a>
          </div>
        </div>
      </li> -->
    <?php } ?>
      
      <!-- Divider -->
      <!-- <hr class="sidebar-divider d-none d-md-block"> -->

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline mt-3">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->
