<div class="container">
	<div class="row mt-3">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div>
				<div class="card-body">
					<form action="" method="post">
						<input type="hidden" name="id" value="<?= $tentang_kami['id']; ?>">
						<div class="form-group">
							<label for="judul">Judul </label>
							<input type="text" class="form-control" id="judul" name="judul" value="<?= $tentang_kami['judul']; ?>">
							
						</div>
						<div class="form-group"> 
							<label for="deskripsi">Deskripsi</label>
							<textarea name="deskripsi" id="editor1" rows="10" cols="80"><?= $tentang_kami['deskripsi']; ?>
				            </textarea>
						</div>

						<button type="submit" name="ubah" class="btn btn-primary float-right" >Ubah Data Selesai</button>
						<a href="<?= base_url(); ?>home" class="btn btn-info float-left"><i class="fa fa-arrow-left"></i></a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>