<div class="container">
	<div class="row mt-3">
		<div class="col-md-10">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div>
				<div class="card-body">
					<form action="" method="post">
						<input type="hidden" name="id" value="<?= $visimisi['id']; ?>">
						<div class="form-group">
							<label for="visi">Visi</label>
							<input type="text" class="form-control" id="visi" name="visi" readonly="true" value="<?= $visimisi['visi']; ?>">
						</div>

						<div class="form-group">
							<label for="deskripsi_visi">Deskripsi Visi</label><br>
							<textarea name="deskripsi_visi" id="editor1" rows="10" cols="80">
								<?= $visimisi['deskripsi_visi']; ?>
				            </textarea>
						</div>

						<div class="form-group">
							<label for="misi">Misi</label>
							<input type="text" class="form-control" id="misi" name="misi" readonly="true" value="<?= $visimisi['misi']; ?>">
						</div>
						<div class="form-group">
							<label for="deskripsi_misi">Deskripsi Misi</label><br>
							<textarea name="deskripsi_misi" id="editor2" rows="10" cols="80">
								<?= $visimisi['deskripsi_misi']; ?>
				            </textarea>
						</div>
						
						<button type="submit" name="ubah" class="btn btn-primary float-right" >Selesai</button>
						<a href="<?= base_url(); ?>profil" class="btn btn-primary"><i class="fa fa-arrow-left"></i></a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	CKEDITOR.replace( 'editor2' );
</script>