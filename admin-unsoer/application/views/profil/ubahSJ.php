<div class="container">
	<div class="row mt-3">
		<div class="col-md-10">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div>
				<div class="card-body">
					<form action="" method="post">
						<input type="hidden" name="id" value="<?= $sejarah['id']; ?>">
						<div class="form-group">
							<label for="judul">Judul</label>
							<input type="text" class="form-control" id="judul" name="judul" value="<?= $sejarah['judul']; ?>">
						</div>
						<div class="form-group">
							<label for="deskripsi">Deskripsi Sejarah Sekolah</label><br>
							<textarea name="deskripsi" id="editor1" rows="10" cols="80">
								<?= $sejarah['deskripsi']; ?>
				            </textarea>
						</div>
						<div class="form-group">
							<label for="gambar">Gambar</label>
							<input type="text" class="form-control" id="gambar" name="gambar" value="<?= $sejarah['gambar']; ?>">
						</div>

						<button type="submit" name="ubah" class="btn btn-primary float-right" >Selesai</button>
						<a href="<?= base_url(); ?>profil" class="btn btn-primary"><i class="fa fa-arrow-left"></i></a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>