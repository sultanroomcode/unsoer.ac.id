<div class="container">
	<div class="row mt-3">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div>
				<div class="card-body">
					<form action="" method="post">
						<input type="hidden" name="id" value="<?= $idmedia['id']; ?>">
						<div class="form-group">
							<label for="nama_sekolah">Nama Sekolah</label>
							<input type="text" class="form-control" id="nama_sekolah" name="nama_sekolah" value="<?= $idmedia['nama_sekolah']; ?>">
						</div>
						<div class="form-group">
							<label for="alamat_sekolah">Alamat Sekolah</label>
							<input type="text" class="form-control" id="alamat_sekolah" name="alamat_sekolah" value="<?= $idmedia['alamat_sekolah']; ?>">
						</div>
						<div class="form-group">
							<label for="telp">Telp</label>
							<input type="text" class="form-control" id="telp" name="telp" value="<?= $idmedia['telp']; ?>">
						</div>
						<div class="form-group">
							<label for="email">Email</label>
							<input type="text" class="form-control" id="email" name="email" value="<?= $idmedia['email']; ?>">
						</div>
						<div class="form-group">
							<label for="facebook">Facebook</label>
							<input type="text" class="form-control" id="facebook" name="facebook" value="<?= $idmedia['facebook']; ?>">
						</div>
						<div class="form-group">
							<label for="instagram">Instagram</label>
							<input type="text" class="form-control" id="instagram" name="instagram" value="<?= $idmedia['instagram']; ?>">
						</div>
						<div class="form-group">
							<label for="wa">WhatsApp</label>
							<input type="text" class="form-control" id="wa" name="wa" value="<?= $idmedia['wa']; ?>">
						</div>					
						<button type="submit" name="ubah" class="btn btn-primary float-right" >Selesai</button>
						<a href="<?= base_url(); ?>profil" class="btn btn-primary"><i class="fa fa-arrow-left"></i></a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>