<div class="container">
	<div class="row">
		<div class="col-xl-8 col-lg-7">
			<?php foreach ($sejarah as $sj) { ?>
              <div class="card shadow mb-4"> 
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary"><?= $sj['judul'];  ?></h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header"></div>
                      <a class="dropdown-item" href="<?= base_url(); ?>profil/ubahSJ/<?= $sj['id']; ?>">Ubah Data</a>
                      
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area-aa">
                    <!-- <canvas id="myAreaChart"></canvas> -->
                    <?= $sj['deskripsi'];  ?><br>
                  </div>
                </div>
              </div>
          	<?php } ?>
        </div>
	</div>
	<div class="row">
		<div class="col-xl-8 col-lg-7">
			<?php foreach ($visimisi as $vm) { ?>
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary"> Visi dan Misi</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header"></div>
                      <a class="dropdown-item" href="<?= base_url(); ?>profil/ubahVM/<?= $vm['id']; ?>">Ubah Data</a>
                      
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area-aa">
                    <!-- <canvas id="myAreaChart"></canvas> -->
                    <?= $vm['visi']; ?><br>
          					<?= $vm['deskripsi_visi']; ?><br>
          					<?= $vm['misi']; ?><br>
          					<?= $vm['deskripsi_misi']; ?><br>
                  </div>
                </div>
              </div>
          	<?php } ?>
        </div>
	</div>
</div>

<!-- <div class="container mt-4">
	<div class="row">
		<div class="col-md-12 content">
			<h3>Identitas dan Media</h3>
			<?php foreach ($idmedia as $im) { ?>
				<b>Nama Sekolah : </b><?= $im['nama_sekolah'];  ?><br>
				<b>Alamat Sekolah : </b><?= $im['alamat_sekolah'];  ?><br>
				<b>Email : </b><?= $im['email'];  ?><br>
				<b>Telp : </b><?= $im['telp'];  ?><br>
				<b>Facebook : </b><?= $im['facebook'];  ?><br>
				<b>Instagram : </b><?= $im['instagram'];  ?><br>
				<b>WhatsApp : </b><?= $im['instagram'];  ?><br>
				<a href="<?= base_url(); ?>profil/ubahIM/<?= $im['id']; ?>" class="btn btn-primary float-right">Ubah Data</a>
			<?php } ?>
		</div>
	</div>
</div> -->