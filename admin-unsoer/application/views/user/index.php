
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800"><?= $judul; ?></h1>

          <div class="card mb-3" style="max-width: 540px">
            <div class="row no-gutters">
              <div class="col-md-4">
                <img src="<?= base_url('assets/img/profile/') .$user['image']; ?>" class="card-img">
              </div>
              <div class="col-md-8">
                <div class="card-body">
                  <h4 class="card-title"><b> <?= $user['nama']; ?></b></h4>
                  <p class="card-text">Admin : <?= $this->session->userdata('user_access_name'); ?></p>
                 <!--  <p class="card-text"> <small class="text-muted">Tanggal daftar <?= date('d F Y', $user['create_time']); ?></small> </p> -->
                </div>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      

