
<div class="container">
	<div class="row">
		<div class="col-md-7 mt-3 shadow">
			<h3 class="text-center mt-4 mb-3">Data Kategori Fasilitas</h3>
			

			<?php if ( empty($kategori) ) : ?>
				<div class="alert alert-danger" role="alert">
					data kategori tidak ada.
				</div>
			<?php endif; ?>
			<ul class="list-group">
				<?php foreach( $kategori as $ktg ) : ?>
			  		<li class="list-group-item"><?= $ktg['nama_kategori']; ?>
			  			<a href="<?= base_url(); ?>fasilitas/kategorihapus/<?= $ktg['id']; ?>" class="badge badge-danger float-right" onclick="return confirm('yakin hapus ?');" >hapus</a>
			  			<a href="<?= base_url(); ?>fasilitas/kategoriubah/<?= $ktg['id']; ?>" class="badge badge-success float-right">ubah</a>
			  			
			  		</li>
			  	<?php endforeach; ?>	
			</ul>
			<br>
			<a href="<?= base_url(); ?>fasilitas" class="btn btn-info mb-3 float-left">
				<i class="fa fa-arrow-left"></i>
			</a>
		</div>

		<div class="col-md-5 mt-3">
			<div class="card">
				<div class="card-header">
					<h6>Tambah Data Kategori Fasilitas Baru</h6>
				</div>
				<div class="card-body">
					<form action="<?= base_url().'fasilitas/kategori'?>" method="post">

						<input type="hidden" name="create_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="create_time" value="<?= time(); ?>">
						<div class="form-group">
							<label for="nama_kategori">Nama Kategori</label>
							<input type="text" class="form-control" id="nama_kategori" name="nama_kategori" required="true">
						</div>
						<button type="submit" name="tambah" class="btn btn-success float-right" >Selesai <i class="fa fa-check"></i></button>
						
					</form>
				</div>
			</div>
		</div>
	</div>
</div>