<div class="container">
	<div class="row mt-3">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div>
				<div class="card-body">
					<form action="<?= base_url().'fasilitas/tambah'?>" method="post" enctype="multipart/form-data">
						
						<input type="hidden" name="create_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="create_time" value="<?= time(); ?>">
						
						<div class="form-group">
							<label for="nama_fasilitas">Nama Fasilitas</label>
							<input type="text" class="form-control" id="nama_fasilitas" name="nama_fasilitas" required="true">
						</div>
						<div class="form-group">
							<label for="kategori_fasilitas">Kategori Fasilitas</label>
							<span class="float-right"><a href="<?= base_url(); ?>fasilitas/kategori"><small> Kategori Baru </small></a></span>
							<select class="form-control" id="kategori_fasilitas" name="kategori_fasilitas">
								<?php foreach ($kategori as $ktg) { ?>
									<option value="<?= $ktg['id']; ?>"><?= $ktg['nama_kategori']; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group">
							<label for="deskripsi">Deskripsi Fasilitas</label>
							<input type="text" class="form-control" id="deskripsi" name="deskripsi" required="true">
						</div>
						<div class="form-group">
							<label for="foto">Foto Fasilitas</label><br>
							<input type="file" id="foto" name="foto"><br>
							<small class="text-muted">Upload gambar dengan ukuran Max 3Mb</small>
						</div>
						
						<button type="submit" name="tambah" class="btn btn-success float-right" >Selesai <i class="fa fa-check"></i></button>
						<a href="<?= base_url(); ?>fasilitas" class="btn btn-info float-left">
							<i class="fa fa-arrow-left"></i>
						</a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>