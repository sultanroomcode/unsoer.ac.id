<div class="container">
	<div class="row mt-3">
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div>
				<div class="card-body">
					<form action="" method="post">
						<div class="form-group">
							<label for="nama">Nama</label>
							<input type="text" class="form-control" id="nama" name="nama" required="true">
							
						</div>
						<div class="form-group">
							<label for="nrp">NRP</label>
							<input type="text" class="form-control" id="nrp" name="nrp" required="true">
							
						</div>
						<div class="form-group">
							<label for="email">Email</label>
							<input type="email" class="form-control" id="email" name="email" required="true">
							
						</div>
						<div class="form-group">
							<label for="jurusan">Jurusan</label>
							<select class="form-control" id="jurusan" name="jurusan">
								<option value="Teknik Informatika">Teknik Informatika</option>
								<option value="Teknik Industri">Teknik Industri</option>
								<option value="Teknik Mesin">Teknik Mesin</option>
								<option value="Teknik Elektro">Teknik Elektro</option>
							</select>
						</div>
						<button type="submit" name="tambah" class="btn btn-primary float-right" >Tambah Data</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>