<div class="container">
	<div class="row mt-3">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
					<?php var_dump($jurnal) ?>
					<?php $jurnal = $jurnal[0]; ?>
					 ["id"]=> string(1) "2" 
					 ["publikasi"]=> string(10) "Ilmu Hukum" 
					 ["link"]=> string(1) "#" 
					 ["tahun_terbit"]=> string(4) "2019" 
					 ["volume"]=> string(1) "3" 
					 ["upload_jurnal"]=> string(21) "jurnal-ilmu-hukum.pdf" 
					 ["slug"]=> string(17) "jurnal-ilmu-hukum" 
				</div>
				<div class="card-body">
					<form action="<?= base_url().'jurnal/ubah/'.$jurnal['id']?>" method="post" enctype="multipart/form-data">
						<input type="hidden" name="update_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="update_time" value="<?= time(); ?>">

						<div class="form-group">
							<label for="judul_jurnal">Judul jurnal</label>
							<input type="text" class="form-control" id="judul_jurnal" name="judul_jurnal" value="<?= $jurnal['judul_jurnal'] ?>" required="true">
						</div>
						<div class="form-group">
							<label for="penulis">Penulis</label>
							<input type="text" class="form-control" id="penulis" name="penulis" value="<?= $jurnal['penulis'] ?>" required="true">
							
						</div>
						<div class="form-group">
							<label for="publikasi">Publikasi</label>
							<input type="text" class="form-control" id="publikasi" name="publikasi" value="<?= $jurnal['publikasi'] ?>" required="true">
						</div>
						<div class="form-group">
							<label for="link">Link</label>
							<input type="text" class="form-control" id="link" name="link" value="<?= $jurnal['link'] ?>" required="true">
						</div>
						<div class="form-group">
							<label for="tahun_terbit">Tahun Terbit</label>
							<input type="text" class="form-control" id="tahun_terbit" name="tahun_terbit" value="<?= $jurnal['tahun_terbit'] ?>" required="true">
						</div>
						<div class="form-group">
							<label for="volume">Volume</label>
							<input type="text" class="form-control" id="volume" name="volume" value="<?= $jurnal['volume'] ?>" required="true">
						</div>

						<div class="form-group">
							<label for="upload_jurnal">Upload File Jurnal</label><br>
							<input type="file" id="upload_jurnal" name="upload_jurnal" accept=".pdf, .docx, .doc" required="true"><br>
						</div>
						<button type="submit" name="tambah" class="btn btn-primary float-right" >Tambah Data</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>