<!-- <h6 class="text-center">Gallery</h6> -->
<h4 class="text-center"><?= $judul; ?></h4>

<div class="container mt-3">
	<?php if ( $this->session->flashdata('flash') ) : ?>
	<div class="row mt-3">
		<div class="col-md-6">
			<div class="alert alert-success alert-dismissible fade show" role="alert">Data Jurnal <strong>berhasil </strong><?= $this->session->flashdata('flash'); ?>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			</div>
		</div>
	</div> 
	<?php endif; ?>

	<div class="row mt-3">
		<div class="col-md-12">
			<?php if ( empty($jurnal) ) : ?>
				<div class="alert alert-danger" role="alert">
					Data Jurnal tidak ditemukan.
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<br>
<div class="container">
	<div class="row"> 
		<div class="col-md-12">		
	        <div class="card shadow mb-4">
	            <div class="card-header py-3">
	      			<div class="row">
	      				<div class="col-md-8">
	      					<a href="<?= base_url(); ?>jurnal/tambah" class="btn btn-primary float-left"><i class="fa fa-plus"></i> Jurnal Baru</a>
	      				</div>
	      				<div class="col-md-4">
	      					<form action="" method="post">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Cari Jurnal.." name="keyword">
									<div class="input-group-append">
										<button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
									</div>
								</div>
							</form>
	      				</div>
	      			</div>
	            </div>
	            <div class="card-body">
	              <div class="table-responsive">
	              	<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
	                  <thead>
	                    <tr>
	                      <th>Judul jurnal</th>
	                      <th>Penulis</th>
	                      <th>Publikasi</th>
	                      <th>Link</th>
	                      <th>Tahun terbit</th>
	                      <th>Volume</th>
	                      <th>Aksi</th>
	                    </tr>
	                  </thead>
	                  <tbody>
	                  	<?php foreach ( $jurnal as $jur) { ?>

	                    <tr>
	                      <td><?= $jur['judul_jurnal']; ?></td>
	                      <td><?= $jur['penulis']; ?></td>
	                      <td><?= $jur['publikasi']; ?></td>
	                      <td><?= $jur['link']; ?></td>
	                      <td><?= $jur['tahun_terbit']; ?></td>
	                      <td><?= $jur['volume']; ?></td>
	                      <td>
	                      	<a href="<?= base_url(); ?>jurnal/hapus/<?= $jur['id']; ?>" class="badge badge-danger float-right" onclick="return confirm('yakin hapus ?');" ><i class="fa fa-trash-alt"></i></a>
				  			<a href="<?= base_url(); ?>jurnal/ubah/<?= $jur['id']; ?>" class="badge badge-success float-right"><i class="fa fa-pencil-alt"></i></a>
				  			<a href="<?= $this->config->item('base_url_frontend').'/upload/jurnal/'.$jur['upload_jurnal']; ?>" target='blank' class="badge badge-primary float-right"><i class="fa fa-eye"></i> </a>
				  			<!-- <a href="<?= base_url(); ?>jurnal/detail/<?= $jur['id']; ?>" class="badge badge-primary float-right"><i class="fa fa-eye"></i></a> -->
	                      </td>
	                    </tr>
	            		<?php } ?>
	                  </tbody>
	                </table>

	              </div>
	            </div>
	        </div>
        </div>
	</div>
</div>
