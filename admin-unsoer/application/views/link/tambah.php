<div class="container">
	<div class="row mt-3">
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div>
				<div class="card-body">
					<form action="<?= base_url().'link/tambah'?>" method="post">
						<div class="form-group">
							<label for="judul">Judul Link</label>
							<input type="text" class="form-control" id="judul" name="judul" required="true" placeholder="Bawaslu Kab ...">
							
						</div>
						<div class="form-group">
							<label for="url">Url atau Alamat web tujuan</label>
							<input type="text" class="form-control" id="url" name="url" required="true" placeholder="http:// ...">
							
						</div>
						<button type="submit" name="tambah" class="btn btn-primary float-right" >Tambah Data Selesai</button>
						<a href="<?= base_url(); ?>link" class="btn btn-info float-left"><i class="fa fa-arrow-left"></i></a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>