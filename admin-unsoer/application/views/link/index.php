<div class="container">
	<div class="content">
		<?php if ( $this->session->flashdata('flash') ) : ?>
		<div class="row mt-3">
			<div class="col-md-6">
				<div class="alert alert-success alert-dismissible fade show" role="alert">Data Link <strong>berhasil </strong><?= $this->session->flashdata('flash'); ?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				</div>
			</div>
		</div>
		<?php endif; ?>

		<div class="row mt-3">
			<div class="col-md-4 float-left">
				<a href="<?= base_url(); ?>link/tambah" class="btn btn-primary"><i class="fa fa-plus"></i> Data Link Baru</a>
			</div>
			<div class="col-md-4 float-right">
				<form action="" method="post">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Cari data Link.." name="keyword">
						<div class="input-group-append">
							<button class="btn btn-primary" type="submit">Cari</button>
						</div>
					</div>
					
				</form>
			</div>
		</div>
		<br>
		<div class="row mt-3">
			<div class="col-md-6">
				<h3>Daftar Link</h3>
				<?php if ( empty($link) ) : ?>
					<div class="alert alert-danger" role="alert">
						data link tidak ada.
					</div>
				<?php endif; ?>
				<ul class="list-group">
					<?php foreach( $link as $li ) : ?>
				  		<li class="list-group-item"><small><b><?= $li['judul']; ?></b></small> / <small class="text-muted"><?= $li['url']; ?></small>
				  			<a href="<?= base_url(); ?>link/hapus/<?= $li['id_link']; ?>" class="badge badge-danger float-right" onclick="return confirm('yakin hapus ?');" >hapus</a>
				  			<a href="<?= base_url(); ?>link/ubah/<?= $li['id_link']; ?>" class="badge badge-success float-right">ubah</a>
				  			<!-- <a href="<?= base_url(); ?>link/detail/<?= $li['id_link']; ?>" class="badge badge-primary float-right">detail</a> -->
				  		</li>
				  	<?php endforeach; ?>	
				</ul>
			</div>
		</div>
	</div>
</div>