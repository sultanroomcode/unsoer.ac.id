<div class="container">
	<div class="row mt-3">
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div>
				<div class="card-body">
					<form action="<?= base_url().'link/ubah/'.$link['id_link']; ?>" method="post">
						<input type="hidden" name="id_link" value="<?= $link['id_link']; ?>">
						<div class="form-group">
							<label for="judul">Judul Link</label>
							<input type="text" class="form-control" id="judul" name="judul" required="true" value="<?= $link['judul']; ?>">
							
						</div>
						<div class="form-group">
							<label for="url">Url atau Alamat web tujuan</label>
							<input type="text" class="form-control" id="url" name="url" required="true" value="<?= $link['url']; ?>">
							
						</div>
						<button type="submit" name="ubah" class="btn btn-primary float-right" >Ubah Data Selesai</button>
						<a href="<?= base_url(); ?>link" class="btn btn-info float-left"><i class="fa fa-arrow-left"></i></a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>