<div class="container">
	<div class="row mt-3">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div>
				<div class="card-body">
					<form action="<?= base_url().'gallery/ubah/'.$gallery['id_gallery']; ?>" method="post" enctype="multipart/form-data">
						
						<input type="hidden" name="id_gallery" value="<?= $gallery['id_gallery']; ?>">
						<input type="hidden" name="create_by" value="<?= $gallery['create_by']; ?>">
						<input type="hidden" name="create_time" value="<?= $gallery['create_time']; ?>">
						<input type="hidden" name="update_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="update_time" value="<?= time(); ?>">
						<!-- <input type="hidden" name="foto" value="<?= time(); ?>"> -->

						<div class="form-group">
							<label for="judul">Judul Album</label>
							<input type="text" class="form-control" id="judul" name="judul" required="true" value="<?= $gallery['judul']; ?>">
						</div>
						<div class="form-group">
							<label for="deskripsi">Deskripsi Album</label>
							<input type="text" class="form-control" id="deskripsi" name="deskripsi" required="true" value="<?= $gallery['deskripsi']; ?>">
						</div>
						<div class="form-group">
							<label for="cover_album">Ubah Cover Album</label><br>
							<?php if($gallery['cover_album'] == 'no-image.png'){ $urlImg = $this->config->item('base_url_frontend').'/assets/images/'.$gallery['cover_album']; 
							} else { $urlImg = $this->config->item('base_url_frontend').'/upload/gallery/'.$gallery['cover_album']; 
						} 
						echo '<input type="hidden" name="gambar_lama" value="../upload/gallery/'.$gallery['cover_album'].'">' ?>
							<img src="<?= $urlImg ?>" class="img-fluid" style="width: 100px;"><br>

							<input type="radio" name="upload_new_file" value="1"> Ganti
							<input type="radio" name="upload_new_file" value="0" checked> Tidak
							<br><br>
						
							<input type="file" id="cover_album" name="cover_album" accept=".png,.jpg,.jpeg">
							<p><small class="text-muted">Mohon upload cover dengan ukuran Max 5Mb</small></p>
						</div>
						
						<button type="submit" name="ubah" class="btn btn-success float-right" >Selesai</button>
						<a href="<?= base_url(); ?>gallery" class="btn btn-info float-left"><i class="fa fa-arrow-left"></i></a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>