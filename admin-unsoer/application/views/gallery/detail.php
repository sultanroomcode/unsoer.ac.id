<div class="container">
	<h5 class="mt-4"><?= $judul ?></h5>
	<div class="row content">
		<div class="col-md-8">
			<a href="<?= base_url(); ?>gallery" class="btn btn-info btn-sm float-left"><i class="fa fa-arrow-left"></i></a>
			<a href="<?= base_url(); ?>gallery/ubah/<?= $gallery['id_gallery']; ?>" class="btn btn-primary btn-sm float-left">Ubah Data Album <i class="fa fa-pencil"></i></a>
			<a href="<?= base_url(); ?>gallery/update_deskripsi/<?= $gallery['id_gallery']; ?>" class="btn btn-primary btn-sm float-left">Lihat Foto Kegiatan <i class="fa fa-picture-o"></i></a>
			<br><br>
			<h6><?= $gallery['create_by']; ?><small class="text-muted float-right"><?= carbonit($gallery['create_time']);  ?></small></h6>
			<?php if($gallery['cover_album'] == 'no-image.png'){ $urlImg = $this->config->item('base_url_frontend').'/assets/images/'.$gallery['cover_album']; } else { $urlImg = $this->config->item('base_url_frontend').'/upload/gallery/'.$gallery['cover_album']; } ?>
			<img src="<?= $urlImg ?>" class="img-fluid"><br>
			<h5><?= $gallery['judul']; ?></h5>
			<p class="text-muted"><?= $gallery['deskripsi']; ?></p>
			
		</div>
	</div>
</div>