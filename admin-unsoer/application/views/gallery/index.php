<div class="container mt-3">
	<?php if ( $this->session->flashdata('flash') ) : ?>
	<div class="row mt-3">
		<div class="col-md-6">
			<div class="alert alert-success alert-dismissible fade show" role="alert">Album Gallery <strong>berhasil </strong><?= $this->session->flashdata('flash'); ?>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<h3 class="text-center mb-4"><?= $judul; ?></h3>
	<div class="row content">
		<div class="col-md-8">
			<a href="<?= base_url(); ?>gallery/tambah" class="btn btn-primary float-left"><i class="fa fa-plus"></i> Album Baru</a> 
			<a href="<?= base_url(); ?>gallery/upload" class="btn btn-primary float-left"><i class="fa fa-image"></i> Upload Foto</a> 
		</div>
		<div class="col-md-4">
			<form action="" method="post">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Cari judul kegiatan.." name="keyword">
					<div class="input-group-append">
						<button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<br>
<div class="container">
	<div class="row">
		<?php foreach ( $gallery as $gal ) { ?>
			<div class="col-lg-4 col-md-4 col-sm-6">
			<?php if($gal['cover_album'] == 'no-image.png'){ $urlImg = $this->config->item('base_url_frontend').'/assets/images/'.$gal['cover_album']; } else { $urlImg = $this->config->item('base_url_frontend').'/upload/gallery/'.$gal['cover_album']; } ?>
			
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary"><?= $gal['judul']; ?></h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Data Album</div>

                      <a class="dropdown-item" href="<?= base_url(); ?>gallery/detail/<?= $gal['id_gallery']; ?>"  >Detail<i class="fa fa-eye float-right text-muted"></i></a>
                      <a class="dropdown-item" href="<?= base_url(); ?>gallery/ubah/<?= $gal['id_gallery']; ?>">Ubah<i class="fa fa-pencil-alt float-right text-muted"></i></a>
                      <a class="dropdown-item" href="<?= base_url(); ?>gallery/hapus/<?= $gal['id_gallery']; ?>" 
                      	onclick="return confirm('yakin hapus Album?');">Hapus<i class="fa fa-trash float-right text-muted"></i></a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="<?= base_url(); ?>gallery/upload_foto/<?= $gal['id_gallery']; ?>">Upload Foto<i class="fa fa-upload float-right text-muted"></i></a>
                      <a class="dropdown-item" href="<?= base_url(); ?>gallery/update_deskripsi/<?= $gal['id_gallery']; ?>">Lihat Foto<i class="fas fa-image float-right text-muted"></i></a>
                      
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area-a">
                    <!-- <canvas id="myAreaChart"></canvas> -->
                   <img src="<?= $urlImg ?>" class="img-fluid">
                  </div>
                </div>
              </div>
        	</div>
          	<?php } ?>

	
	<div class="row mt-3">
		<div class="col-md-12">
			<?php if ( empty($gallery) ) : ?>
					<div class="alert alert-danger" role="alert">
						Album kegiatan tidak ditemukan.
					</div>
				<?php endif; ?>
		</div>
	</div>					
</div>