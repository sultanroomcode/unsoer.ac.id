
<div class="container mt-5">
	<div class="row">
		<div class="col-md-12">
			<a href="<?= base_url(); ?>gallery" class="btn btn-info btn-sm float-left"><i class="fa fa-arrow-left"></i></a>
		</div>
	</div>
	<div class="row">
		<?php foreach($gallery as $f): ?>
			<div class="col-md-3 mt-2">
				
				<span id="span-desc-<?=$f['id_gallery'].'-'.$f['id_foto']?>"><?= $f['deskripsi'] ?></span><br>

				<?php $urlImg = $this->config->item('base_url_frontend').'/upload/gallery/foto-kegiatan/'.$f['file_foto']; ?>
				<img src="<?= $urlImg ?>" class="img-fluid">
				<div class="form-desc-tog" id="form-desc-<?=$f['id_gallery'].'-'.$f['id_foto']?>"></div>

				<a href="javascript:void(0)" data-back="<?=$f['id_gallery'].'-'.$f['id_foto']?>" data-url="<?= base_url(); ?>gallery/form_deskripsi/<?= $f['id_gallery'].'/'.$f['id_foto']; ?>" class="badge badge-info float-left trig_form">edit deskripsi</a>

				<a href="<?= base_url(); ?>gallery/delete/<?= $f['id_gallery'].'/'.$f['id_foto']; ?>" class="badge badge-danger float-left trig_form" onclick="return confirm('yakin hapus foto ?');">hapus</a>

			</div>
	
		<?php endforeach; ?>	
	</div>
</div>