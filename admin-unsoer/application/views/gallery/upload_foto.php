<?php
$maxnum = $this->fkm->maxNumNow($gallery['id_gallery']);
$v = $maxnum->result();
$maxnum_val = ($v[0]->id_foto == null)?0:$v[0]->id_foto;
// var_dump($maxnum_val);
 // var_dump(file_exists('../upload/gallery/foto-kegiatan'))
?>
<div class="container">
	<div class="row mt-3">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div>
				<div class="card-body">
					<form action="<?= base_url().'gallery/upload_foto/'.$gallery['id_gallery']; ?>" method="post" enctype="multipart/form-data">
						
						<input type="hidden" name="id_gallery" value="<?= $gallery['id_gallery']; ?>">
						<input type="hidden" name="id_foto" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="max_now" value="<?= $maxnum_val ?>">
						<input type="hidden" name="deskripsi" value="">
						<input type="hidden" name="create_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="create_time" value="<?= time(); ?>">
						<input type="hidden" name="update_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="update_time" value="<?= time(); ?>">
						
						<h3>Album : <?= $gallery['judul']; ?></h3>
						
						<div class="form-group">
							<label for="foto">Upload foto kegiatan </label><br>
							<input type="file" multiple id="foto" name="file_foto[]" accept=".png,.jpg,.jpeg">
						</div>
						
						<button type="submit" name="upload_foto" class="btn btn-success float-right" >Selesai</button>
						<a href="<?= base_url(); ?>gallery" class="btn btn-info float-left"><i class="fa fa-arrow-left"></i></a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>