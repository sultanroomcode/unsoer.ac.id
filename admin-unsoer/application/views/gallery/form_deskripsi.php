<form class="form_trigger" action="<?=base_url()?>gallery/form_deskripsi/<?=$data['id_gallery']?>/<?=$data['id_foto']?>" method="post">
	<input type="hidden" name="id_gallery" value="<?=$data['id_gallery']?>">
	<input type="hidden" name="id_foto" value="<?=$data['id_foto']?>">
	<textarea name="deskripsi" placeholder="masukkan deskripsi" class="form-control"><?= ($data['data2']['deskripsi']); ?></textarea>
	<button class="btn btn-block btn-sm btn-success">Selesai <i class="fa fa-check"></i></button>
</form>
<script type="text/javascript">
	$(function(){
		//ready
		$(".form_trigger").submit(function(e){
           	//kirimkan data
           	$.post("<?=base_url().'gallery/form_deskripsi/'.$data['id_gallery'].'/'.$data['id_foto']?>", $(this).serialize(), function(data) {
		        var callback = JSON.parse(data);
		        $('#span-desc-<?=$data['id_gallery'].'-'.$data['id_foto']?>').notify(callback.message);
		        $('#form-desc-<?=$data['id_gallery'].'-'.$data['id_foto']?>').hide();
		        $('#span-desc-<?=$data['id_gallery'].'-'.$data['id_foto']?>').text(callback.desc);
		    });
            e.preventDefault();
            // return false;
        });
	});
</script>