<div class="container">
	<div class="row mt-3">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div>
				<div class="card-body">
					<form action="<?= base_url().'gallery/tambah'?>" method="post" enctype="multipart/form-data">
						
						<input type="hidden" name="create_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="create_time" value="<?= time(); ?>">
						<input type="hidden" name="update_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="update_time" value="<?= time(); ?>">
						<input type="hidden" name="foto" value="<?= time(); ?>">
						<div class="form-group">
							<label for="judul">Judul Album</label>
							<input type="text" class="form-control" id="judul" name="judul" required="true">
						</div>
						<div class="form-group">
							<label for="deskripsi">Deskripsi Album</label>
							<input type="text" class="form-control" id="deskripsi" name="deskripsi" required="true">
						</div>
						<div class="form-group">
							<label for="cover_album">Cover Album</label><br>
							<input type="file" id="cover_album" name="cover_album" accept=".png,.jpg,.jpeg">
							<p><small class="text-muted">Mohon upload cover dengan ukuran Max 5Mb</small></p>
						</div>
						<!-- <div class="form-group">
							<label for="judul">Upload Foto Kegiatan</label><br>
							<input type="file" id="judul" name="judul" required="true">
						</div> -->
						<button type="submit" name="tambah" class="btn btn-success float-right" >Selesai</button>
						<a href="<?= base_url(); ?>gallery" class="btn btn-info float-left"><i class="fa fa-arrow-left"></i></a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>