<div class="container mt-5">
	<h3>Pengaturan Ukuran Cover Album</h3>
	<br>
	<div class="row">
		<div class="col-md-4">
			<div id="cropContainerPreload" style="width: 500px; height: 330px;"></div>	
		</div>
	</div>
	<br>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<a href="<?= base_url(); ?>gallery" class="btn btn-info float-left"><i class="fa fa-arrow-left"></i></a>
		</div> 
	</div>
</div>

<script>
<?php
$urlImg = $this->config->item('base_url_frontend').'/upload/gallery/'.$gallery['cover_album'];
$arr = [
	'uploadUrl' => base_url().'gallery/upload-crop',
	'cropUrl' => base_url().'gallery/crop',
	'loadPicture' => $urlImg,
	'baseImage' => '../upload/gallery/'.$gallery['cover_album'],
	'nameCrop' => '../upload/gallery/crop_'.$gallery['cover_album'],
	'name' => 'varCrop',
	'id' => 'cropContainerPreload',
];
echo $this->croppic->js_set_var($arr);
?>
</script>