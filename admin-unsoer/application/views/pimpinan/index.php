<h6 class="text-center">Profil</h6>
<h4 class="text-center"><?= $judul; ?></h4>

<div class="container mt-3">
	<?php if ( $this->session->flashdata('flash') ) : ?>
	<div class="row mt-3">
		<div class="col-md-6">
			<div class="alert alert-success alert-dismissible fade show" role="alert">Pimpinan <strong>berhasil </strong><?= $this->session->flashdata('flash'); ?>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			</div>
		</div>
	</div> 
	<?php endif; ?>

	<div class="row mt-3">
		<div class="col-md-12">
			<?php if ( empty($pimpinan) ) : ?>
				<div class="alert alert-danger" role="alert">
					Data Pimpinan tidak ditemukan.
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<br>
<div class="container">
	<div class="row"> 
		<div class="col-md-12">		
	        <div class="card shadow mb-4">
	            <div class="card-header py-3">
	      			<div class="row">
	      				<div class="col-md-8">
	      					<a href="<?= base_url(); ?>Pimpinan/tambah" class="btn btn-primary float-left"><i class="fas fa-user-plus"></i> Pimpinan Baru</a>
	      				</div>
	      				<div class="col-md-4">
	      					<form action="" method="post">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Cari Nama Pimpinan.." name="keyword">
									<div class="input-group-append">
										<button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
									</div>
								</div>
							</form>
	      				</div>
	      			</div>
	            </div>
	            <div class="card-body">
	              <div class="table-responsive">
	              	<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
	                  <thead>
	                    <tr>
	                      <th>Foto</th>
	                      <th>Nama</th>
	                      <th>Jenis Kelamin</th>
	                      <th>Jabatan</th>
	                      <th>Aksi</th>
	                    </tr>
	                  </thead>
	                  <tbody>
	                  	<?php foreach ( $pimpinan as $kom) { ?>

	                  		<!-- Foto profil -->
	                  		<?php if($kom['foto'] == 'default.png'){ $urlImgProfil = $this->config->item('base_url_frontend').'/assets/images/'.$kom['foto']; } else { $urlImgProfil = $this->config->item('base_url_frontend').'/upload/pimpinan/'.$kom['foto']; } ?>

	                  		<!-- jenis kelamin -->
	                  		<?php if($kom['jenis_kelamin'] == 'L') { $jk = 'Laki-laki'; } else { 
	                  			$jk = 'Perempuan'; }
	                  		?>

	                    <tr>
	                      <td><a href="<?= base_url(); ?>pimpinan/detail/<?= $kom['id']; ?>"><img src="<?= $urlImgProfil; ?>" width="40px" ></a> </td>
	                      <td><?= $kom['nama']; ?></td>
	                      <td><?= $jk; ?></td>
	                      <td><?= $kom['jabatan']; ?></td>
	                      <td>
	                      	<a href="<?= base_url(); ?>pimpinan/hapus/<?= $kom['id']; ?>" class="badge badge-danger float-right" onclick="return confirm('yakin hapus ?');" ><i class="fa fa-trash-alt"></i></a>
				  			<a href="<?= base_url(); ?>pimpinan/ubah/<?= $kom['id']; ?>" class="badge badge-success float-right"><i class="fa fa-pencil-alt"></i></a>
				  			<a href="<?= base_url(); ?>pimpinan/detail/<?= $kom['id']; ?>" class="badge badge-primary float-right"><i class="fa fa-eye"></i></a>
	                      </td>
	                    </tr>
	            		<?php } ?>
	                  </tbody>
	                </table>

	              </div>
	            </div>
	        </div>
        </div>
	</div>
</div>
