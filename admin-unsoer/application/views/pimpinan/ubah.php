<div class="container">
	<div class="row mt-3">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div> 
				<div class="card-body">
					<form action="<?= base_url().'pimpinan/ubah/'.$pimpinan['id']; ?>" method="post" enctype="multipart/form-data">
							
						<input type="hidden" name="id" value="<?= $pimpinan['id']; ?>">
						<input type="hidden" name="create_by" value="<?= $pimpinan['create_by']; ?>">
						<input type="hidden" name="create_time" value="<?= $pimpinan['create_time']; ?>">
						<input type="hidden" name="user_access" value="<?= $pimpinan['user_access']; ?>">

						<div class="form-group">
							<label for="nama">Nama Pimpinan</label>
							<input type="text" class="form-control" id="nama" name="nama" required="true" value="<?= $pimpinan['nama']; ?>">
						</div> 
						<div class="form-group">
							<label for="jenis_kelamin">Jenis Kelamin</label>
							<select class="form-control" id="jenis_kelamin" name="jenis_kelamin">
								<option value="L" <?= ($pimpinan['jenis_kelamin'] == 'L')?'selected':''?>> Laki - laki</option>
								<option value="P" <?= ($pimpinan['jenis_kelamin'] == 'P')?'selected':''?>>Perempuan</option>
							</select>
						</div>
						<div class="form-group">
							<label for="tempat_lahir">Tempat Lahir</label>
							<input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" required="true" value="<?= $pimpinan['tempat_lahir']; ?>">
						</div>
						<div class="form-group">
							<label for="tanggal_lahir">Tanggal Lahir</label>
							<input type="text" class="form-control" id="tanggal_lahir" name="tanggal_lahir" required="true" value="<?= $pimpinan['tanggal_lahir']; ?>">
						</div>
						<div class="form-group">
							<label for="unit_kerja">Unit Kerja</label>
							<input type="text" class="form-control" id="unit_kerja" name="unit_kerja" required="true" value="<?= $pimpinan['unit_kerja']; ?>">
						</div>
						<div class="form-group">
							<label for="jabatan">Jabatan</label>
							<input type="text" class="form-control" id="jabatan" name="jabatan" required="true" value="<?= $pimpinan['jabatan']; ?>">
						</div>
						<div class="form-group"> 
							<label for="pengalaman_organisasi">Tentang</label>
							<textarea name="pengalaman_organisasi" id="editor1" rows="10" cols="80"><?= $pimpinan['pengalaman_organisasi']; ?>
				            </textarea>
						</div>
						<div class="form-group"> 
							<label for="pengalaman_pemilu">Sertifikat</label>
							<textarea name="pengalaman_pemilu" id="editor2" rows="10" cols="80"><?= $pimpinan['pengalaman_pemilu']; ?>
				            </textarea>
						</div>
						<div class="form-group">
							<label for="foto">Ubah Foto Profil</label><br>
							<?php if($pimpinan['foto'] == 'default.png'){ $urlImg = $this->config->item('base_url_frontend').'/assets/images/'.$pimpinan['foto']; 
							} else { $urlImg = $this->config->item('base_url_frontend').'/upload/pimpinan/'.$pimpinan['foto']; } echo '<input type="hidden" name="gambar_lama" value="./bawaslukabmagetan.id/upload/pimpinan/'.$pimpinan['foto'].'">' ?>
							<img src="<?= $urlImg ?>" class="img-fluid" style="width: 100px;"><br>

							<input type="radio" name="upload_new_file" value="1" id="show"> Ganti
							<input type="radio" name="upload_new_file" value="0" id="hide" checked> Tidak
							<br>
							
							<div class="browse mt-1">
								<input type="file" id="foto" name="foto" accept=".png,.jpg,.jpeg">
								<p><small class="text-muted">Mohon upload Foto dengan ukuran Max 5Mb</small></p>
							</div>
						</div>
						
						<button type="submit" name="ubah" class="btn btn-success float-right" >Selesai</button>
						<a href="<?= base_url(); ?>pimpinan" class="btn btn-info float-left"><i class="fa fa-arrow-left"></i></a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		$(".browse").hide();
	  $("#hide").click(function(){
	    $(".browse").hide(500);
	  });
	  $("#show").click(function(){
	    $(".browse").show(500);
	  });
	});
</script>