<div class="container">
	<div class="row mt-3">
		<div class="col-md-12">
			<div class="card mb-3" style="max-width: 840px">
            <div class="row no-gutters">
              <div class="col-md-4 px-2 mt-3">

              	<?php if($pimpinan['foto'] == 'default.png'){ $urlImg = $this->config->item('base_url_frontend').'/assets/images/'.$pimpinan['foto']; } else { $urlImg = $this->config->item('base_url_frontend').'/upload/pimpinan/'.$pimpinan['foto']; } ?>

              	<!-- jenis kelamin -->
          		<?php if($pimpinan['jenis_kelamin'] == 'L') { $jk = 'Laki-laki'; } else { 
          			$jk = 'Perempuan'; }
          		?>

                <img src="<?= $urlImg ?>" class="card-img">
              </div>
              <div class="col-md-8">
                <div class="card-body">
					<h5 class="card-title"><b>Nama : </b><?= $pimpinan['nama']; ?></h5>
					<p class="card-text"><b>Jenis Kelamin : </b><?= $jk; ?></p>
					<p class="card-text"><b>Tempat Lahir : </b><?= $pimpinan['tempat_lahir']; ?></p>
					<p class="card-text"><b>Tanggal Lahir : </b><?= $pimpinan['tanggal_lahir']; ?></p>
					<p class="card-text"><b>Unit Kerja : </b><?= $pimpinan['unit_kerja']; ?></p>
					<p class="card-text"><b>Jabatan : </b><?= $pimpinan['jabatan']; ?></p>
					<p class="card-text"><b>Pengalaman Organisasi : </b><?= $pimpinan['pengalaman_organisasi']; ?></p>
					<p class="card-text"><b>Pengalaman di Kegiatan : </b><?= $pimpinan['pengalaman_pemilu']; ?></p>
					<a href="<?= base_url(); ?>pimpinan" class="btn btn-primary btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
					<a href="<?= base_url(); ?>pimpinan/ubah/<?= $pimpinan['id']; ?>" class="btn btn-info btn-sm"><i class="fa fa-pencil-alt"></i> Ubah Data </a>
                </div>
              </div>
            </div>
          </div>
		</div>
	</div>
</div>