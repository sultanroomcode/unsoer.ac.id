<div class="container">
	<div class="row mt-3">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div> 
				<div class="card-body">
					<form action="<?= base_url().'pimpinan/tambah'?>" method="post" enctype="multipart/form-data">
						
						<input type="hidden" name="create_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="user_access" value="<?= $this->session->user_access; ?>">
						<input type="hidden" name="create_time" value="<?= time(); ?>">					
						<div class="form-group">
							<label for="nama">Nama Pimpinan</label>
							<input type="text" class="form-control" id="nama" name="nama" required="true">
						</div>
						<div class="form-group">
							<label for="jenis_kelamin">Jenis Kelamin</label>
							<select class="form-control" id="jenis_kelamin" name="jenis_kelamin">
								<option value="L">Laki - laki</option>
								<option value="P">Perempuan</option>
							</select>
						</div>
						<div class="form-group">
							<label for="tempat_lahir">Tempat Lahir</label>
							<input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" required="true"> 
						</div>
						<div class="form-group">
							<label for="tanggal_lahir">Tanggal Lahir</label>
							<input type="text" class="form-control" id="tanggal_lahir" name="tanggal_lahir" required="true">
						</div>
						<div class="form-group">
							<label for="unit_kerja">Unit Kerja</label>
							<input type="text" class="form-control" id="unit_kerja" name="unit_kerja" required="true">
						</div>
						<div class="form-group">
							<label for="jabatan">Jabatan</label>
							<input type="text" class="form-control" id="jabatan" name="jabatan" required="true">
						</div>
						<div class="form-group">
							<label for="pengalaman_organisasi">Tentang</label>
							<textarea name="pengalaman_organisasi" id="editor1" rows="10" cols="80">  
				            </textarea>
						</div>
						<div class="form-group">
							<label for="pengalaman_pemilu">Sertifikat</label>
							<textarea name="pengalaman_pemilu" id="editor2" rows="10" cols="80">  
				            </textarea>
						</div>
						<div class="form-group">
							<label for="foto">Foto Profil</label><br>
							<input type="file" id="foto" name="foto" accept=".png,.jpg,.jpeg">
							<p><small class="text-muted">Mohon upload Foto dengan ukuran Max 5Mb</small></p>
						</div>
						
						<button type="submit" name="tambah" class="btn btn-success float-right" >Selesai</button>
						<a href="<?= base_url(); ?>pimpinan" class="btn btn-info float-left"><i class="fa fa-arrow-left"></i></a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>