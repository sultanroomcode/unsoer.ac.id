<div class="container mt-3">
	<?php if ( $this->session->flashdata('flash') ) : ?>
	<div class="row mt-3">
		<div class="col-md-6">
			<div class="alert alert-success alert-dismissible fade show" role="alert">File <strong>berhasil </strong><?= $this->session->flashdata('flash'); ?>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<h3 class="text-center mb-4"><?= $judul; ?></h3>
	<div class="row content">
		
		<div class="col-md-8">
			<a href="<?= base_url(); ?>download/tambah" class="btn btn-primary float-left"><i class="fa fa-plus"></i> File Download Baru</a>
			<!-- <a href="<?= base_url(); ?>kategori_download/index" class="btn btn-primary float-left"><i class="fa fa-bookmark-o"></i> Info Kategori</a> -->
		</div>
		<div class="col-md-4">
			<form action="" method="post">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Cari judul File .." name="keyword">
					<div class="input-group-append">
						<button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
					</div>
				</div>
			</form>
		</div>
		<div class="row mt-3">
			<div class="col-md-12">
				<?php if ( empty($download) ) : ?>
						<div class="alert alert-danger" role="alert">
							Data file tidak ditemukan.
						</div>
					<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="row mt-3">
		<div class="col-md-6">
			<ul class="list-group">
				<?php foreach ($download as $down) { ?>
					<li class="list-group-item"><?= $down['judul']; ?>
			  			<a href="<?= base_url(); ?>download/hapus/<?= $down['id_download']; ?>" class="badge badge-danger float-right" onclick="return confirm('yakin hapus ?');" >hapus</a>
			  			<a href="<?= $this->config->item('base_url_frontend').'/upload/download/'.$down['file_upload']; ?>" target='blank' class="badge badge-primary float-right">lihat file</a>
			  		</li>
				<?php } ?>
			</ul>
		</div>
	</div>
</div>