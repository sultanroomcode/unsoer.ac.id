<div class="container">
	<div class="row mt-3">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div>
				<div class="card-body">
					<form action="<?= base_url().'download/ubah/'.$download['id_download']; ?>" method="post" enctype="multipart/form-data">

						<input type="hidden" name="id_download" value="<?= $download['id_download']; ?>">
						<input type="hidden" name="create_by" value="<?= $download['create_by']; ?>">
						<input type="hidden" name="create_time" value="<?= $download['create_time']; ?>">
						<input type="hidden" name="update_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="update_time" value="<?= time(); ?>">
						

						<div class="form-group">
							<label for="judul">Judul File</label>
							<input type="text" class="form-control" id="judul" name="judul" required="true" value="<?= $download['judul']; ?>">
						</div>
						<div class="form-group">
							<label for="deskripsi">Deskripsi</label>
							<input type="text" class="form-control" id="deskripsi" name="deskripsi" required="true" value="<?= $download['deskripsi']; ?>">
						</div>
						<div class="form-group">
							<label for="id_kategori">Kategori File</label>
							<select class="form-control" id="id_kategori" name="id_kategori">
								<?php foreach ($kategori as $ktg) { ?>
									<?php if ( $ktg['id_kategori'] == $download['id_kategori'] ) : ?>
									<option value="<?= $ktg['id_kategori']; ?>" selected><?= $ktg['nama_kategori']; ?></option>
									<?php else : ?>
										<option value="<?= $ktg['id_kategori']; ?>"><?= $ktg['nama_kategori']; ?></option>
									<?php endif; ?>
								<?php } ?>
							</select>
						</div>
						<div class="form-group">
							<label for="file_upload">Ubah File</label><br>
							<?php if($download['file_upload'] == 'no-image.png'){ $urlImg = $this->config->item('base_url_frontend').'/assets/images/'.$download['file_upload']; 

							} else { $urlImg = $this->config->item('base_url_frontend').'/upload/download/'.$download['file_upload']; } echo '<input type="hidden" name="file_lama" value="../upload/download/'.$download['file_upload'].'">' ?>
							<?= $urlImg;  ?>

							<input type="radio" name="upload_new_file" value="1"> Ganti
							<input type="radio" name="upload_new_file" value="0" checked> Tidak
							<br><br>
						
							<input type="file" id="file_upload" name="file_upload" accept=".png,.jpg,.jpeg">
						</div>

						<!-- <div class="form-group">
							<label for="file_upload">Upload File Upload</label><br>
							<input type="file" id="file_upload" name="file_upload" accept=".xls,.xlsx,.pdf,.doc,.docx,.odt" required="true">
						</div> -->
						<button type="submit" name="tambah" class="btn btn-success float-right" >Selesai <i class="fa fa-check"></i></button>
						<a href="<?= base_url(); ?>download" class="btn btn-info float-left"><i class="fa fa-arrow-left"></i></a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>