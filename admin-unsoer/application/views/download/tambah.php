<div class="container">
	<div class="row mt-3">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div>
				<div class="card-body">
					<form action="<?= base_url().'download/tambah'?>" method="post" enctype="multipart/form-data">
						<input type="hidden" name="create_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="create_time" value="<?= time(); ?>">
						<input type="hidden" name="update_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="update_time" value="<?= time(); ?>">	
						<input type="hidden" name="jumlah_download" value="0">					

						<div class="form-group">
							<label for="judul">Judul File</label>
							<input type="text" class="form-control" id="judul" name="judul" required="true">
						</div>
						<div class="form-group">
							<label for="deskripsi">Deskripsi</label>
							<input type="text" class="form-control" id="deskripsi" name="deskripsi" required="true">
						</div>
						<div class="form-group">
							<label for="id_kategori">Kategori</label>
							<select class="form-control" id="id_kategori" name="id_kategori">
								<?php foreach ($kategori as $ktg) { ?>
									<option value="<?= $ktg['id_kategori']; ?>"><?= $ktg['nama_kategori']; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group">
							<label for="file_upload">Upload File Upload</label><br>
							<input type="file" id="file_upload" name="file_upload" accept=".xls,.xlsx,.pdf,.doc,.docx,.odt" required="true"><br>
							<!-- <small class="text-muted">Upload File Max 5 MB</small> -->
						</div>
						<button type="submit" name="tambah" class="btn btn-success float-right" >Selesai <i class="fa fa-check"></i></button>
						<a href="<?= base_url(); ?>download" class="btn btn-info float-left"><i class="fa fa-arrow-left"></i></a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>