
  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-lg-6">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Login</h1>
                  </div>
                  <?= $this->session->flashdata('message'); ?>
                  <form action="<?= base_url(); ?>login/auth" method="post">
						<div class="form-group">
							<label for="username">Username</label>
							<input type="text" class="form-control" id="username" name="username" required="true">
							
						</div>
						<div class="form-group">
							<label for="password">Password</label>
							<input type="password" class="form-control" id="password" name="password" required="true">
						</div>
						
						<hr>
						<button type="submit" class="btn btn-primary btn-block float-right" >Login</button>
					</form>
    				<br>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

