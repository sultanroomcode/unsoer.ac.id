<div class="container">
	<div class="row mt-3">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div>
				<div class="card-body">
					<form action="<?= base_url().'slider/ubah/'.$slider['id_slider']; ?>" method="post" enctype="multipart/form-data">

						<input type="hidden" name="id_slider" value="<?= $slider['id_slider']; ?>">
						<input type="hidden" name="create_by" value="<?= $slider['create_by']; ?>">
						<input type="hidden" name="user_access" value="<?= $slider['user_access']; ?>">
						<input type="hidden" name="create_time" value="<?= $slider['create_time']; ?>">
						<input type="hidden" name="update_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="update_time" value="<?= time(); ?>">
						<div class="form-group">

							<label for="judul">Judul Slider</label>
							<input type="text" class="form-control" id="judul" name="judul" required="true" value="<?= $slider['judul']; ?>">
						</div>
						<div class="form-group">
							<label for="deskripsi">Deskripsi Slider</label>
							<input type="text" class="form-control" id="deskripsi" name="deskripsi" required="true" value="<?= $slider['deskripsi']; ?>">
						</div>

						<div class="form-group">
							<label for="background">Ubah Background Slider</label><br>
							<?php if($slider['background'] == 'no-image-slide.png'){ $urlImg = $this->config->item('base_url_frontend').'/assets/images/'.$slider['background']; 
							} else { $urlImg = $this->config->item('base_url_frontend').'/upload/slider/'.$slider['background']; } echo '<input type="hidden" name="gambar_lama" value="../demidesa.id/upload/slider/'.$slider['background'].'">' ?>
							<img src="<?= $urlImg ?>" class="img-fluid" style="width: 100px;"><br>

							<input type="radio" name="upload_new_file" value="1"> Ganti
							<input type="radio" name="upload_new_file" value="0" checked> Tidak
							<br><br>
						
							<input type="file" id="background" name="background" accept=".png,.jpg,.jpeg">
							<p><small class="text-muted">Upload cover dengan ukuran Max 3Mb</small></p>
						</div>
						<div class="form-group">
							<label for="link">Ubah Link Slider</label>
							<input type="text" class="form-control" id="link" name="link" placeholder="http://unsoer.ac.id/ ..." value="<?= $slider['link']; ?>">
						</div>

						<!-- <div class="form-group">
							<label for="background">Background Slider</label><br>
							<input type="file" id="background" name="background"><br>
							<small class="text-muted">Mohon upload gambar dengan ukuran 500 x 330 pixel</small>
						</div> -->
						<button type="submit" name="ubah" class="btn btn-success float-right" >Selesai <i class="fa fa-check"></i></button>
							<a href="<?= base_url(); ?>slider" class="btn btn-info float-left">
								<i class="fa fa-arrow-left"></i>
							</a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>