<div class="container">
	<div class="row mt-3">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div>
				<div class="card-body">
					<form action="<?= base_url().'slider/tambah'?>" method="post" enctype="multipart/form-data">
						
						<input type="hidden" name="create_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="user_access" value="<?= $user['user_access']; ?>">
						<input type="hidden" name="create_time" value="<?= time(); ?>">
						<input type="hidden" name="update_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="update_time" value="<?= time(); ?>">
						<div class="form-group">
							<label for="judul">Judul Slider</label>
							<input type="text" class="form-control" id="judul" name="judul" required="true">
						</div>
						<div class="form-group">
							<label for="deskripsi">Deskripsi Slider</label>
							<input type="text" class="form-control" id="deskripsi" name="deskripsi" required="true">
						</div>
						<div class="form-group">
							<label for="background">Background Slider</label><br>
							<input type="file" id="background" name="background"><br>
							<small class="text-muted">Upload gambar dengan ukuran Max 3Mb</small>
						</div>
						<div class="form-group">
							<label for="link">Tambahkan Link Slider</label>
							<input type="text" class="form-control" id="link" name="link" placeholder="Http://unsoer.ac.id/ ...">
						</div>
						<button type="submit" name="tambah" class="btn btn-success float-right" >Selesai <i class="fa fa-check"></i></button>
							<a href="<?= base_url(); ?>slider" class="btn btn-info float-left">
								<i class="fa fa-arrow-left"></i>
							</a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>