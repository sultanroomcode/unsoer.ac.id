<div class="container">
	<div class="row mt-3">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div> 
				<div class="card-body">
					<form action="<?= base_url().'upload_foto/ubah/'.$upload_foto['id']; ?>" method="post" enctype="multipart/form-data">
							
						<input type="hidden" name="id" value="<?= $upload_foto['id']; ?>">
						<input type="hidden" name="create_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="create_time" value="<?= time(); ?>">
						<input type="hidden" name="update_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="update_time" value="<?= time(); ?>">

						

						<div class="form-group">
							<label for="file_foto">Ubah Foto</label><br>
							<?php if($upload_foto['file_foto'] == 'default.png'){ $urlImg = $this->config->item('base_url_frontend').'/assets/images/'.$upload_foto['file_foto']; 
							} else { $urlImg = $this->config->item('base_url_frontend').'/upload/upload-foto/'.$upload_foto['file_foto']; } echo '<input type="hidden" name="gambar_lama" value="./magetan.bawaslu.go.id/upload/upload-foto/'.$upload_foto['file_foto'].'">' ?>
							<img src="<?= $urlImg ?>" class="img-fluid" style="width: 100px;"><br>

							<input type="radio" name="upload_new_file" value="1" id="show"> Ganti
							<input type="radio" name="upload_new_file" value="0" id="hide" checked> Tidak
							<br>
							
							<div class="browse mt-1">
								<input type="file" id="file_foto" name="file_foto" accept=".png,.jpg,.jpeg">
								<p><small class="text-muted">Mohon upload Foto dengan ukuran Max 5Mb</small></p>
							</div>
						</div>
						
						<div class="form-group">
							<label for="deskripsi">Deskripsi</label><br>
							<textarea name="deskripsi" rows="5" cols="80"><?= $upload_foto['deskripsi']; ?> 
				            </textarea>
						</div>

						<button type="submit" name="ubah" class="btn btn-success float-right" >Selesai</button>
						<a href="<?= base_url(); ?>upload_foto" class="btn btn-info float-left"><i class="fa fa-arrow-left"></i></a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		$(".browse").hide();
	  $("#hide").click(function(){
	    $(".browse").hide(500);
	  });
	  $("#show").click(function(){
	    $(".browse").show(500);
	  });
	});
</script>