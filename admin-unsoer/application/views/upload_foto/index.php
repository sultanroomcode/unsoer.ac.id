<h6 class="text-center">Gallery</h6>
<h4 class="text-center"><?= $judul; ?></h4>

<div class="container mt-3">
	<?php if ( $this->session->flashdata('flash') ) : ?>
	<div class="row mt-3">
		<div class="col-md-6">
			<div class="alert alert-success alert-dismissible fade show" role="alert">Foto <strong>berhasil </strong><?= $this->session->flashdata('flash'); ?>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			</div>
		</div>
	</div> 
	<?php endif; ?>

	<div class="row mt-3">
		<div class="col-md-12">
			<?php if ( empty($upload_foto) ) : ?>
				<div class="alert alert-danger" role="alert">
					Data Foto tidak ditemukan.
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<br>
<div class="container">
	<div class="row"> 
		<div class="col-md-12">		
	        <div class="card shadow mb-4">
	            <div class="card-header py-3">
	      			<div class="row">
	      				<div class="col-md-8">
	      					<a href="<?= base_url(); ?>upload_foto/tambah" class="btn btn-primary float-left"><i class="fa fa-upload"></i> Upload Foto</a>
	      				</div>
	      				<div class="col-md-4">
	      					<form action="" method="post">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Cari foto.." name="keyword">
									<div class="input-group-append">
										<button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
									</div>
								</div>
							</form>
	      				</div>
	      			</div>
	            </div>
	            <div class="card-body">
	              <div class="table-responsive">
	              	<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
	                  <thead>
	                    <tr>
	                      <th>Foto</th>
	                      <th>Deskripsi</th>
	                      <th>Aksi</th>
	                    </tr>
	                  </thead>
	                  <tbody>
	                  	<?php foreach ( $upload_foto as $foto) { ?>

	                  		<!-- Foto profil -->
	                  		<?php if($foto['file_foto'] == 'default.png'){ $urlImgProfil = $this->config->item('base_url_frontend').'/assets/images/'.$foto['file_foto']; } else { $urlImgProfil = $this->config->item('base_url_frontend').'/upload/upload-foto/'.$foto['file_foto']; } ?>

	                    <tr>
	                      <td><img src="<?= $urlImgProfil; ?>" width="100px" > </td>
	                      <td><?= $foto['deskripsi']; ?></td>
	                      <td>
	                      	<a href="<?= base_url(); ?>upload_foto/hapus/<?= $foto['id']; ?>" class="badge badge-danger float-right" onclick="return confirm('yakin hapus ?');" ><i class="fa fa-trash-alt"></i></a>
				  			<a href="<?= base_url(); ?>upload_foto/ubah/<?= $foto['id']; ?>" class="badge badge-success float-right"><i class="fa fa-pencil-alt"></i></a>
				  			<!-- <a href="<?= base_url(); ?>upload_foto/detail/<?= $foto['id']; ?>" class="badge badge-primary float-right"><i class="fa fa-eye"></i></a> -->
	                      </td>
	                    </tr>
	            		<?php } ?>
	                  </tbody>
	                </table>

	              </div>
	            </div>
	        </div>
        </div>
	</div>
</div>
