<div class="container">
	<div class="row mt-3">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<?= $judul;  ?>
				</div> 
				<div class="card-body">
					<form action="<?= base_url().'upload_foto/tambah'?>" method="post" enctype="multipart/form-data">
						<input type="hidden" name="create_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="create_time" value="<?= time(); ?>">
						<input type="hidden" name="update_by" value="<?= $this->session->ses_id; ?>">
						<input type="hidden" name="update_time" value="<?= time(); ?>">
						
						<div class="form-group">
							<label for="file_foto">Foto Kegiatan</label><br>
							<input type="file" id="file_foto" name="file_foto" accept=".png,.jpg,.jpeg">
							<p><small class="text-muted">Mohon upload Foto dengan ukuran Max 5Mb</small></p>
						</div>

						<div class="form-group">
							<label for="deskripsi">Deskripsi</label>
						    <textarea class="form-control" id="deskripsi" name="deskripsi" required="true"></textarea>
						</div>

						
						<button type="submit" name="tambah" class="btn btn-success float-right" >Selesai</button>
						<a href="<?= base_url(); ?>upload_foto" class="btn btn-info float-left"><i class="fa fa-arrow-left"></i></a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>