<?php  

/**
 * Fakultas
 */
class Fakultas extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Fakultas_model');
	}

	/*public function index()
	{
		$data['judul'] = 'Fakultas';
		$data['slider'] = $this->Fakultas_model->getAllSlider();
		$data['pimpinan'] = $this->Fakultas_model->getAllPimpinan();
		$data['artikel'] = $this->Fakultas_model->getAllArtikel();

		$this->load->view('templates/header', $data);
		$this->load->view('fakultas/index', $data);
		$this->load->view('templates/footer'); 
	}*/
	
	public function hukum()
	{
		$n = 2;
		$data['judul'] = 'Fakultas Hukum';
		$data['slider'] = $this->Fakultas_model->getAllSlider($n);
		$data['pimpinan'] = $this->Fakultas_model->getAllPimpinan($n);
		$data['artikel'] = $this->Fakultas_model->getAllArtikel($n);

		$this->load->view('templates/header', $data);
		$this->load->view('fakultas/hukum', $data);
		$this->load->view('templates/footer'); 
	}

	public function fisip()
	{
		$n = 3;
		$data['judul'] = 'Fakultas Ilmu Sosial dan Ilmu Politik';
		$data['slider'] = $this->Fakultas_model->getAllSlider($n);
		$data['pimpinan'] = $this->Fakultas_model->getAllPimpinan($n);
		$data['artikel'] = $this->Fakultas_model->getAllArtikel($n);

		$this->load->view('templates/header', $data);
		$this->load->view('fakultas/administrasi-negara', $data);
		$this->load->view('templates/footer'); 
	} 

	public function pertanian()
	{
		$n = 4;
		$data['judul'] = 'Fakultas Pertanian';
		$data['slider'] = $this->Fakultas_model->getAllSlider($n);
		$data['pimpinan'] = $this->Fakultas_model->getAllPimpinan($n);
		$data['artikel'] = $this->Fakultas_model->getAllArtikel($n);

		$this->load->view('templates/header', $data);
		$this->load->view('fakultas/agroteknologi', $data);
		$this->load->view('templates/footer'); 
	} 

	public function ekonomi()
	{
		$n = 5;
		$data['judul'] = 'Fakultas Ekonomi';
		$data['slider'] = $this->Fakultas_model->getAllSlider($n);
		$data['pimpinan'] = $this->Fakultas_model->getAllPimpinan($n);
		$data['artikel'] = $this->Fakultas_model->getAllArtikel($n);

		$this->load->view('templates/header', $data);
		$this->load->view('fakultas/manajemen', $data);
		$this->load->view('templates/footer'); 
	} 
	public function teknik()
	{
		$n = 6;
		$data['judul'] = 'Fakultas Teknik';
		$data['slider'] = $this->Fakultas_model->getAllSlider($n);
		$data['pimpinan'] = $this->Fakultas_model->getAllPimpinan($n);
		$data['artikel'] = $this->Fakultas_model->getAllArtikel($n);

		$this->load->view('templates/header', $data);
		$this->load->view('fakultas/teknik-sipil', $data);
		$this->load->view('templates/footer'); 
	}
}