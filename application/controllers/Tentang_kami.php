<?php 
/**
 * 
 */
class Tentang_kami extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Home_model');
		$this->load->model('Artikel_model');
		$this->load->model('Kategori_model');
	}


	public function index()
	{
		$data['judul'] = 'Profil';
		$data['tentang_kami'] = $this->Home_model->getAllTentangKami();
		$this->load->view('templates/header', $data);
		$this->load->view('tentang_kami/index', $data);
		$this->load->view('templates/footer');
	}
}