<?php 
/**
 * 
 */
class Profil extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('Profil_model');
		
	}

	public function index()
	{
		$data['judul'] = 'Profil Universitas';
		$data['universitas'] = $this->Profil_model->getAllProfil();
		
		$this->load->view('templates/header', $data);
		$this->load->view('profil/index', $data);
		$this->load->view('templates/footer');
	}

	public function sejarah()
	{
		$data['judul'] = 'Sejarah Soerjo';
		$data['sejarah'] = $this->Profil_model->getAllSejarah();
		
		$this->load->view('templates/header', $data);
		$this->load->view('profil/sejarah', $data);
		$this->load->view('templates/footer');
	}

	public function visimisi()
	{
		$data['judul'] = 'Visi Misi';
		$data['visimisi'] = $this->Profil_model->getAllVisimisi();
		
		$this->load->view('templates/header', $data);
		$this->load->view('profil/visimisi', $data);
		$this->load->view('templates/footer');
	}
}