<?php  

/**
 * Fasilitas
 */
class Fasilitas extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Fasilitas_model');
	}

	public function ruangkuliah()
	{
		$data['judul'] = 'Fasilitas Ruang Kuliah';
		// $data['fasilitas'] = $this->Fasilitas_model->getAllFasilitas();
		$data['fasilitas'] = $this->Fasilitas_model->getAllFasilitasRuangKuliah();

		$this->load->view('templates/header', $data);
		$this->load->view('fasilitas/ruang-kuliah', $data);
		$this->load->view('templates/footer'); 
	}

	public function laboratorium()
	{
		$data['judul'] = 'Fasilitas Laboratorium';
		$data['fasilitas'] = $this->Fasilitas_model->getAllFasilitasLaboratorium();

		$this->load->view('templates/header', $data);
		$this->load->view('fasilitas/laboratorium', $data);
		$this->load->view('templates/footer'); 
	}

	public function gedung()
	{
		$data['judul'] = 'Fasilitas Gedung';
		$data['fasilitas'] = $this->Fasilitas_model->getAllFasilitasGedung();

		$this->load->view('templates/header', $data);
		$this->load->view('fasilitas/gedung', $data);
		$this->load->view('templates/footer'); 
	}

	public function lainnya()
	{
		$data['judul'] = 'Fasilitas Lainnya';
		$data['fasilitas'] = $this->Fasilitas_model->getAllFasilitasLainnya();

		$this->load->view('templates/header', $data);
		$this->load->view('fasilitas/lainnya', $data);
		$this->load->view('templates/footer'); 
	}
}