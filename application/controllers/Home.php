<?php 
/**
 * Beranda 
 */
class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		// $this->load->model('Home_model');
		$this->load->model('Artikel_model');
		$this->load->model('Artikel_baru_model', 'abm_model');
		$this->load->model('Home_model');
		$this->load->model('Video_model');
	}
	 
	public function error404()
	{
		$data['judul'] = 'Halaman Tidak ditemukan | Universitas Soerjo Ngawi';
		
		$this->load->view('templates/header', $data);
		$this->load->view('home/error404');
		$this->load->view('templates/footer');
	}

	public function index ($nama = '')
	{
		$data['judul'] = 'Universitas Soerjo Ngawi';
		$data['nama'] = $nama;
		// $this->db->order_by('artikel.create_time', 'DESC');
		$this->db->limit(6);
		$data['artikel'] = $this->abm_model->getAllArtikel();
		// $data['gallery'] = $this->Gallery_model->getAllGallery();
		$data['slider'] = $this->abm_model->getAllSlider(); 
		$data['tentang_kami'] = $this->Home_model->getAllTentangKami();
		$data['pimpinan'] = $this->Home_model->getAllPimpinan();
		$data['video'] = $this->Video_model->getAllVideo();
		$this->load->view('templates/header', $data);
		$this->load->view('home/index', $data);
		$this->load->view('templates/footer');
	}
}