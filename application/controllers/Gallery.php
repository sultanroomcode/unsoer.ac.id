<?php 

/**
 * Gallery
 */
class Gallery extends CI_Controller
{ 
 
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Gallery_model');
		
	}

	public function index()
	{
		$data['judul'] = 'Gallery';
		$data['upload_foto'] = $this->Gallery_model->getAllGallery();
		$this->load->view('templates/header', $data);
		$this->load->view('gallery/index', $data);
		$this->load->view('templates/footer');
	}

	public function detail($id)
	{
		$data['judul'] = 'Detail Gallery';
		$data['upload_foto'] = $this->Gallery_model->getGalleryById($id);
		// $data['gallery'] = $this->Gallery_model->getAllFotoKegiatanFromGallery($id_gallery);
		$this->load->view('templates/header', $data);
		$this->load->view('gallery/detail', $data);
		$this->load->view('templates/footer');
	}

	public function foto_kegiatan($id_gallery)
	{
		$data['judul'] = 'Foto Kegiatan';
		$data['gallery'] = $this->Foto_kegiatan_model->getAllFotoKegiatanFromGallery($id_gallery);
		
		$this->load->view('templates/header', $data);
		$this->load->view('gallery/foto_kegiatan', $data);
		$this->load->view('templates/footer');
		
	}


}