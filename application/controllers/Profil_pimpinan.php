<?php 
/**
 * 
 */
class Profil_pimpinan extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('Profil_pimpinan_model');
		
	}

	public function index()
	{
		$data['judul'] = 'Profil pimpinan';
		$data['pimpinan'] = $this->Profil_pimpinan_model->getAllProfilPimpinan(1);
		
		$this->load->view('templates/header', $data);
		$this->load->view('pimpinan/index', $data);
		$this->load->view('templates/footer');
	}

	public function detail($id)
	{
		$data['judul'] = 'Detail Pimpinan';
		
		$data['pimpinan'] = $this->Profil_pimpinan_model->getPimpinanById($id);
		
		$this->load->view('templates/header', $data);
		$this->load->view('pimpinan/detail', $data);
		$this->load->view('templates/footer');
	}
}