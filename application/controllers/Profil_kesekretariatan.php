<?php 
/**
 * 
 */
class Profil_Kesekretariatan extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct(); 
		
		$this->load->model('Profil_kesekretariatan_model');
		
	}

	public function index()
	{
		$data['judul'] = 'Profil Kesekretariatan';
		$data['profil_kesekretariatan'] = $this->Profil_kesekretariatan_model->getAllProfilKesekretariatan();
		
		$this->load->view('templates/header', $data);
		$this->load->view('profil_kesekretariatan/index', $data);
		$this->load->view('templates/footer');
	}
}