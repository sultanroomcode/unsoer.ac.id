<?php  

/**
 * Artikel
 */
class Artikel extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Artikel_model');
		$this->load->model('Artikel_baru_model', 'abm_model');
		$this->load->model('Kategori_model');
		// $this->load->library('form_validation');
	}
	
	public function index ($nama = '')
	{
		$data['judul'] = 'Unsoer Ngawi';
		$data['nama'] =$nama;
		$this->db->order_by('artikel.create_time', 'DESC');
		$data['artikel'] = $this->Artikel_model->getAllArtikel();
		$this->load->view('templates/header', $data);
		$this->load->view('artikel/index', $data);
		$this->load->view('templates/footer');
	}

	public function detail($id)
	{
		$data['judul'] = 'Detail Artikel';
		// $data['kategori'] = $this->Kategori_model->getAllKategori($id);
		$data['artikel'] = $this->abm_model->getArtikelBySlug($id);
		//tambah 1
		$this->abm_model->addReadOne($id);
		$this->load->view('templates/header', $data);
		$this->load->view('artikel/detail', $data);
		$this->load->view('templates/footer');
	}

	public function ajaxartikel($id_kategori)
	{
		$data['artikel'] = $this->Artikel_model->getArtikelByIdKategori($id_kategori);
		$this->load->view('artikel/ajax-artikel', $data);
	}

	public function ajaxartikeldetail($id_kategori)
	{
		$data['artikel'] = $this->Artikel_model->getArtikelByIdKategori($id_kategori);
		$this->load->view('artikel/ajax-artikel-detail', $data);
	}
}