<?php  

/**
 * Prodi
 */
class Prodi extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Fakultas_model');
	}

	public function administrasi_negara()
	{
		$data['judul'] = 'Prodi Ilmu Administrasi Negara';
		$data['slider'] = $this->Fakultas_model->getAllSlider(10);
		$data['pimpinan'] = $this->Fakultas_model->getAllPimpinan(10);
		$data['artikel'] = $this->Fakultas_model->getAllArtikel(10);

		$this->load->view('templates/header', $data);
		$this->load->view('prodi/prodi-view', $data);
		$this->load->view('templates/footer'); 
	} 
	public function ilmu_hukum()
	{
		$data['judul'] = 'Prodi Ilmu Hukum';
		$data['slider'] = $this->Fakultas_model->getAllSlider(11);
		$data['pimpinan'] = $this->Fakultas_model->getAllPimpinan(11);
		$data['artikel'] = $this->Fakultas_model->getAllArtikel(11);

		$this->load->view('templates/header', $data);
		$this->load->view('prodi/prodi-view', $data);
		$this->load->view('templates/footer'); 
	}

	public function agroteknologi()
	{
		$data['judul'] = 'Prodi Agroteknologi';
		$data['slider'] = $this->Fakultas_model->getAllSlider(12);
		$data['pimpinan'] = $this->Fakultas_model->getAllPimpinan(12);
		$data['artikel'] = $this->Fakultas_model->getAllArtikel(12);

		$this->load->view('templates/header', $data);
		$this->load->view('prodi/prodi-view', $data);
		$this->load->view('templates/footer'); 
	} 

	public function manajemen()
	{
		$data['judul'] = 'Prodi Manajemen';
		$data['slider'] = $this->Fakultas_model->getAllSlider(13);
		$data['pimpinan'] = $this->Fakultas_model->getAllPimpinan(13);
		$data['artikel'] = $this->Fakultas_model->getAllArtikel(13);

		$this->load->view('templates/header', $data);
		$this->load->view('prodi/prodi-view', $data);
		$this->load->view('templates/footer'); 
	} 
	public function sipil()
	{
		$data['judul'] = 'Prodi Teknik Sipil';
		$data['slider'] = $this->Fakultas_model->getAllSlider(14);
		$data['pimpinan'] = $this->Fakultas_model->getAllPimpinan(14);
		$data['artikel'] = $this->Fakultas_model->getAllArtikel(14);

		$this->load->view('templates/header', $data);
		$this->load->view('prodi/prodi-view', $data);
		$this->load->view('templates/footer'); 
	}

	public function mesin()
	{
		$data['judul'] = 'Prodi Teknik Mesin';
		$data['slider'] = $this->Fakultas_model->getAllSlider(15);
		$data['pimpinan'] = $this->Fakultas_model->getAllPimpinan(15);
		$data['artikel'] = $this->Fakultas_model->getAllArtikel(15);

		$this->load->view('templates/header', $data);
		$this->load->view('prodi/prodi-view', $data);
		$this->load->view('templates/footer'); 
	}  
}