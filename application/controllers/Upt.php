<?php  

/**
 * Upt
 */
class Upt extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Upt_model');
	}

	public function bau()
	{
		$au = 16;
		$data['judul'] = 'UPT BAU';
		$data['slider'] = $this->Upt_model->getAllSlider($au);
		$data['pimpinan'] = $this->Upt_model->getAllPimpinan($au);
		$data['artikel'] = $this->Upt_model->getAllArtikel($au);

		$this->load->view('templates/header', $data);
		$this->load->view('upt/upt-view', $data);
		$this->load->view('templates/footer'); 
	} 
	public function baak()
	{
		$au = 17;
		$data['judul'] = 'UPT BAAK';
		$data['slider'] = $this->Upt_model->getAllSlider($au);
		$data['pimpinan'] = $this->Upt_model->getAllPimpinan($au);
		$data['artikel'] = $this->Upt_model->getAllArtikel($au);

		$this->load->view('templates/header', $data);
		$this->load->view('upt/upt-view', $data);
		$this->load->view('templates/footer'); 
	}

	public function lppm()
	{
		$au = 18;
		$data['judul'] = 'UPT LPPM';
		$data['slider'] = $this->Upt_model->getAllSlider($au);
		$data['pimpinan'] = $this->Upt_model->getAllPimpinan($au);
		$data['artikel'] = $this->Upt_model->getAllArtikel($au);

		$this->load->view('templates/header', $data);
		$this->load->view('upt/upt-view', $data);
		$this->load->view('templates/footer'); 
	} 

	public function lpm()
	{
		$au = 19;
		$data['judul'] = 'UPT LPM';
		$data['slider'] = $this->Upt_model->getAllSlider($au);
		$data['pimpinan'] = $this->Upt_model->getAllPimpinan($au);
		$data['artikel'] = $this->Upt_model->getAllArtikel($au);

		$this->load->view('templates/header', $data);
		$this->load->view('upt/upt-view', $data);
		$this->load->view('templates/footer'); 
	} 

	public function perpustakaan()
	{
		$au = 20;
		$data['judul'] = 'UPT Perpustakaan';
		$data['slider'] = $this->Upt_model->getAllSlider($au);
		$data['pimpinan'] = $this->Upt_model->getAllPimpinan($au);
		$data['artikel'] = $this->Upt_model->getAllArtikel($au);

		$this->load->view('templates/header', $data);
		$this->load->view('upt/upt-view', $data);
		$this->load->view('templates/footer'); 
	} 

	public function pusat_karir()
	{
		$au = 21;
		$data['judul'] = 'UPT Pusat Karir';
		$data['slider'] = $this->Upt_model->getAllSlider($au);
		$data['pimpinan'] = $this->Upt_model->getAllPimpinan($au);
		$data['artikel'] = $this->Upt_model->getAllArtikel($au);

		$this->load->view('templates/header', $data);
		$this->load->view('upt/upt-view', $data);
		$this->load->view('templates/footer'); 
	} 
}