<?php 

/**
 * Mahasiswa Controller
 */
class Mahasiswa extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Mahasiswa_new_model','mhs_model');
		$this->load->library('form_validation');
	}

	public function aturan_akademik($nama = '')
	{
		$data['judul'] = 'Aturan Akademik - Universita Soeryo Ngawi';
		$data['nama'] =$nama;
		// $this->db->order_by('artikel.create_time', 'DESC');
		// $data['artikel'] = $this->Artikel_model->getAllArtikel();
		$data['artikel'] = $this->mhs_model->getAllKonten('aturan-akademik');
		$this->load->view('templates/header', $data);
		$this->load->view('mahasiswa/mahasiswa-view', $data);
		$this->load->view('templates/footer');
	}

	public function ekstrakurikuler($nama = '')
	{
		$data['judul'] = 'Ekstrakurikuler - Universita Soeryo Ngawi';
		$data['nama'] =$nama;
		// $this->db->order_by('artikel.create_time', 'DESC');
		// $data['artikel'] = $this->Artikel_model->getAllArtikel();
		$data['artikel'] = $this->mhs_model->getAllKonten('ekstrakurikuler');
		$this->load->view('templates/header', $data);
		$this->load->view('mahasiswa/mahasiswa-view', $data);
		$this->load->view('templates/footer');
	}

	public function prestasi($nama = '')
	{
		$data['judul'] = 'Prestasi Mahasiswa - Universita Soeryo Ngawi';
		$data['nama'] =$nama;
		// $this->db->order_by('artikel.create_time', 'DESC');
		// $data['artikel'] = $this->Artikel_model->getAllArtikel();
		$data['artikel'] = $this->mhs_model->getAllKonten('prestasi');
		$this->load->view('templates/header', $data);
		$this->load->view('mahasiswa/mahasiswa-view', $data);
		$this->load->view('templates/footer');
	}



	/*public function index ()
	{
		$data['judul'] = 'Daftar Mahasiswa';
		$data['mahasiswa'] = $this->Mahasiswa_model->getAllMahasiswa();
		$this->load->view('templates/header', $data);
		$this->load->view('mahasiswa/index', $data);
		$this->load->view('templates/footer');
	}*/

	/*public function tambah () 
	{
		$data['judul'] = 'Form Tambah Data Mahasiswa';

		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('nrp', 'NRP', 'required|numeric');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');

		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('mahasiswa/tambah', $data);
			$this->load->view('templates/footer');
		} else {
			$this->Mahasiswa_model->tambahDataMahasiswa();
			$this->session->set_flashdata('flash', 'ditambahkan');
			redirect('mahasiswa');
		}
	}

	public function hapus($id)
	{
		$this->Mahasiswa_model->hapusDataMahasiswa($id);
		$this->session->set_flashdata('flash', 'dihapus');
		redirect('mahasiswa');
	}*/
}