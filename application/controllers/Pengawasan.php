<?php 

/**
 * Pengawasan
 */
class Pengawasan extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		// $this->load->model('Putusan_jatim_model');
	}
	
	public function index()
	{
		$data['judul'] = 'Pojok Pengawasan';
		// $data['putusan_jatim'] = $this->Putusan_jatim_model->getAllPutusanJatim();
		$this->load->view('templates/header', $data);
		$this->load->view('pengawasan/index', $data);
		$this->load->view('templates/footer');
	}

	public function file($id)
	{
		$putusan_jatim = $this->Download_model->getPutusanJatimById($id);
		$this->Putusan_jatim_model->addDownloadOne($id);
		force_download('upload/putusan_jatim/'.$putusan_jatim['file_upload'], null, true);
	}
}