<?php 

/**
 * Kontak
 */
class Kontak extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Artikel_model');
		$this->load->model('Kategori_model');
	}
	
	public function index()
	{
		$data['judul'] = 'Kontak Kami';
		// $data['artikel'] = $this->Artikel_model->getAllArtikel();
		$this->load->view('templates/header', $data);
		$this->load->view('kontak/index', $data);
		$this->load->view('templates/footer');
	}
}