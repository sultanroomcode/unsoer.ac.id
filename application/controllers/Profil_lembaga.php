<?php 
/**
 * 
 */
class Profil_lembaga extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('Profil_lembaga_model');
		
	}

	public function index()
	{
		$data['judul'] = 'Profil Lembaga';
		$data['profil_lembaga'] = $this->Profil_lembaga_model->getAllProfilLembaga();
		
		$this->load->view('templates/header', $data);
		$this->load->view('profil_lembaga/index', $data);
		$this->load->view('templates/footer');
	}
}