<?php 

/**
 * Video_kegiatan 
 */
class Video_kegiatan extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Video_kegiatan_model');
		
	}
	
	public function index()
	{
		$data['judul'] = 'Video Kegiatan';
		$data['video_kegiatan'] = $this->Video_kegiatan_model->getAllVideoKegiatan();
		$this->load->view('templates/header', $data);
		$this->load->view('video_kegiatan/index', $data);
		$this->load->view('templates/footer');
	}
}