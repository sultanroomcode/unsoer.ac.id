<?php  

/**
 * Artikel
 */
class Penelitian_inovasi extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Jurnal_model');
		$this->load->model('Penelitian_inovasi_model', 'pi_model');
		$this->load->model('Artikel_model');
		$this->load->model('Kategori_model');
		// $this->load->library('form_validation');
	}
	
	public function index($nama = '')
	{
		$data['judul'] = 'Unsoer Ngawi';
		$data['nama'] =$nama;
		// $this->db->order_by('artikel.create_time', 'DESC');
		$data['jurnal'] = $this->Jurnal_model->getAllJurnal();
		$this->load->view('templates/header', $data);
		$this->load->view('jurnal/index', $data);
		$this->load->view('templates/footer');
	}

	public function jurnal($nama = '')
	{
		$data['judul'] = 'Jurnal - Universita Soeryo Ngawi';
		$data['nama'] =$nama;
		// $this->db->order_by('artikel.create_time', 'DESC');
		// $data['artikel'] = $this->Artikel_model->getAllArtikel();
		$data['jurnal'] = $this->Jurnal_model->getAllJurnal();
		$this->load->view('templates/header', $data);
		$this->load->view('jurnal/index', $data);
		$this->load->view('templates/footer');
	}

	public function penelitian($nama = '')
	{
		$data['judul'] = 'Penelitian - Universita Soeryo Ngawi';
		$data['nama'] =$nama;
		// $this->db->order_by('artikel.create_time', 'DESC');
		// $data['artikel'] = $this->Artikel_model->getAllArtikel();
		$data['artikel'] = $this->pi_model->getAllKonten('penelitian');
		$this->load->view('templates/header', $data);
		$this->load->view('jurnal/penelitian', $data);
		$this->load->view('templates/footer');
	}

	public function inovasi($nama = '')
	{
		$data['judul'] = 'Inovasi - Universita Soeryo Ngawi';
		$data['nama'] =$nama;
		// $this->db->order_by('artikel.create_time', 'DESC');
		// $data['artikel'] = $this->Artikel_model->getAllArtikel();
		$data['artikel'] = $this->pi_model->getAllKonten('inovasi');
		$this->load->view('templates/header', $data);
		$this->load->view('jurnal/penelitian', $data);
		$this->load->view('templates/footer');
	}

	public function riset_inovasi($nama = '')
	{
		$data['judul'] = 'Riset dan Inovasi - Universita Soeryo Ngawi';
		$data['nama'] =$nama;
		// $this->db->order_by('artikel.create_time', 'DESC');
		// $data['artikel'] = $this->Artikel_model->getAllArtikel();
		$data['artikel'] = $this->pi_model->getAllKonten('riset-inovasi');
		$this->load->view('templates/header', $data);
		$this->load->view('jurnal/penelitian', $data);
		$this->load->view('templates/footer');
	}

	public function Pengabdian_masyarakat($nama = '')
	{
		$data['judul'] = 'Pengabdian Masyarakat - Universita Soeryo Ngawi';
		$data['nama'] =$nama;
		// $this->db->order_by('artikel.create_time', 'DESC');
		// $data['artikel'] = $this->Artikel_model->getAllArtikel();
		$data['artikel'] = $this->pi_model->getAllKonten('pengabdian-masyarakat');
		$this->load->view('templates/header', $data);
		$this->load->view('jurnal/penelitian', $data);
		$this->load->view('templates/footer');
	}

	public function detail_jurnal($id)
	{
		$data['judul'] = 'Detail Artikel';
		// $data['kategori'] = $this->Kategori_model->getAllKategori($id);
		$data['jurnal'] = $this->Jurnal_model->getJurnalBySlug($id);
		//tambah 1
		// $this->Artikel_model->addReadOne($id);

		$this->load->view('templates/header', $data);
		$this->load->view('jurnal/jurnal-detail', $data);
		$this->load->view('templates/footer');
	}

	public function download_jurnal($id)
	{
		$this->load->helper('download');
		$data = $this->Jurnal_model->getJurnalBySlug($id);
		$dirfile = 'upload/jurnal/'.$data['upload_jurnal'];
		force_download($dirfile, null);
	}

	public function ajaxartikel($id_kategori)
	{
		$data['artikel'] = $this->Artikel_model->getArtikelByIdKategori($id_kategori);
		$this->load->view('artikel/ajax-artikel', $data);
	}

	public function ajaxartikeldetail($id_kategori)
	{
		$data['artikel'] = $this->Artikel_model->getArtikelByIdKategori($id_kategori);
		$this->load->view('artikel/ajax-artikel-detail', $data);
	}
}