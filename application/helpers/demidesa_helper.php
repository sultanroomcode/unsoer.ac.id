<?php 
	function carbonit($date)
	{
		Carbon\Carbon::setLocale('id'); 
		// $tanggal = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date);
		$tanggal = Carbon\Carbon::createFromTimestamp($date);
    	// echo $date;   // output:  2017-01-21 00:00:00
    	// echo '<br/>'.$date->diffForHumans();  // output: 7 hours ago 
		return $tanggal->diffForHumans();
	}	
	function tanggal($date) 
	{
		return date('d F Y', $date);
	}
 ?>