<?php
class MY_Model extends CI_Model{
	public $tableName, $limit, $offset, $where_condition =null, $page_count;
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->setLimit(1, 5);
	}

	public function setTableName($name)
	{
		$this->tableName = $name;
	}

	public function setLimit($page, $perpage){
		$this->offset = ($page - 1) * $perpage;
		$this->limit = $perpage;
	}

	public function setWhere($val){
		$this->where_condition = $val;
	}

	function getdata(){
		if($this->where_condition != null && is_array($this->where_condition))
		{
			$this->db->where($this->where_condition);
		}
		return $this->db->get($this->tableName);
	}

	function page(){
		$this->db->limit($this->limit, $this->offset);
		return $this->get();
	}

	function page_total(){
		$total = $this->get()->num_rows();
		$total_page = ceil($total/$this->limit);
		return $total_page;
	}

	function one($key){
		$this->db->where($key);
		return $this->get();
	}
	
	function input_data($data){
		return $this->db->insert($this->tableName,$data);
	}

	function update_data($data, $key){
		return $this->db->update($this->tableName,$data, $key);
	}

	function delete_data($key){
		return $this->db->delete($this->tableName,$key);
	}

	public function generate_paging($url, $total, $now=1)
    {
        $txt = [];        
        for($i = 1; $i <= $total; $i++){
            if($i == $now){
                $txt[] = '<li class="page-item active"><span class="page-link">'.$i.'<span class="sr-only">(current)</span></span></li>';
            } else {
            	if($i == 1 || $i == $total || ($i >= $now - 2 && $i <= $now + 2)){
                	$txt[] = '<li class="page-item"><a href="'.base_url($url).'/'.$i.'" class="page-link">'.$i.'</a>';
                }
            }
        }

        return $txt;
    }
}
