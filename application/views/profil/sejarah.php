
<!-- Page Content-->
<section class="section novi-background bg-cover section-70 section-md-114 bg-default">
    <div class="shell text-left">
      <div class="range range range-xs-center">
        <div class="cell-xs-10 cell-sm-8">
          <?php foreach ($sejarah as $sj) { ?>
          <h4 class="text-bold"><?= $sj['judul']; ?></h4>
          <p><?= $sj['deskripsi']; ?></p>
         <?php } ?>
      </div>
    </div>
</section>
