
<!-- Page Content-->
<section class="section novi-background bg-cover section-70 section-md-114 bg-default">
    <div class="shell text-left">
      <div class="range range range-xs-center">
        <div class="cell-xs-10 cell-sm-8">
          <?php foreach ($universitas as $univ) { ?>
          <h4 class="text-bold"><?= $univ['judul']; ?></h4>
          <p><?= $univ['deskripsi']; ?></p>
         <?php } ?>
      </div>
    </div>
</section>
