
<!-- Page Content-->
<section class="section novi-background bg-cover section-70 section-md-114 bg-default">
    <div class="shell text-left">
      <div class="range range range-xs-center">
        <div class="cell-xs-10 cell-sm-8">
          <?php foreach ($visimisi as $vm) { ?>
          <h4 class="text-bold"><?= $vm['visi']; ?></h4>
          <p><?= $vm['deskripsi_visi']; ?></p>
          <br>
          <h4 class="text-bold"><?= $vm['misi']; ?></h4>
          <p><?= $vm['deskripsi_misi']; ?></p>
         <?php } ?>
      </div>
    </div>
</section>
