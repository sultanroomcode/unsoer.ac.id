<div class="container">
	 <div class="bg-white ">
        <div class="row">
            <div class="col-md-8 mt-3 content border">                
               <?php foreach ($profil_lembaga as $pl) { ?>
               	
                    <h3 class="artikel-judul-detail pl-3 ml-3">
                        <?= $pl['judul']; ?>
                    </h3>                   
                     <div class="content-isi-artikel mt-3 pl-3 px-3"><?= $pl['deskripsi']; ?></div>
               <?php } ?>
            </div>
            <div class="col-md-4 mt-3">
            	
            </div>
        </div>
    </div>
</div>
