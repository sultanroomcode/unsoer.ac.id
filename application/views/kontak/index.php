<div class="container-fluid bg-content-grey">
     <div class="container">
        <div class="row">
            <div class="col-md-8 bg-content-artikel">
                <br>
                <h3 class="text-center title">
                    KONTAK KAMI
                </h3>
                <h3 class="mbr-section-subtitle align-center mbr-light pb-3 mbr-fonts-style display-5">
                    
                </h3> 
            
                <form class="mbr-form" action="https://mobirise.com/" method="post" data-form-title="Mobirise Form"><input type="hidden" name="email" data-form-email="true" value="DwhXBQKe7m7npkoNXc4x19Xkh2dtRqQZgkkBtJDYi/+/aH2IuxDS29tB11bZ06vzpUEiQYCDYH9vMRMCg8IM9kn16mOqW9Ks4ZoSxUsKAhiYqDdY05FTOmnq5zackXWZ">
                        <div class="row row-sm-offset">
                            <div class="col-md-4 multi-horizontal" data-for="name">
                                <div class="form-group">
                                    <label class="form-control-label mbr-fonts-style display-7" for="name-form1-15">Nama</label>
                                    <input type="text" class="form-control" name="name" data-form-field="Name" required="" id="name-form1-15" placeholder="nama saya ...">
                                </div>
                            </div>
                            <div class="col-md-4 multi-horizontal" data-for="email">
                                <div class="form-group">
                                    <label class="form-control-label mbr-fonts-style display-7" for="email-form1-15">Email</label>
                                    <input type="email" class="form-control" name="email" data-form-field="Email" required="" id="email-form1-15" placeholder="email saya ...">
                                </div>
                            </div>
                            <div class="col-md-4 multi-horizontal" data-for="phone">
                                <div class="form-group">
                                    <label class="form-control-label mbr-fonts-style display-7" for="phone-form1-15">Telp</label>
                                    <input type="tel" class="form-control" name="phone" data-form-field="Phone" id="phone-form1-15" placeholder="+62 ..">
                                </div>
                            </div>
                        </div>
                        <div class="form-group" data-for="message">
                            <label class="form-control-label mbr-fonts-style display-7" for="message-form1-15">Pesan</label>
                            <textarea type="text" class="form-control" name="message" rows="7" data-form-field="Message" id="message-form1-15" placeholder="isi pesan ..."></textarea>
                        </div>
            
                        <span class="input-group-btn  float-right">
                            <button href="" type="submit" class="btn btn-primary btn-form display-4">KIRIM PESAN<i class="fa fa-send"></i></button>
                        </span>
                    </form>
            </div>

            <!-- sidebar -->
            <div class="col-md-4 sidebar">
                <?php $this->load->view('templates/com_last_artikel') ?>
            </div>
        </div>
    </div>
</div>