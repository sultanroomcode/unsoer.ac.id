<div class="container">
	 <div class="bg-white">
        <div class="row">
            <div class="col-md-8 mt-3 content border">                
               <?php foreach ($profil_kesekretariatan as $sk) { ?>
               	
                <div class="px-5 mt-3">
                    <h5><b><?= $sk['judul']; ?></b></h5><br>
                    <p class="mb-1"><b>Kepala Sekretariat : </b><?= $sk['kepala_sekretariat']; ?></p>
                    <p class="mb-1"><b>Bendahara : </b><?= $sk['bendahara']; ?></p>
                    <p class="mb-1"><b>PBP : </b><?= $sk['PBP']; ?></p>
                    <p class="mb-1"><b>PPHP : </b><?= $sk['PPHP']; ?></p>
                    <p class="mb-1"><b>Staf Admin : </b><?= $sk['staf_admin']; ?></p>
                    <p class="mb-1"><b>Staf Teknis : </b><?= $sk['staf_teknis']; ?></p>
                    <p class="mb-1"><b>Staf Non Teknis : </b><?= $sk['staf_non_teknis']; ?></p>
                </div>
                     
               <?php } ?>
            </div>
            <div class="col-md-4 mt-3">
            	
            </div>
        </div>
    </div>
</div>
