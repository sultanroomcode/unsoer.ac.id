<!-- Slider -->
 <section class="section">
    <!-- Swiper-->
    <div class="swiper-container swiper-slider swiper-slider-3" data-loop="true" data-slide-effect="true">
      <div class="swiper-wrapper">
        <?php foreach ($slider as $sli) { ?>

          <?php if($sli['background'] == 'no-image-slide.png'){ $imgSlide = base_url().'assets/images/'.$sli['background']; } else { $imgSlide = base_url().'upload/slider/'.$sli['background']; } ?>

        <div class="swiper-slide" data-slide-bg="<?= $imgSlide ?>" style="background-position: 80% center">
          <div class="swiper-slide-caption section-70">
            <div class="container">
              <div class="range range-xs-center range-lg-left">
                <div class="cell-md-9 text-md-left cell-xs-10">
                  <div data-caption-animate="fadeInUp" data-caption-delay="100">
                    <h1 class="text-bold text-shadow"><?= $sli['judul']; ?></h1>
                  </div>
                  <div class="offset-top-20 offset-xs-top-40 offset-xl-top-60 inset-lg-right-100" data-caption-animate="fadeInUp" data-caption-delay="150">
                    <h5><?= $sli['deskripsi']; ?></h5>
                  </div>
                  <div class="offset-top-20 offset-xl-top-40" data-caption-animate="fadeInUp" data-caption-delay="400">
                    <div class="group-xl group-middle"><a class="btn btn-primary" href="#">Selengkapnya</a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>

      </div>
      <!-- Swiper Pagination-->
      <div class="swiper-pagination"></div>
    </div>
</section>
<!-- Akhir Slider -->


<!--People-->
<section class="section novi-background bg-cover section-70 section-md-114 bg-default">
    <div class="shell">
      <h2 class="text-bold">Pimpinan <?= $judul; ?></h2>
      <hr class="divider bg-madison">
      <div class="range range-30 text-md-left offset-top-60">

        <?php foreach ($pimpinan as $p) { ?>

          <?php if($p['foto'] == 'default.png'){ $imgPimpinan = base_url().'assets/images/'.$p['foto']; } else { $imgPimpinan = base_url().'upload/pimpinan/'.$p['foto']; } ?>

        <div class="cell-sm-6 cell-md-3"><img class="img-responsive reveal-inline-block img-rounded" src="<?= $imgPimpinan ?>" width="270" height="270" alt="">
          <div class="offset-top-20">
            <h6 class="text-bold text-primary"><a href="<?= base_url(); ?>profil_pimpinan/detail/<?= $p['id']; ?>"><?= $p['nama']; ?></a></h6>
          </div>
          <div class="offset-top-5">
            <p><?= $p['jabatan']; ?></p>
          </div>
        </div>
        <?php } ?>

      </div>
    </div>
</section>
<!-- Akhir People -->

 <!-- Latest news-->
      <section class="section novi-background bg-cover section-70 section-md-114 bg-catskill">
        <div class="shell">
          <div class="range range-85 range-xs-center">
            <div class="cell-md-8">
              <div class="range range-30 text-sm-left range-xs-center">
                
                <?php foreach ($artikel as $art) { ?>
                <div class="col-xs-12 col-sm-6 col-md-6 isotope-item">
                  <article class="post-news"><a href="<?= base_url(); ?>artikel/detail/<?= $art['slug']; ?>">

                    <?php if($art['cover_img'] == 'no-image.png'){ $urlImg = base_url().'assets/images/'.$art['cover_img']; } else { $urlImg = base_url().'/upload/artikel/'.$art['cover_img']; } ?>

                    <img class="img-responsive box-shadow" src="<?= $urlImg ?>" width="370" height="240" alt=""></a>
                    <div class="post-news-body box-shadow">
                      <h6><a href="<?= base_url(); ?>artikel/detail/<?= $art['slug']; ?>"><?= $art['judul'];?></a></h6>
                      <div class="offset-top-20">
                        <p>Liberal Arts Colleges emphasize undergraduate education and award at least half of their degrees in the liberal arts fields of study.</p>
                      </div>
                      <div class="post-news-meta offset-top-20"><span class="icon novi-icon icon-xs mdi mdi-calendar-clock text-middle text-madison"></span><span class="text-middle inset-left-10 text-italic text-black"><?= tanggal($art['create_time']);  ?></span></div>
                    </div>
                  </article>
                </div>
                <?php } ?>
        
              </div>
            </div>
            <!-- aside-->
            <div class="cell-xs-8 cell-md-4 text-left">
              <aside class="aside inset-md-left-30">
                <div class="aside-item">
                  <!-- Archive-->
                  <h6 class="text-bold">Pengumuman</h6>
                  <div class="text-subline"></div>
                  <div class="row offset-top-20">
                    <div class="col-xs-6">
                      <ul class="list list-marked list-marked-primary">
                        <li><a href="#">Jun 2018</a></li>
                        <li><a href="#">Aug 2018</a></li>
                        <li><a href="#">Oct 2018</a></li>
                        <li><a href="#">Dec 2018</a></li>
                        <li><a href="#">Feb 2017</a></li>
                      </ul>
                    </div>
                    <div class="col-xs-6">
                      <ul class="list list-marked list-marked-primary">
                        <li><a href="#">Jul 2018</a></li>
                        <li><a href="#">Sep 2018</a></li>
                        <li><a href="#">Nov 2018</a></li>
                        <li><a href="#">Jan 2018</a></li>
                        <li><a href="#">Mar 2017</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="aside-item">
                  <!-- Flickr Feed-->
                  <h6 class="text-bold">Agenda</h6>
                  <div class="text-subline"></div>
                  
                </div>
                <div class="aside-item">
                  <!-- Categories-->
                  <h6 class="text-bold">Categories</h6>
                  <div class="text-subline"></div>
                  <div class="offset-top-20">
                    <ul class="list list-marked list-marked-primary">
                      <li><a href="#">News</a></li>
                      <li><a href="#">University</a></li>
                      <li><a href="#">Global Education</a></li>
                      <li><a href="#">Law</a></li>
                      <li><a href="#">Colleges</a></li>
                    </ul>
                  </div>
                </div>
              </aside>
            </div>
          </div>
        </div>
      </section>