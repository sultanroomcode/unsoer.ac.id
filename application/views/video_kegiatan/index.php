
  <div class="container bg-white">
      <div class="row">
         <div class="col-md-12 mt-3 download"> 
            <h4 class="ml-4 mt-3 mb-5 judul "><?= $judul; ?></h4>
            <div class="row">
             <?php foreach ($video_kegiatan as $vk) { ?>
                  <div class="col-md-4 mt-3 mb-3">
                   <?= $vk['judul']; ?>
                     <div class="embed-responsive embed-responsive-16by9">
                       <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?=$vk['link'];?>?rel=0" allowfullscreen>
                       </iframe>
                     </div>
                  </div>
             <?php } ?>
            </div>
         </div>
      </div>
   </div>
