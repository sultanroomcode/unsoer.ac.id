 <!--4 Columns Layout-->
  <section class="section novi-background bg-cover section-70 section-md-114 bg-default">
    <div class="shell">
      <div class="range range-30 range-xs-center">
        <div class="cell-sm-4 text-sm-left">
            <?php if($pimpinan['foto'] == 'default.png'){ $urlImg = base_url().'/assets/images/'.$pimpinan['foto']; } else { $urlImg = base_url().'/upload/pimpinan/'.$pimpinan['foto']; } ?>

          <div class="inset-sm-right-30"><img class="img-responsive reveal-inline-block" src="<?= $urlImg ?>" width="340" height="340" alt="">
            <!-- <div class="offset-top-15 offset-sm-top-30"><a class="btn btn-primary btn-block" href="#" style="max-width: 340px; margin-left:auto; margin-right:auto">Get in Touch</a></div> -->
            <div class="offset-top-15 offset-sm-top-30">
              <ul class="list list-unstyled">
                <li><span class="icon novi-icon icon-xs mdi mdi-phone text-middle text-madison"></span><a class="reveal-inline-block text-dark inset-left-10" href="tel:#">0851 7817 092</a></li>
                <li><span class="icon novi-icon icon-xs mdi mdi-email-open text-middle text-madison"></span><a class="reveal-inline-block inset-left-10" href="mailto:info@demolink.org">info@gmail.com</a></li>
              </ul>
            </div>
            <div class="offset-top-15 offset-sm-top-30">
              <ul class="list-inline list-inline-xs list-inline-madison">
                <li><a class="icon novi-icon icon-xxs fa-facebook icon-circle icon-gray-light-filled" href="#"></a></li>
                <li><a class="icon novi-icon icon-xxs fa-twitter icon-circle icon-gray-light-filled" href="#"></a></li>
                <li><a class="icon novi-icon icon-xxs fa-google icon-circle icon-gray-light-filled" href="#"></a></li>
                <li><a class="icon novi-icon icon-xxs fa-instagram icon-circle icon-gray-light-filled" href="#"></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="cell-sm-8 text-left">
          <div>
            <h2 class="text-bold"><?= $pimpinan['nama']; ?></h2>
          </div>
          <h5 class="offset-top-10"><?= $pimpinan['jabatan']; ?></h5>
          <div class="offset-top-15 offset-sm-top-30">
            <hr class="divider bg-madison hr-left-0">
          </div>
          <div class="offset-top-30 offset-sm-top-60">
            <h6 class="text-bold">Tentang <?= $pimpinan['nama']; ?></h6>
            <div class="text-subline"></div>
          </div>
          <div class="offset-top-20">
            <p><?=$pimpinan['pengalaman_organisasi'] ?></p>
          </div>
          <div class="offset-top-30 offset-sm-top-60">
            <h6 class="text-bold">Sertifikat & Penelitian</h6>
            <div class="text-subline"></div>
          </div>

          <div class="offset-top-20">
            <p><?=$pimpinan['pengalaman_pemilu'] ?></p>
          </div>
          
        </div>
      </div>
    </div>
</section>


