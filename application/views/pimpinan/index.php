<!--People-->
<section class="section novi-background bg-cover section-70 section-md-114 bg-default">
    <div class="shell">
      <h2 class="text-bold">Pimpinan Universitas</h2>
      <hr class="divider bg-madison">
      <div class="range range-30 text-md-left offset-top-60">

        <?php foreach ($pimpinan as $p) { ?>

          <?php if($p['foto'] == 'default.png'){ $imgPimpinan = base_url().'assets/images/'.$p['foto']; } else { $imgPimpinan = base_url().'upload/pimpinan/'.$p['foto']; } ?>

        <div class="cell-sm-6 cell-md-3"><img class="img-responsive reveal-inline-block img-rounded" src="<?= $imgPimpinan ?>" width="270" height="270" alt="">
          <div class="offset-top-20">
            <h6 class="text-bold text-primary"><a href="<?= base_url(); ?>profil_pimpinan/detail/<?= $p['id']; ?>"><?= $p['nama']; ?></a></h6>
          </div>
          <div class="offset-top-5">
            <p><?= $p['jabatan']; ?></p>
          </div>
        </div>
        <?php } ?>

      </div>
    </div>
</section>
<!-- Akhir People -->


