<div class="container-fluid">
<div class="container foto-kegiatan">
    <div class="row">  
        <div class="col-md-12">
            <section class="mbr-gallery mbr-slider-carousel cid-rb7SQVasCr-foto" id="gallery1-l">
            <div><!-- Filter --><!-- Gallery -->                
                <a href="<?= base_url(); ?>gallery" class="float-right" > <small>foto lainnya</small></a>
                <br>
                <!-- <h6 class="text-muted mt-2">Album Kegiatan Kami</h6>                              -->
                <div class="mbr-gallery-row">
                    <div class="mbr-gallery-layout-default">
                        <?php $num = 0; foreach ($gallery as $fk) { ?>
                        <?php $urlImg = base_url().'/upload/gallery/foto-kegiatan/'.$fk['file_foto']; ?>
 
                        <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Awesome">
                            <div href="#lb-gallery1-l" data-slide-to="<?= $num; ?>" data-toggle="modal">
                                <img src="<?= $urlImg ?>" alt="" title="">
                                <span class="icon-focus"></span><span class="mbr-gallery-title mbr-fonts-style display-7"><?= $fk['deskripsi']; ?></span>
                            </div>
                        </div>    
                        <div class="clearfix"></div>
                        <?php
                        $num++; 
                    } ?> 
                    </div>
                </div><!-- Lightbox -->

                <div data-app-prevent-settings="" class="mbr-slider modal fade carousel slide" tabindex="-1" data-keyboard="true" data-interval="false" id="lb-gallery1-l">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="carousel-inner">

                                    <?php $num = 0; foreach ($gallery as $fk) { ?>
                                        <?php $urlImg = base_url().'/upload/gallery/foto-kegiatan/'.$fk['file_foto']; ?>  
                                        <div class="carousel-item <?= ($num==0)?'active':''?>">
                                            <img src="<?= $urlImg ?>" alt="" title="">
                                        </div>

                                        <?php 
                                        $num++;
                                    } ?> 

                                    
                                </div>
                                <a class="carousel-control carousel-control-prev" role="button" data-slide="prev" href="#lb-gallery1-l">
                                    <span class="mbri-left mbr-iconfont" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control carousel-control-next" role="button" data-slide="next" href="#lb-gallery1-l">
                                    <span class="mbri-right mbr-iconfont" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                                <a class="close" href="#" role="button" data-dismiss="modal">
                                    <span class="sr-only">Close</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                               
            </div>
        </section>
        </div>
    </div>
</div>
</div>