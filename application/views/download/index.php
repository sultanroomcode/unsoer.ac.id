<div class="container-fluid bg-content-grey">
     <div class="container">
        <div class="row">
            <div class="col-md-8 download bg-content-artikel">
                <div class="section-head text-center space30">
                    <h3 class="text-center title mb-5">
                        Download
                    </h3>
                </div>
                <div class="clearfix"></div>
                <div id="bootstrap-accordion_36" class="panel-group accordionStyles accordion" role="tablist" aria-multiselectable="true">
                    <?php $i=1; foreach ($download as $dn) { ?>
                        
                    <div class="card">
                        <div class="card-header" role="tab" id="heading<?= $i; ?>">
                            <a role="button" class="panel-title collapsed text-black" data-toggle="collapse" data-core="" href="#collapse<?= $i; ?>_36" aria-expanded="false" aria-controls="collapse<?= $i; ?>">
                                <h4 class="mbr-fonts-style display-7">
                                    <span class="sign mbr-iconfont mbri-arrow-down inactive float-right"></span>
                                    <?= $dn['nama_kategori']; ?>
                                </h4>
                            </a>
                        </div>
                        <div id="collapse<?= $i; ?>_36" class="panel-collapse noScroll collapse " role="tabpanel" aria-labelledby="heading<?= $i; ?>" data-parent="#bootstrap-accordion_36">
                            <div class="panel-body p-4">
                                <p class="mbr-fonts-style panel-text display-7">
                                    <?php 
                                    $src = $this->Download_model->getAllFileFromKategoriDownload($dn['id_kategori']);
                                    foreach ($src as $d) {

                                    echo '<div class="row mb-2 bg-download-file"><div class="col-md-10"><a href="'.base_url().'download/file/'.$d['id_download'].'">'.$d['judul'].'</a></div>
                                            <div class="col-md-2">
                                                <span class="text-muted float-right f10"> '.$d['jumlah_download'].' </span>
                                            </div>
                                        </div>';
                                        }
                                    ?>
                                         
                                </p>
                            </div>
                        </div>
                    </div>

                    <?php 
                    $i++;
                } ?>

                </div>
            </div>
            
            <!-- sidebar -->
            <div class="col-md-4 sidebar">
                <?php $this->load->view('templates/com_last_artikel') ?>
            </div>
        </div>
    </div>
</div>