<!-- Slider -->
 <section class="section">
    <!-- Swiper-->
    <div class="swiper-container swiper-slider swiper-slider-3" data-loop="true" data-slide-effect="true">
      <div class="swiper-wrapper">
        <?php foreach ($slider as $sli) { ?>

          <?php if($sli['background'] == 'no-image-slide.png'){ $imgSlide = base_url().'assets/images/'.$sli['background']; } else { $imgSlide = base_url().'upload/slider/'.$sli['background']; } ?>

        <div class="swiper-slide" data-slide-bg="<?= $imgSlide ?>" style="background-position: 80% center">
          <div class="swiper-slide-caption section-70">
            <div class="container">
              <div class="range range-xs-center range-lg-left">
                <div class="cell-md-9 text-md-left cell-xs-10">
                  <div data-caption-animate="fadeInUp" data-caption-delay="100">
                    <h1 class="text-bold text-shadow"><?= $sli['judul']; ?></h1>
                  </div>
                  <div class="offset-top-20 offset-xs-top-40 offset-xl-top-60 inset-lg-right-100" data-caption-animate="fadeInUp" data-caption-delay="150">
                    <h5><?= $sli['deskripsi']; ?></h5>
                  </div>
                  <div class="offset-top-20 offset-xl-top-40" data-caption-animate="fadeInUp" data-caption-delay="400">
                    <div class="group-xl group-middle"><a class="btn btn-primary" href="#">Selengkapnya</a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>

      </div>
      <!-- Swiper Pagination-->
      <div class="swiper-pagination"></div>
    </div>
</section>
<!-- Akhir Slider -->

<!--People-->
<section class="section novi-background bg-cover section-70 section-md-114 bg-default">
    <div class="shell">
      <h2 class="text-bold">Pimpinan <?= $judul; ?></h2>
      <hr class="divider bg-madison">
      <div class="range range-30 text-md-left offset-top-60">

        <?php foreach ($pimpinan as $p) { ?>

          <?php if($p['foto'] == 'default.png'){ $imgPimpinan = base_url().'assets/images/'.$p['foto']; } else { $imgPimpinan = base_url().'upload/pimpinan/'.$p['foto']; } ?>

        <div class="cell-sm-6 cell-md-3"><img class="img-responsive reveal-inline-block img-rounded" src="<?= $imgPimpinan ?>" width="270" height="270" alt="">
          <div class="offset-top-20">
            <h6 class="text-bold text-primary"><a href="<?= base_url(); ?>profil_pimpinan/detail/<?= $p['id']; ?>"><?= $p['nama']; ?></a></h6>
          </div>
          <div class="offset-top-5">
            <p><?= $p['jabatan']; ?></p>
          </div>
        </div>
        <?php } ?>

      </div>
    </div>
</section>
<!-- Akhir People -->
