<section class="section novi-background bg-cover section-70 section-md-114 bg-default">
    <div class="shell text-left">
      <div class="range range range-xs-center">
        <div class="cell-xs-12 cell-sm-12">
          <h4 class="text-bold">Daftar Jurnal dan Publikasi</h4>
          <p>
            <table class="table table-striped">
                <tr>
                    <th>Judul</th>
                    <th>Tanggal Upload</th>
                    <th>Penulis</th>
                    <th>Aksi</th>
                </tr>
            <?php foreach ($jurnal as $jurn) { ?>
                <tr>
                    <td><?= $jurn['judul_jurnal']; ?></td>
                    <td><?= tanggal($jurn['create_time']); ?></td>
                    <td><?= $jurn['penulis']; ?></td>
                    <td>
                        <a href="<?= base_url(); ?>penelitian-inovasi/detail-jurnal/<?= $jurn['slug']; ?>">detail</a>      
                    </td>
                </tr>
            <?php } ?>
            </table>
          </p>
      </div>
    </div>
</section>