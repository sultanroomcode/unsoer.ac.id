<style type="text/css">
a.bordbottom {
    border-bottom: 1px dotted black;
    padding-bottom: 2px;
}
</style>
<section class="section novi-background bg-cover section-70 section-md-114 bg-default">
    <div class="shell text-left">
      <div class="range range range-xs-center">
        <div class="cell-xs-12 cell-sm-12">
          <h4 class="text-bold">Daftar Jurnal dan Publikasi</h4>
          <p>
            <table class="table table-striped">
                <tr>
                    <th>Judul</th>
                    <td><?= $jurnal['judul_jurnal'] ?></td>
                </tr>
                
                <tr>
                    <th>Penulis</th>
                    <td><?= $jurnal['penulis'] ?></td>
                </tr>

                <tr>
                    <th>Tahun Terbit</th>
                    <td><?= $jurnal['tahun_terbit'] ?></td>
                </tr>

                <tr>
                    <th>Volume</th>
                    <td><?= $jurnal['volume'] ?></td>
                </tr>

                <tr>
                    <th>Tanggal Upload</th>
                    <td><?= $jurnal['judul_jurnal'] ?></td>
                </tr>

                <tr>
                    <th>Jumlah Download</th>
                    <td><?= $jurnal['jumlah_baca'] ?></td>
                </tr>

                <tr>
                    <th>Aksi</th>
                    <td><a class="bordbottom" href="<?= base_url(); ?>penelitian-inovasi/download-jurnal/<?= $jurnal['slug']; ?>">download</a></td>
                </tr>
            </table>
          </p>
      </div>
    </div>
</section>