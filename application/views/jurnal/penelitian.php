<section class="section novi-background bg-cover section-70 section-md-114 bg-catskill">
    <div class="shell">
      <div class="range range-85 range-xs-center">
        <div class="cell-md-8">
          <div class="range range-30 text-sm-left range-xs-center">
            
            <?php foreach ($artikel as $art) { ?>
            <div class="col-xs-12 col-sm-6 col-md-6 isotope-item">
              <article class="post-news"><a href="<?= base_url(); ?>artikel/detail/<?= $art['slug']; ?>">

                <?php if($art['thumb_img'] == 'no-image.png'){ $urlImg = base_url().'assets/images/'.$art['thumb_img']; } else { $urlImg = base_url().'/upload/artikel/'.$art['thumb_img']; } ?>

                <img class="img-responsive box-shadow" src="<?= $urlImg ?>" width="370" height="240" alt=""></a>
                <div class="post-news-body box-shadow">
                  <h6><a href="<?= base_url(); ?>artikel/detail/<?= $art['slug']; ?>"><?= $art['title'];?></a></h6>
                  <div class="offset-top-20">
                    <p><?= $art['content'];?></p>
                  </div>
                  <div class="post-news-meta offset-top-20"><span class="icon novi-icon icon-xs mdi mdi-calendar-clock text-middle text-madison"></span><span class="text-middle inset-left-10 text-italic text-black"><?= tanggal($art['created_at']);  ?></span></div>
                </div>
              </article>
            </div>
            <?php } ?>
    
          </div>
        </div>
        <!-- aside-->
        <div class="cell-xs-8 cell-md-4 text-left">
          <aside class="aside inset-md-left-30">
            <div class="aside-item">
              <!-- Archive-->
              <h6 class="text-bold">Pengumuman</h6>
              <div class="text-subline"></div>
              <div class="row offset-top-20">
                <div class="col-xs-6">
                  <ul class="list list-marked list-marked-primary">
                    <li><a href="#">Jun 2018</a></li>
                    <li><a href="#">Aug 2018</a></li>
                    <li><a href="#">Oct 2018</a></li>
                    <li><a href="#">Dec 2018</a></li>
                    <li><a href="#">Feb 2017</a></li>
                  </ul>
                </div>
                <div class="col-xs-6">
                  <ul class="list list-marked list-marked-primary">
                    <li><a href="#">Jul 2018</a></li>
                    <li><a href="#">Sep 2018</a></li>
                    <li><a href="#">Nov 2018</a></li>
                    <li><a href="#">Jan 2018</a></li>
                    <li><a href="#">Mar 2017</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="aside-item">
              <!-- Flickr Feed-->
              <h6 class="text-bold">Agenda</h6>
              <div class="text-subline"></div>
              
            </div>
            <div class="aside-item">
              <!-- Categories-->
              <h6 class="text-bold">Categories</h6>
              <div class="text-subline"></div>
              <div class="offset-top-20">
                <ul class="list list-marked list-marked-primary">
                  <li><a href="#">News</a></li>
                  <li><a href="#">University</a></li>
                  <li><a href="#">Global Education</a></li>
                  <li><a href="#">Law</a></li>
                  <li><a href="#">Colleges</a></li>
                </ul>
              </div>
            </div>
          </aside>
        </div>
      </div>
    </div>
  </section>