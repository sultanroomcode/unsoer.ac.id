<section class="">
    <div class="container">
        <div class="bg-white download">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <?php foreach ($artikel as $art) { ?>
                        <div class="card p-3 col-md-4 col-sm-4 col-lg-6">
                            <div class="card-wrapper borderpad">
                              <a href="<?= base_url(); ?>artikel/detail/<?= $art['slug']; ?>">
                                <div class="card-img">
                                    <?php if($art['cover_img'] == 'no-image.png'){ $urlImg = base_url().'/assets/images/'.$art['cover_img']; } else { $urlImg = base_url().'/upload/artikel/'.$art['cover_img']; } ?>
                                    <img src="<?= $urlImg ?>" class="img-fluid">
                                </div>
                                <div class="card-box">
                                    <!-- <h6 class="title-card-box float-left"><i class="fa fa-bookmark i-index"></i> <?= $art['nama_kategori'];  ?> </h6> -->
                                    <!-- <span class="title-card-box-span float-right"><i class="fa fa-user i-index"></i> <?= $art['username'];  ?></span> -->
                                    <!-- <div class="clearfix"></div> -->
                                    <small class="i-index"><i class="fa fa-clock-o"></i> <?= tanggal($art['create_time']);  ?></small>
                                    <h4 class="card-title mbr-fonts-style display-4">
                                        <?= $art['judul']; ?>
                                    </h4>
                                </div>
                                <!-- <small class="float-left i-index"><i class="fa fa-eye"></i> <?= $art['jumlah_baca'];  ?></small> -->
                                
                              </a>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>

                <!-- sidebar -->
                <div class="col-md-4 sidebar">
                    <?php $this->load->view('templates/com_last_artikel') ?>
                </div>
            </div>
        </div>
    </div>
</section>