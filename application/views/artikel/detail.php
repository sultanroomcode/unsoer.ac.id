<!-- Latest news-->
<section class="section novi-background bg-cover section-70 section-md-114 bg-default">
    <div class="shell">
        <div class="range range-50 range-xs-center">
            <div class="cell-sm-8 cell-md-8 text-md-left">
                <h2 class="text-bold">
                    <?= $artikel['title']; ?>
                </h2>
                <hr class="divider bg-madison hr-md-left-0">
                <div class="offset-md-top-47 offset-top-20">
                    <ul class="post-news-meta list list-inline list-inline-xl">
                        <li>
                            <span class="icon novi-icon icon-xs mdi mdi-calendar-clock text-middle text-madison"></span>
                            <span class="text-middle inset-left-10 text-italic text-black"><?= tanggal($artikel['created_at']);  ?>
                                
                            </span>
                        </li>
                        <li>
                            <span class="icon novi-icon icon-xs mdi mdi-account text-middle text-madison"></span>
                            <span class="text-middle inset-left-10 text-italic text-primary"> <?= $artikel['username']; ?>
                            </span>
                        </li>
                        <li>
                            <span class="icon novi-icon icon-xs mdi mdi-eye text-middle text-madison"></span>
                            <span class="text-middle inset-left-10 text-italic text-primary"> <?= $artikel['counter']; ?>
                            </span>
                        </li>
                    </ul>
                </div>

                <?php if($artikel['thumb_img'] == 'no-image.png'){ $urlImg = base_url().'/assets/images/'.$artikel['thumb_img']; 
                            } else { $urlImg = base_url().'/upload/artikel/'.$artikel['thumb_img']; } ?>

                <div class="offset-top-30">
                    <img class="img-responsive" src="<?= $urlImg ?>" width="770" height="500" alt="">
                    <div class="offset-top-30">
                        <?= $artikel['content']; ?>
                    </div>
                </div>
            </div>
        
            <!-- aside-->
            <div class="cell-xs-8 cell-md-4 text-left">
                <aside class="aside inset-md-left-30">
                    <div class="aside-item">
                        <h6 class="text-bold">Search</h6>
                        <div class="text-subline"></div>
                        <div class="offset-top-30">
                        <!-- RD Search Form-->
                            <form class="form-search rd-search form-search-widget" action="search-results.html" method="GET">
                              <div class="form-group">
                                <div class="input-group">
                                  <input class="form-search-input  form-control" type="text" name="s" autocomplete="off"><span class="input-group-btn">
                                    <button class="btn btn-primary" type="submit"><span class="icon fa-search"> </span></button></span>
                                </div>
                              </div>
                            </form>
                        </div>
                    </div>
                    
                    <!-- Archive-->
                    <div class="aside-item">
                        <h6 class="text-bold">Archive</h6>
                        <div class="text-subline"></div>
                        <div class="row offset-top-20">
                            <div class="col-xs-6">
                              <ul class="list list-marked list-marked-primary">
                                <li><a href="#">Jun 2018</a></li>
                                <li><a href="#">Aug 2018</a></li>
                                <li><a href="#">Oct 2018</a></li>
                                <li><a href="#">Dec 2018</a></li>
                                <li><a href="#">Feb 2017</a></li>
                              </ul>
                            </div>
                            <div class="col-xs-6">
                              <ul class="list list-marked list-marked-primary">
                                <li><a href="#">Jul 2018</a></li>
                                <li><a href="#">Sep 2018</a></li>
                                <li><a href="#">Nov 2018</a></li>
                                <li><a href="#">Jan 2018</a></li>
                                <li><a href="#">Mar 2017</a></li>
                              </ul>
                            </div>
                        </div>
                    </div>
                
                    <!-- Categories-->
                    <div class="aside-item">
                        <h6 class="text-bold">Categories</h6>
                        <div class="text-subline"></div>
                        <div class="offset-top-20">
                            <ul class="list list-marked list-marked-primary">
                                <li><a href="#">News</a></li>
                                <li><a href="#">University</a></li>
                                <li><a href="#">Global Education</a></li>
                                <li><a href="#">Law</a></li>
                                <li><a href="#">Colleges</a></li>
                            </ul>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>
</section>

