<section class="features3 cid-rb7PmikRF4" id="features3-f">
    <a href="" class="float-right px-5"> <!-- <i class="fa fa-spinner fa-spin px-3"></i> -->  X</a>
    <br>
    <div class="container">
        <div class="row">
            <?php foreach ($artikel as $art) { ?>
            <div class="card p-3 col-md-6 col-sm-6 col-lg-4">        
                <div class="card-wrapper-index">
                    <div class="card-img-index">

                        <?php if($art['cover_img'] == 'no-image.png'){ $urlImg = base_url().'/assets/images/'.$art['cover_img']; } else { $urlImg = base_url().'/upload/artikel/'.$art['cover_img']; } ?>
                        <img src="<?= $urlImg ?>" class="img-fluid">
                    
                    </div>

                    <a href="<?= base_url(); ?>artikel/detail/<?= $art['slug']; ?>">
                        <div class="card-box-index">
                            <p class="mbr-text mbr-fonts-style display-7">
                                <?= $art['judul'];?> 
                            </p>
                        </div>
                    </a> 
                    
                    <p class="title-index-p"><?= $art['nama_kategori'];  ?> 
                        <span class="float-right"><?= carbonit($art['create_time']);  ?></span>
                    </p>
                </div>
          
            </div> 
            <?php } ?>
        </div>
    </div>
</section>