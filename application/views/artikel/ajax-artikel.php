<h5 class="sidebar"><i class="fa fa-heart-o"></i> Artikel Populer</h5>
	<?php foreach ($artikel as $art) { ?>
		<a href="<?= base_url(); ?>artikel/detail/<?= $art['slug']; ?>">
			<div class="sidebar-box">
				<?php if($art['cover_img'] == 'no-image.png'){ $urlImg = base_url().'/assets/images/'.$art['cover_img']; } else { $urlImg = base_url().'/upload/artikel/'.$art['cover_img']; } ?>
	            <img src="<?= $urlImg ?>" class="img-fluid float-left">
				<?php 
					$j = $art['judul'];
					$jud = substr($j,0,70);
					echo "<h4> $jud</h4>"; 
				 ?>
				<small><?= tanggal($art['create_time']);  ?></small>
				<span><?= $art['nama_kategori']; ?></span>
			</div>
		</a>
	<?php } ?> 