
<!-- Artikel -->
  <section class="section novi-background bg-cover section-70 section-md-114 bg-catskill">
    <div class="shell isotope-wrap">
      <h3 class="text-bold">Fasilitas</h3>
      <p><?= $judul; ?></p>
      <hr class="divider bg-madison">
      <div class="row range-30 isotope offset-top-60 text-left">

        <?php foreach ($fasilitas as $fas) { ?>
        <div class="col-xs-12 col-sm-6 col-md-4 isotope-item">
          <article class="post-news"><a href="">

            <?php if($fas['foto'] == 'no-image.png'){ $urlImg = base_url().'assets/images/'.$fas['foto']; } else { $urlImg = base_url().'/upload/fasilitas/'.$fas['foto']; } ?>

            <img class="img-responsive box-shadow" src="<?= $urlImg ?>" width="370" height="240" alt=""></a>
            <div class="post-news-body box-shadow">
              <h6><a href="<?= base_url(); ?>fasilitas/detail/<?= $fas['id']; ?>"><?= $fas['nama_fasilitas'];?></a></h6>
              <div class="offset-top-20">
                <p>Liberal Arts Colleges emphasize undergraduate education and award at least half of their degrees in the liberal arts fields of study.</p>
              </div>
              <div class="post-news-meta offset-top-20"><span class="icon novi-icon icon-xs mdi mdi-calendar-clock text-middle text-madison"></span><span class="text-middle inset-left-10 text-italic text-black"><?= tanggal($fas['create_time']);  ?></span></div>
            </div>
          </article>
        </div>
        <?php } ?>

      </div>
      <div class="offset-top-50"><a class="btn btn-primary" href="grid-news.html">Fasilitas lainnya</a></div>
    </div>
</section>
<!--  akhir Berita  -->