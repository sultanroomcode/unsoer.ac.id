<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('templates/header', $data);
?>
<h1><?php echo $heading; ?></h1>
<?php echo $message; ?>
<?php $this->load->view('templates/footer'); ?>