
<div id="load-kategori">
<h5 class="sidebar"> Berita Populer</h5>
	<?php foreach ($this->Artikel_model->getPopularArtikel() as $art) { ?>
		<a href="<?= base_url(); ?>artikel/detail/<?= $art['slug']; ?>">
			<div class="sidebar-box">
				<div class="container">
					<div class="row">
						<div class="col-md-2">
							<div class="sidebar-box-img">
								<?php if($art['cover_img'] == 'no-image.png'){ $urlImg = base_url().'/assets/images/'.$art['cover_img']; } else { $urlImg = base_url().'/upload/artikel/'.$art['cover_img']; } ?>
					            <img src="<?= $urlImg ?>" class="img-fluid">
				            </div>
						</div>
						<div class="col-md-4">
							<?php 
								$j = $art['judul']; 
								$jud = substr($j,0,70);
								echo "<h4> $jud</h4>"; 
							 ?>
						</div>
					</div>
				</div>
				

				
				 <!-- <div class="clear"></div> -->
				<!-- <small><i class="fa fa-clock-o"></i> <?= tanggal($art['create_time']);  ?></small> -->
				<!-- <span><i class="fa fa-bookmark-o"></i> <?= $art['nama_kategori']; ?></span> -->
			</div>
		</a>
	<?php } ?> 
</div>



<!-- <script type="text/javascript">
	$(function(){
		$('select#id_kategori').change(function(){
			var category = $(this).val();
			$('#load-kategori').load('<?=base_url()?>artikel/ajaxartikel/'+category);
		});
	});
</script> -->