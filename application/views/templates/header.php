<!DOCTYPE html>
<html class="wide wow-animation scrollTo" lang="en">
  <head>
    <!-- Site Title-->
    <title><?= $judul; ?></title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="keywords" content="intense web design multipurpose template">
    <meta name="date" content="Dec 26">
    <link rel="icon" href="<?= base_url(); ?>/assets/templates/images/icon.png" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:400,300italic,300,400italic,600,700%7CMerriweather:400,300,300italic,400italic,700,700italic">
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/templates/css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/templates/css/style.css">
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/templates/css/custom.css">
        <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
        <![endif]-->
  </head>
  <body>
    <div class="page-loader">
      <div class="page-loader-body"><span class="cssload-loader"><span class="cssload-loader-inner"></span></span></div>
    </div>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <header class="page-head">
        <!-- RD Navbar Transparent-->
        <div class="rd-navbar-wrap">
          <nav class="rd-navbar rd-navbar-default" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-xxl-device-layout="rd-navbar-static" data-lg-stick-up-offset="120px" data-xl-stick-up-offset="120px" data-xxl-stick-up-offset="120px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
            <div class="rd-navbar-inner">
              <!-- RD Navbar Panel-->
              <div class="rd-navbar-panel">
                <!-- RD Navbar Toggle-->
                <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar, .rd-navbar-nav-wrap"><span></span></button>
                <h4 class="panel-title">UNSOER</h4>
                <!-- RD Navbar Right Side Toggle-->
                <button class="rd-navbar-top-panel-toggle" data-rd-navbar-toggle=".rd-navbar-top-panel"><span></span></button>
                <div class="rd-navbar-top-panel novi-background">
                  <div class="rd-navbar-top-panel-left-part">
                    <ul class="list-unstyled">
                      <li>
                        <div class="unit unit-horizontal unit-middle unit-spacing-xs">
                          <div class="unit-left"><span class="icon novi-icon mdi mdi-phone text-middle"></span></div>
                          <div class="unit-body"><a href="">(0351) 749358</a>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="unit unit-horizontal unit-middle unit-spacing-xs">
                          <div class="unit-left"><span class="icon novi-icon mdi mdi-map-marker text-middle"></span></div>
                          <div class="unit-body"><a href="#">Universitas Soerjo Ngawi</a></div>
                        </div>
                      </li>
                      <li>
                        <div class="unit unit-horizontal unit-middle unit-spacing-xs">
                          <div class="unit-left"><span class="icon novi-icon mdi mdi-email-open text-middle"></span></div>
                          <div class="unit-body"><a href="">universitas@unsoer.ac.id</a></div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
                <!-- <div class="top-header">slnvlsvln</div> -->
              </div>
              
              <div class="rd-navbar-menu-wrap clearfix novi-background">
                <!--Navbar Brand-->
                <div class="rd-navbar-brand"><a class="reveal-inline-block" href=""><img src="<?= base_url(); ?>assets/templates/images/logo.png" alt="" width="288" height="59" srcset="<?= base_url(); ?>assets/templates/images/logo.png"/></a></div>
                <div class="rd-navbar-nav-wrap">
                  <div class="rd-navbar-mobile-scroll">
                    <div class="rd-navbar-mobile-header-wrap">
                      <!--Navbar Brand Mobile-->
                      <div class="rd-navbar-mobile-brand"><a href=""><img src="<?= base_url(); ?>assets/templates/images/logo.png" alt="" width="150" height="150" srcset="<?= base_url(); ?>assets/templates/images/logo.png"/></a></div>
                    </div>
                    <!-- RD Navbar Nav-->
                    <ul class="rd-navbar-nav">
                      <li class=""><a href="<?= base_url('home'); ?>">Beranda</a></li>
                      <li><a href="#">Profil</a>
                        <ul class="rd-navbar-dropdown">
                          <li><a href="<?= base_url('profil'); ?>">Profil Universitas</a></li>
                          <li><a href="<?= base_url('profil/sejarah'); ?>">Sejarah Soerjo</a></li>
                          <li><a href="<?= base_url('profil-pimpinan'); ?>">Pimpinan Universitas</a></li>
                          <li><a href="<?= base_url('profil/visimisi'); ?>">Visi dan Misi</a>
                          </li>
                        </ul>
                      </li>
                      <li><a href="">Penelitian & Inovasi</a>
                        <ul class="rd-navbar-dropdown">
                          <li><a href="<?= base_url('penelitian-inovasi/jurnal'); ?>">Jurnal</a></li>
                          <li><a href="<?= base_url('penelitian-inovasi/penelitian'); ?>">Penelitian</a></li>
                          <li><a href="<?= base_url('penelitian-inovasi/inovasi'); ?>">Inovasi</a></li>
                          <li><a href="<?= base_url('penelitian-inovasi/riset-inovasi'); ?>">Riset dan Inovasi</a></li>
                          <li><a href="<?= base_url('penelitian-inovasi/pengabdian-masyarakat'); ?>">Pengabdian Masyarakat</a></li>
                        </ul>
                      </li>
                      <li><a href="#">Fakultas</a>
                        <div class="rd-navbar-megamenu">
                          <div class="row section-relative">
                            <ul class="col-md-4">
                              <li>
                                <h6><a href="<?= base_url('fakultas/fisip'); ?>">Fakultas Ilmu Sosial dan Ilmu Politik</a></h6>
                                <ul class="list-unstyled offset-lg-top-20">
                                  <li><a href="<?= base_url('prodi/administrasi-negara'); ?>">Prodi Ilmu Administrasi Negara</a></li>
                                </ul>
                              </li>
                              <br>
                              <li>
                                <h6><a href="<?= base_url('fakultas/hukum'); ?>">Fakultas Hukum</a></h6>
                                <ul class="list-unstyled offset-lg-top-20">
                                  <li><a href="<?= base_url('prodi/ilmu-hukum'); ?>">Prodi Ilmu Hukum</a></li>
                                </ul>
                              </li>
                            </ul>
                            <ul class="col-md-4">
                              <li>
                                <h6><a href="<?= base_url('fakultas/pertanian'); ?>">Fakultas Pertanian</a></h6>
                                <ul class="list-unstyled offset-lg-top-20">
                                  <li><a href="<?= base_url('prodi/agroteknologi'); ?>">Prodi Agroteknologi</a></li>
                                </ul>
                              </li>
                              <br>
                              <li>
                                <h6><a href="<?= base_url('fakultas/ekonomi'); ?>">Fakultas Ekonomi</a></h6>
                                <ul class="list-unstyled offset-lg-top-20">
                                  <li><a href="<?= base_url('prodi/manajemen'); ?>">Prodi Manajemen</a></li>
                                </ul>
                              </li>
                            </ul>
                            <ul class="col-md-4">
                              <li>
                                <h6><a href="<?= base_url('fakultas/teknik'); ?>">Fakultas Teknik</a></h6>
                                <ul class="list-unstyled offset-lg-top-20">
                                  <li><a href="<?= base_url('prodi/sipil'); ?>">Prodi Teknik Sipil</a></li>
                                  <li><a href="<?= base_url('prodi/mesin'); ?>">Prodi Teknik Mesin</a></li>
                                </ul>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </li>
                      <li><a href="#">Fasilitas</a>
                        <ul class="rd-navbar-dropdown">
                          <li><a href="<?= base_url('fasilitas/ruangkuliah'); ?>">Ruang Kuliah</a>
                          </li>
                          <li><a href="<?= base_url('fasilitas/laboratorium'); ?>">Laboratorium</a>
                          </li>
                          <li><a href="<?= base_url('fasilitas/gedung'); ?>">Gedung</a>
                          </li>
                          <li><a href="<?= base_url('fasilitas/lainnya'); ?>">lainnya</a>
                          </li>
                        </ul>
                      </li>
                      <li><a href="#">UPT</a>
                        <ul class="rd-navbar-dropdown">
                          <li><a href="<?= base_url('upt/bau'); ?>">BAU</a></li>
                          <li><a href="<?= base_url('upt/baak'); ?>">BAAK</a></li>
                          <li><a href="<?= base_url('upt/lppm'); ?>">LPPM</a></li>
                          <li><a href="<?= base_url('upt/lpm'); ?>">LPM</a></li>
                          <li><a href="<?= base_url('upt/perpustakaan'); ?>">Perpustakaan</a></li>
                          <li><a href="<?= base_url('upt/pusat-karir'); ?>">Pusat Karir</a></li>
                        </ul>
                      </li>
                      <li><a href="#">Mahasiswa</a>
                        <ul class="rd-navbar-dropdown">
                          <li><a href="<?= base_url('mahasiswa/aturan-akademik'); ?>">Aturan Akademik</a></li>
                          <li><a href="<?= base_url('mahasiswa/ekstrakurikuler'); ?>">Extrakurikuler</a></li>
                          <li><a href="<?= base_url('mahasiswa/prestasi'); ?>">Prestasi Mahasiswa</a></li>
                        </ul>
                      </li>
                    </ul>
                    <!--RD Navbar Mobile Search-->
                    <div class="rd-navbar-search-mobile" id="rd-navbar-search-mobile">
                      <form class="rd-navbar-search-form search-form-icon-right rd-search" action="search-results.html" method="GET">
                        <div class="form-group">
                          <label class="form-label" for="rd-navbar-mobile-search-form-input">Search...</label>
                          <input class="rd-navbar-search-form-input form-control form-control-gray-lightest" id="rd-navbar-mobile-search-form-input" type="text" name="s" autocomplete="off"/>
                        </div>
                        <button class="icon fa-search rd-navbar-search-button" type="submit"></button>
                      </form>
                    </div>
                  </div>
                </div>
                <!--RD Navbar Search-->
                <div class="rd-navbar-search"><a class="rd-navbar-search-toggle mdi" data-rd-navbar-toggle=".rd-navbar-search" href="#"><span></span></a>
                  <form class="rd-navbar-search-form search-form-icon-right rd-search" action="search-results.html" data-search-live="rd-search-results-live" method="GET">
                    <div class="form-group">
                      <label class="form-label" for="rd-navbar-search-form-input">Search</label>
                      <input class="rd-navbar-search-form-input form-control form-control-gray-lightest" id="rd-navbar-search-form-input" type="text" name="s" autocomplete="off"/>
                      <div class="rd-search-results-live" id="rd-search-results-live"></div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </header>