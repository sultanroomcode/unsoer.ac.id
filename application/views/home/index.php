 <!-- Slider -->
 <section class="section">
    <!-- Swiper-->
    <div class="swiper-container swiper-slider swiper-slider-3" data-loop="true" data-slide-effect="true">
      <div class="swiper-wrapper">
        <?php foreach ($slider as $sli) { ?>

          <?php if($sli['thumb_img'] == 'no-image-slide.png'){ $imgSlide = base_url().'assets/images/'.$sli['thumb_img']; } else { $imgSlide = base_url().'upload/artikel/'.$sli['thumb_img']; } ?>

        <div class="swiper-slide" data-slide-bg="<?= $imgSlide ?>" style="background-position: 80% center">
          <div class="swiper-slide-caption section-70">
            <div class="container">
              <div class="range range-xs-center range-lg-left">
                <div class="cell-md-9 text-md-left cell-xs-10">
                  <div data-caption-animate="fadeInUp" data-caption-delay="100">
                    <h1 class="text-bold text-shadow"><?= $sli['title']; ?></h1>
                  </div>
                  <div class="offset-top-20 offset-xs-top-40 offset-xl-top-60 inset-lg-right-100" data-caption-animate="fadeInUp" data-caption-delay="150">
                    <h5><?= $sli['content']; ?></h5>
                  </div>
                  <div class="offset-top-20 offset-xl-top-40" data-caption-animate="fadeInUp" data-caption-delay="400">
                    <div class="group-xl group-middle"><a class="btn btn-primary" href="#">Selengkapnya</a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>

      </div>
      <!-- Swiper Pagination-->
      <div class="swiper-pagination"></div>
    </div>
</section>
<!-- Akhir Slider -->

<!-- Tentang Universitas-->
<section class="section novi-background bg-cover section-70 section-md-114 bg-default">
    <div class="shell">
        <div class="range range-50 text-sm-left range-sm-middle range-sm-justify">
        <?php foreach ($tentang_kami as $ttg) { ?>
            <div class="cell-sm-5 cell-md-4 cell-sm-push-2">
                <a href="">
                    <img class="img-responsive reveal-inline-block" src="<?= base_url(); ?>/assets/templates/images/logo.png" alt="" width="326" height="237"/>
                </a>
            </div>
            <div class="cell-sm-7 cell-md-7 cell-sm-push-1">
                <h2 class="text-bold"><?= $ttg['judul']; ?></h2>
                <hr class="divider bg-madison hr-sm-left-0">
                <div class="offset-top-35 offset-md-top-60">
                    <p><?= $ttg['deskripsi']; ?></p>
                </div>
                <div class="offset-top-30">
                    <a class="btn btn-icon btn-icon-right btn-success" href="<?= base_url('profil'); ?>">
                        <span class="icon fa-arrow-right"></span>
                        <span>Selengkapnya</span>
                    </a>
                </div>
            </div>
        <?php } ?>
        </div>
    </div>
</section>
<!-- Akhir Tentang Universitas-->

<!--People-->
<section class="section novi-background bg-cover section-70 section-md-114 bg-default">
    <div class="shell">
      <h2 class="text-bold">Pimpinan Universitas</h2>
      <hr class="divider bg-madison">
      <div class="range range-30 text-md-left offset-top-60">

        <?php foreach ($pimpinan as $p) { ?>

          <?php if($p['foto'] == 'default.png'){ $imgPimpinan = base_url().'assets/images/'.$p['foto']; } else { $imgPimpinan = base_url().'upload/pimpinan/'.$p['foto']; } ?>

        <div class="cell-sm-6 cell-md-3"><img class="img-responsive reveal-inline-block img-rounded" src="<?= $imgPimpinan ?>" width="270" height="270" alt="">
          <div class="offset-top-20">
            <h6 class="text-bold text-primary"><a href="<?= base_url(); ?>profil_pimpinan/detail/<?= $p['id']; ?>"><?= $p['nama']; ?></a></h6>
          </div>
          <div class="offset-top-5">
            <p><?= $p['jabatan']; ?></p>
          </div>
        </div>
        <?php } ?>

      </div>
    </div>
</section>
<!-- Akhir People -->

<!-- Artikel -->
  <section class="section novi-background bg-cover section-70 section-md-114 bg-catskill">
    <div class="shell isotope-wrap">
      <h2 class="text-bold">Artikel</h2>
      <hr class="divider bg-madison">
      <div class="row range-30 isotope offset-top-60 text-left">

        <?php foreach ($artikel as $art) { ?>
        <div class="col-xs-12 col-sm-6 col-md-4 isotope-item">
          <article class="post-news"><a href="<?= base_url(); ?>artikel/detail/<?= $art['slug']; ?>">

            <?php if($art['thumb_img'] == 'no-image.png'){ $urlImg = base_url().'assets/images/'.$art['thumb_img']; } else { $urlImg = base_url().'/upload/artikel/'.$art['thumb_img']; } ?>

            <img class="img-responsive box-shadow" src="<?= $urlImg ?>" width="370" height="240" alt=""></a>
            <div class="post-news-body box-shadow">
              <h6><a href="<?= base_url(); ?>artikel/detail/<?= $art['slug']; ?>"><?= $art['title'];?></a></h6>
              <div class="offset-top-20">
                <p><?= $art['content']?></p>
              </div>
              <div class="post-news-meta offset-top-20"><span class="icon novi-icon icon-xs mdi mdi-calendar-clock text-middle text-madison"></span><span class="text-middle inset-left-10 text-italic text-black"><?= tanggal($art['created_at']);  ?></span></div>
              <div class="post-news-meta offset-top-20"><span class="icon novi-icon icon-xs mdi mdi-apps text-middle text-madison"></span><span class="text-middle inset-left-10 text-italic text-black"><?= $art['type'];  ?></span></div>
            </div>
          </article>
        </div>
        <?php } ?>

      </div>
      <div class="offset-top-50"><a class="btn btn-primary" href="grid-news.html">Artikel lainnya</a></div>
    </div>
</section>
<!--  akhir Berita  -->

<!--Video-->
<section class="section novi-background bg-cover section-70 section-md-114 bg-default">
    <div class="shell">
      <h2 class="text-bold">Video</h2>
      <hr class="divider bg-madison">
      <div class="range range-30 text-md-left offset-top-60">

        <?php foreach ($video as $v) { ?>

        <div class="cell-sm-6 cell-md-3">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?=$v['link'];?>?rel=0" allowfullscreen>
            </iframe>
          </div>
        </div>
        <?php } ?>

      </div>
    </div>
</section>
<!-- Akhir Video -->



