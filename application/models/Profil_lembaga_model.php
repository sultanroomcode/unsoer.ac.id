<?php 

/**
 * Profil_lembaga
 */
class Profil_lembaga_model extends CI_model
{
	
	public function getAllProfilLembaga()
	{
		$this->db->from('tentang_kami');
		return $this->db->get()->result_array();
	}
}