<?php 

/**
 * Fakultas_model
 */
class Fakultas_model extends CI_model
{
	
	public function getAllSlider($ua)
	{
		$this->db->from('konten');
		$this->db->where(['created_by' => $ua, 'type' => 'slider']);
		return $this->db->get()->result_array();
	}

	public function getAllPimpinan($ua)
	{
		$this->db->from('pimpinan');
		$this->db->where('create_by', $ua);
		return $this->db->get()->result_array();
	}

	public function getAllArtikel($ua)
	{
		$this->db->where(['created_by' => $ua, 'status' => 'PUBLISH']);
		$this->db->where_in('type', ['berita','pengumuman', 'agenda']);
		return $this->db->get('konten')->result_array();
	}


	public function getAllSliderUniversitas()
	{
		$this->db->from('slider');
		$this->db->where('user_access', '1');
		return $this->db->get()->result_array();
	}
	public function getAllSliderAdminNegara()
	{
		$this->db->from('slider');
		$this->db->where('user_access', '2');
		return $this->db->get()->result_array();
	}
	public function getAllSliderHukum()
	{
		$this->db->from('slider');
		$this->db->where('user_access', '3');
		return $this->db->get()->result_array();
	}
	public function getAllSliderAgroteknologi()
	{
		$this->db->from('slider');
		$this->db->where('user_access', '4');
		return $this->db->get()->result_array();
	}
	public function getAllSliderManajemen()
	{
		$this->db->from('slider');
		$this->db->where('user_access', '5');
		return $this->db->get()->result_array();
	}
	public function getAllSliderSipil()
	{
		$this->db->from('slider');
		$this->db->where('user_access', '6');
		return $this->db->get()->result_array();
	}
	public function getAllSliderMesin()
	{
		$this->db->from('slider');
		$this->db->where('user_access', '7');
		return $this->db->get()->result_array();
	}

	public function getAllPimpinanUniversitas()
	{
		$this->db->from('pimpinan');
		$this->db->where('user_access', '1');
		return $this->db->get()->result_array();
	}
	public function getAllPimpinanAdminNegara()
	{
		$this->db->from('pimpinan');
		$this->db->where('user_access', '2');
		return $this->db->get()->result_array();
	}
	public function getAllPimpinanHukum()
	{
		$this->db->from('pimpinan');
		$this->db->where('user_access', '3');
		return $this->db->get()->result_array();
	}
	public function getAllPimpinanAgroteknologi()
	{
		$this->db->from('pimpinan');
		$this->db->where('user_access', '4');
		return $this->db->get()->result_array();
	}
	public function getAllPimpinanManajemen()
	{
		$this->db->from('pimpinan');
		$this->db->where('user_access', '5');
		return $this->db->get()->result_array();
	}
	public function getAllPimpinanSipil()
	{
		$this->db->from('pimpinan');
		$this->db->where('user_access', '6');
		return $this->db->get()->result_array();
	}
	public function getAllPimpinanMesin()
	{
		$this->db->from('pimpinan');
		$this->db->where('user_access', '7');
		return $this->db->get()->result_array();
	}
}