<?php 
/**
 * Artikel_model 
 */
class Artikel_baru_model extends MY_model
{
	private $wherex_;
	function __construct(){
		parent::__construct();
		$this->setTableName('konten');
		$this->wherex_ = ['type' => 'agenda'];
        $this->load->library('form_validation');
	}

	public function getAllArtikel($ua = 'universitas')
	{
		$this->db->select('konten.id, slug, konten.type, title, content, admin.username, konten.created_at, thumb_img');
		$this->db->join('admin', 'konten.created_by = admin.id');
		$this->db->order_by('konten.created_at', 'DESC');
		$this->db->where_in('konten.type', ['agenda', 'berita', 'pengumuman']);
		$this->db->where(['admin.depart' => $ua, 'konten.status' => 'PUBLISH']);
		// $this->setWhere($this->wherex_);
		return $this->getdata()->result_array();
	}

	public function getAllSlider($ua = 'universitas')
	{
		$this->db->select('konten.id, slug, konten.type, title, content, admin.username, konten.created_at, thumb_img');
		$this->db->join('admin', 'konten.created_by = admin.id');
		$this->db->order_by('konten.created_at', 'DESC');
		$this->db->where(['admin.depart' => $ua, 'konten.status' => 'PUBLISH', 'konten.type' => 'slider']);
		// $this->setWhere($this->wherex_);
		return $this->getdata()->result_array();
	}

	public function getArtikelBySlug($slug_artikel)
	{
		$slug_artikel = str_replace('_', '-', $slug_artikel);
		$this->db->select('konten.id, slug, konten.type, title, content, admin.username, konten.created_at, thumb_img, counter');
		$this->db->join('admin', 'konten.created_by = admin.id');
		// $this->db->order_by('artikel.create_time', 'DESC');
		$this->db->where(['konten.slug' => $slug_artikel, 'konten.status' => 'PUBLISH']);
		return $this->getdata()->row_array();
	}

	public function addReadOne($slug)
	{
		$slug = str_replace('_', '-', $slug);
		$this->db->where('slug', $slug);
		$this->db->set('counter', '`counter`+ 1', FALSE);
		$this->db->update('konten');
	}
}

