<?php 

/**
 * Foto_kegiatan_model
 */
class Foto_kegiatan_model extends CI_model
{
	
	public function getAllFotoKegiatan()
	{
		return $this->db->get('foto_kegiatan')->result_array();
	}

	public function getAllFotoKegiatanFromGallery($id_gallery)
	{
		$this->db->order_by('foto_kegiatan.create_time', 'DESC');
		$this->db->where(['id_gallery' => $id_gallery]);
		return $this->db->get('foto_kegiatan')->result_array();
	}
}
