<?php 

/**
 * Gallery_model
 */
class Gallery_model extends CI_model
{

	public function getAllGallery()
	{
		return $this->db->get('upload_foto')->result_array();
	}

	public function getGalleryById($id)
	{
		$this->db->where(['id' => $id]);
		return $this->db->get('upload_foto')->row_array();
	}

	public function getAllFotoKegiatanFromGallery($id_gallery)
	{
		$this->db->order_by('foto_kegiatan.create_time', 'DESC');
		$this->db->where(['id_gallery' => $id_gallery]);
		return $this->db->get('foto_kegiatan')->result_array();
	}

}