<?php 
class Profil_pimpinan_model extends MY_model
{
	private $wherex_;
	function __construct(){
		parent::__construct();
		$this->setTableName('pimpinan');
        $this->load->library('form_validation');
	}

	public function getAllProfilPimpinan($ua)
	{
		$this->wherex_ = ['user_access' => $ua];
		$this->setWhere($this->wherex_);
		return $this->getdata()->result_array();
	}

	public function getPimpinanById($id)
	{
		return $this->db->get_where('pimpinan', ['id' => $id])->row_array();
	}
}