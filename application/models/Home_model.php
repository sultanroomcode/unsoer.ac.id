<?php 

/**
 * Home
 */
class Home_model extends CI_model
{
	
	public function getAllSlider()
	{
		$this->db->where('user_access', '1');
		return $this->db->get('slider')->result_array();
	}

	public function getAllTentangKami()
	{
		return $this->db->get('tentang_kami')->result_array();
	}

	public function getAllPimpinan()
	{
		$this->db->where('user_access', '1');
		return $this->db->get('pimpinan')->result_array();
	}

	public function getAllSliderArtikel()
	{
		$this->db->order_by('artikel.create_time', 'DESC');
		$this->db->limit(5);
		return $this->db->get('artikel')->result_array();
	}
}
