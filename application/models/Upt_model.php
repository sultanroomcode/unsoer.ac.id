<?php 

/**
 * Upt_model
 */
class Upt_model extends CI_model
{
	public function getAllSlider($ua)
	{
		$this->db->from('konten');
		$this->db->where(['created_by' => $ua, 'type' => 'slider']);
		return $this->db->get()->result_array();
	}

	public function getAllPimpinan($ua)
	{
		$this->db->from('pimpinan');
		$this->db->where('create_by', $ua);
		return $this->db->get()->result_array();
	}

	public function getAllArtikel($ua)
	{
		$this->db->where(['created_by' => $ua, 'status' => 'PUBLISH']);
		$this->db->where_in('type', ['berita','pengumuman', 'agenda']);
		return $this->db->get('konten')->result_array();
	}



	/*---------------------------------------------------------------*/

	public function getAllSliderBau()
	{
		$this->db->from('slider');
		$this->db->where('user_access', '8');
		return $this->db->get()->result_array();
	}
	public function getAllSliderBaak()
	{
		$this->db->from('slider');
		$this->db->where('user_access', '9');
		return $this->db->get()->result_array();
	}
	public function getAllSliderLppm()
	{
		$this->db->from('slider');
		$this->db->where('user_access', '3');
		return $this->db->get()->result_array();
	}
	public function getAllSliderLpm()
	{
		$this->db->from('slider');
		$this->db->where('user_access', '4');
		return $this->db->get()->result_array();
	}
	public function getAllSliderPerpustakaan()
	{
		$this->db->from('slider');
		$this->db->where('user_access', '5');
		return $this->db->get()->result_array();
	}
	public function getAllSliderPusatKarir()
	{
		$this->db->from('slider');
		$this->db->where('user_access', '5');
		return $this->db->get()->result_array();
	}
	
	public function getAllPimpinanBau()
	{
		$this->db->from('pimpinan');
		$this->db->where('user_access', '8');
		return $this->db->get()->result_array();
	}
	public function getAllPimpinanBaak()
	{
		$this->db->from('pimpinan');
		$this->db->where('user_access', '9');
		return $this->db->get()->result_array();
	}

	public function getAllPimpinanHukum()
	{
		$this->db->from('pimpinan');
		$this->db->where('user_access', '3');
		return $this->db->get()->result_array();
	}
	public function getAllPimpinanAgroteknologi()
	{
		$this->db->from('pimpinan');
		$this->db->where('user_access', '4');
		return $this->db->get()->result_array();
	}
	public function getAllPimpinanManajemen()
	{
		$this->db->from('pimpinan');
		$this->db->where('user_access', '5');
		return $this->db->get()->result_array();
	}
	public function getAllPimpinanSipil()
	{
		$this->db->from('pimpinan');
		$this->db->where('user_access', '6');
		return $this->db->get()->result_array();
	}
	public function getAllPimpinanMesin()
	{
		$this->db->from('pimpinan');
		$this->db->where('user_access', '7');
		return $this->db->get()->result_array();
	}

	
}