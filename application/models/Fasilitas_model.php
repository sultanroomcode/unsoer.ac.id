<?php 

/**
 * Fasilitas_model
 */
class Fasilitas_model extends CI_model
{
	
	public function getAllFasilitasRuangKuliah()
	{
		$this->db->from('fasilitas');
		$this->db->where('kategori_fasilitas', '1');
		return $this->db->get()->result_array();
	}

	public function getAllFasilitasGedung()
	{
		$this->db->from('fasilitas');
		$this->db->where('kategori_fasilitas', '3');
		return $this->db->get()->result_array();
	}

	public function getAllFasilitasLaboratorium()
	{
		$this->db->from('fasilitas');
		$this->db->where('kategori_fasilitas', '4');
		return $this->db->get()->result_array();
	}
	public function getAllFasilitasLainnya()
	{
		$this->db->from('fasilitas');
		$this->db->where('kategori_fasilitas', '5');
		return $this->db->get()->result_array();
	}
}