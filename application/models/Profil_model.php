<?php 

/**
 * Profil_model
 */
class Profil_model extends CI_model
{
	
	public function getAllProfil()
	{
		$this->db->from('tentang_kami');
		return $this->db->get()->result_array();
	}

	public function getAllSejarah()
	{
		$this->db->from('sejarah');
		return $this->db->get()->result_array();
	}

	public function getAllVisimisi()
	{
		$this->db->from('visimisi');
		return $this->db->get()->result_array();
	}
}