<?php 
/**
 * Artikel_model 
 */
class Artikel_model extends CI_model
{

	public function getAllArtikel()
	{
		$this->db->select('id_artikel, judul, isi_artikel, kategori.nama_kategori, admin.username, artikel.create_time, jumlah_baca, cover_img, slug');
		$this->db->from('artikel');
		$this->db->join('admin', 'artikel.create_by = admin.id');
		$this->db->join('kategori', 'artikel.id_kategori = kategori.id_kategori');
		$this->db->order_by('artikel.create_time', 'DESC');
		return $this->db->get()->result_array();
	}

	public function getArtikelById($id_artikel)
	{
		$this->db->select('id_artikel, judul, isi_artikel, kategori.nama_kategori, admin.username, artikel.create_time, cover_img, jumlah_baca');
		$this->db->from('artikel');
		$this->db->join('admin', 'artikel.create_by = admin.id');
		$this->db->join('kategori', 'artikel.id_kategori = kategori.id_kategori');
		// $this->db->order_by('artikel.create_time', 'DESC');
		$this->db->where(['id_artikel' => $id_artikel]);
		return $this->db->get()->row_array();
	}

	public function getArtikelBySlug($slug_artikel)
	{
		$this->db->select('id_artikel, judul, isi_artikel, kategori.nama_kategori, admin.username, artikel.create_time, cover_img, jumlah_baca');
		$this->db->from('artikel');
		$this->db->join('admin', 'artikel.create_by = admin.id');
		$this->db->join('kategori', 'artikel.id_kategori = kategori.id_kategori');
		// $this->db->order_by('artikel.create_time', 'DESC');
		$this->db->where(['slug' => $slug_artikel]);
		return $this->db->get()->row_array();
	}

	public function getLastArtikel()
	{
		$this->db->select('id_artikel, judul, isi_artikel, kategori.nama_kategori, admin.username, artikel.create_time, cover_img, slug');
		$this->db->from('artikel');
		$this->db->join('admin', 'artikel.create_by = admin.id');
		$this->db->join('kategori', 'artikel.id_kategori = kategori.id_kategori');
		$this->db->order_by('artikel.create_time', 'DESC');
		$this->db->limit(7);
		return $this->db->get()->result_array();
	}

	public function getPopularArtikel()
	{
		$this->db->select('id_artikel, judul, isi_artikel, kategori.nama_kategori, admin.username, artikel.create_time, cover_img, slug, jumlah_baca');
		$this->db->from('artikel');
		$this->db->join('admin', 'artikel.create_by = admin.id');
		$this->db->join('kategori', 'artikel.id_kategori = kategori.id_kategori');
		$this->db->order_by('jumlah_baca', 'DESC');
		$this->db->limit(7);
		return $this->db->get()->result_array();
	}

	public function getArtikelByIdKategori($id_kategori)
	{
		$this->db->select('id_artikel, judul, isi_artikel, kategori.nama_kategori, admin.username, artikel.create_time, cover_img, slug');
		$this->db->from('artikel');
		$this->db->join('admin', 'artikel.create_by = admin.id');
		$this->db->join('kategori', 'artikel.id_kategori = kategori.id_kategori');
		$this->db->order_by('artikel.create_time', 'DESC');
		$this->db->limit(7);
		$this->db->where(['artikel.id_kategori' => $id_kategori ]);
		return $this->db->get()->result_array();
	}

	public function addReadOne($slug)
	{
		$this->db->where('slug', $slug);
		$this->db->set('jumlah_baca', '`jumlah_baca`+ 1', FALSE);
		$this->db->update('artikel');
	}
}

